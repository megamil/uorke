<!DOCTYPE html>
<html lang="en">
  <head>
      <meta name="theme-color" content="#f7921c">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){

        $('.select2').select2();

        $('.select2tag').select2({

          tags: true,

          ajax: {
            url: "admin/Controller_webservice/buscar_Locais",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                  cidade: params.term // search term
              };
            },
            processResults: function (data) {
              // parse the results into the format expected by Select2.
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data
              return {
                  results: data
              };
            },
            cache: true
            },
            minimumInputLength: 3


        });

        $('#pesquisar').click(function(){
          window.location = "usuarios.php?subcategoria="+$('#categoria').val()+"&cidade="+$('#cidade').val();
        });


      });
    </script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/bt2.css" rel="stylesheet">
    <link href="css/bt3.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">
    <style>
       #slide_100{
           height: 100%;
       }
    </style>
      <script>
          function fechar_bloco(){
              $('#bloco_fixo').animate({'opacity':'0'}, 800, 'linear');
              $('#bloco_fixo').css('display', 'none');
          }

      </script>




     <style>

         @media screen and (min-width: 350px) and (max-width: 370px) {
             .select2-container--default .select2-selection--multiple{
                 width: 90%;

             }
         }
         @media screen and (min-width: 330px) and (max-width: 300px) {
             .select2-container--default .select2-selection--multiple{
                 width: 60%;

             }
         }
     </style>


  </head>
<!-- NAVBAR
================================================== -->
  <body>
  <div id="tudo">
  <!---------------------- topo ------------->
      <?php include'topo.php' ; ?>
 	<!---------------------- fecha topo ------------->
    <!-- Carousel
    ================================================== -->

    <div id="myCarousel" class="carousel slide" data-ride="carousel" >


        <div class="container" id="caixa">
            <div class="carousel-caption"  id="primeiro_slide">
                <p>&nbsp;</p>
                <div id="bloco_fixo">
                    <p id="fechar_bloco" onclick="fechar_bloco()">X</p>
                <h1 id="titulo_slide" >O modo mais prático</h1>
                    <p class="sub_t_slide" style="font-size: 17px"><span style="color: #5a5a5a">cadastre-se</span> por 15 dias grátis.</p>
                    <p><center><a class="btn btn-lg btn-primary" href="cadastro.php" role="button" style="width: 50%;display: table" id="cadastrar">Cadastre-se grátis</centera></a></p>
                </div>

                <div id="caixa_pesquisa" class="col-lg-12  col-md-12 col-sm-12 col-xs-12" style="text-shadow: 0 0 0 #000;">
                    <form action="#" method="POST" id="formulario_pesquisa">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" id="alinhar_form_pequeno">

                            <span class="pesquisar_cidade_prof_direita"  style="padding-top: 3px ">Sua Cidade</span><br>
                            <select class="form-control select2tag" id="cidade" name="cidade">
                              <option value="">Digite a cidade de origem</option>
                            </select>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="text-shadow: 0 0 0 #000" id="alinhar_form_pequeno1">
                            <span class="pesquisar_cidade_prof" style="width: 100%; padding-top: 3px ">Escolha o seu profissional</span><br>
                            <select class="form-control select2" id="categoria" name="categoria">
                              <option value="1">REFORMAS -> Arquiteto</option>
                              <option value="2">REFORMAS -> Automação Residencial</option>
                              <option value="3">REFORMAS -> Chaveiro</option>
                              <option value="4">REFORMAS -> Decorador</option>
                              <option value="5">REFORMAS -> Dedetizador   </option>
                              <option value="6">REFORMAS -> Desentupidor</option>
                              <option value="7">REFORMAS -> Eletricista</option>
                              <option value="8">REFORMAS -> Encanador</option>
                              <option value="9">REFORMAS -> Engenheiro</option>
                              <option value="10">REFORMAS -> Gesso e drywall</option>
                              <option value="11">REFORMAS -> Impermeabilizador</option>
                              <option value="12">REFORMAS -> Jardinagem</option>
                              <option value="13">REFORMAS -> Limpeza Pós-Obra</option>
                              <option value="14">REFORMAS -> Marceneiro</option>
                              <option value="15">REFORMAS -> Marido de aluguel</option>
                              <option value="16">REFORMAS -> Montador de móveis</option>
                              <option value="17">REFORMAS -> Mudanças e carretos</option>
                              <option value="19">REFORMAS -> Paisagista       </option>
                              <option value="20">REFORMAS -> Pedreiro</option>
                              <option value="21">REFORMAS -> Pintor</option>
                              <option value="22">REFORMAS -> Piscina</option>
                              <option value="23">REFORMAS -> Segurança eletrônica</option>
                              <option value="24">REFORMAS -> Serralheria e solda</option>
                              <option value="25">REFORMAS -> Tapeceiro</option>
                              <option value="26">REFORMAS -> Terraplanagem</option>
                              <option value="27">REFORMAS -> Vidraceiro</option>
                              <option value="28">ASSISTÊNCIA TÉCNICA -> Aparelhos de Som</option>
                              <option value="29">ASSISTÊNCIA TÉCNICA -> Ar condicionado</option>
                              <option value="30">ASSISTÊNCIA TÉCNICA -> Cabeamento e Redes</option>
                              <option value="31">ASSISTÊNCIA TÉCNICA -> Câmera</option>
                              <option value="32">ASSISTÊNCIA TÉCNICA -> Celular</option>
                              <option value="33">ASSISTÊNCIA TÉCNICA -> Computador desktop</option>
                              <option value="34">ASSISTÊNCIA TÉCNICA -> DVD / Blu-Ray</option>
                              <option value="35">ASSISTÊNCIA TÉCNICA -> Eletrodomésticos</option>
                              <option value="36">ASSISTÊNCIA TÉCNICA -> Fogão e Cooktop</option>
                              <option value="37">ASSISTÊNCIA TÉCNICA -> Geladeira e freezer</option>
                              <option value="38">ASSISTÊNCIA TÉCNICA -> Home theater</option>
                              <option value="39">ASSISTÊNCIA TÉCNICA -> Impressora</option>
                              <option value="40">ASSISTÊNCIA TÉCNICA -> Lava Louça</option>
                              <option value="41">ASSISTÊNCIA TÉCNICA -> Lava Roupas</option>
                              <option value="42">ASSISTÊNCIA TÉCNICA -> Microondas</option>
                              <option value="43">ASSISTÊNCIA TÉCNICA -> Notebooks e laptops</option>
                              <option value="45">ASSISTÊNCIA TÉCNICA -> Tablet</option>
                              <option value="46">ASSISTÊNCIA TÉCNICA -> Telefones (não celular)</option>
                              <option value="47">ASSISTÊNCIA TÉCNICA -> Telefonia PABX</option>
                              <option value="48">ASSISTÊNCIA TÉCNICA -> Televisão</option>
                              <option value="49">ASSISTÊNCIA TÉCNICA -> Video game</option>
                              <option value="50">AULAS -> Artes e artesanatos</option>
                              <option value="51">AULAS -> Bem-Estar</option>
                              <option value="52">AULAS -> Concursos</option>
                              <option value="53">AULAS -> Aulas de Dança</option>
                              <option value="54">AULAS -> Aulas particulares</option>
                              <option value="55">AULAS -> Esportes</option>
                              <option value="56">AULAS -> Aulas de idiomas</option>
                              <option value="57">AULAS -> Aulas de informática</option>
                              <option value="58">AULAS -> Lutas</option>
                              <option value="59">AULAS -> Aulas de música</option>
                              <option value="61">AUTOS -> Alarme automotivo</option>
                              <option value="62">AUTOS -> Ar Condicionado</option>
                              <option value="63">AUTOS -> Funilaria</option>
                              <option value="64">AUTOS -> Inspeção veicular</option>
                              <option value="65">AUTOS -> Insufilm</option>
                              <option value="66">AUTOS -> Martelinho de Ouro</option>
                              <option value="67">AUTOS -> Revisão</option>
                              <option value="68">AUTOS -> Som Automotivo</option>
                              <option value="69">CONSULTORIA -> Advogados</option>
                              <option value="70">CONSULTORIA -> Assessoria de imprensa</option>
                              <option value="71">CONSULTORIA -> Auxilio administrativo</option>
                              <option value="72">CONSULTORIA -> Consultor pessoal</option>
                              <option value="73">CONSULTORIA -> Consultoria especializada</option>
                              <option value="74">CONSULTORIA -> Contador</option>
                              <option value="75">CONSULTORIA -> Detetive Particular</option>
                              <option value="76">CONSULTORIA -> Digitalizar documentos</option>
                              <option value="77">CONSULTORIA -> Economia e finanças</option>
                              <option value="79">CONSULTORIA -> Pesquisas em geral</option>
                              <option value="80">CONSULTORIA -> Produção de conteúdo</option>
                              <option value="81">CONSULTORIA -> Segurança do trabalho</option>
                              <option value="82">CONSULTORIA -> Tradutores</option>
                              <option value="83">DESIGN E TECNOLOGIA -> Animação</option>
                              <option value="84">DESIGN E TECNOLOGIA -> Apps para smartphone</option>
                              <option value="85">DESIGN E TECNOLOGIA -> Áudio e vídeo</option>
                              <option value="86">DESIGN E TECNOLOGIA -> Modelagem 2D e 3D</option>
                              <option value="87">DESIGN E TECNOLOGIA -> Convites</option>
                              <option value="88">DESIGN E TECNOLOGIA -> Criação de logos</option>
                              <option value="89">DESIGN E TECNOLOGIA -> Desenvolvimento de sites</option>
                              <option value="90">DESIGN E TECNOLOGIA -> Diagramador</option>
                              <option value="91">DESIGN E TECNOLOGIA -> Edição de fotos</option>
                              <option value="92">DESIGN E TECNOLOGIA -> Ilustração</option>
                              <option value="93">DESIGN E TECNOLOGIA -> Marketing online</option>
                              <option value="94">DESIGN E TECNOLOGIA -> Materiais promocionais</option>
                              <option value="96">DESIGN E TECNOLOGIA -> Produção gráfica</option>
                              <option value="97">DESIGN E TECNOLOGIA -> Web Design</option>
                              <option value="98">EVENTOS -> Animação de festas</option>
                              <option value="99">EVENTOS -> Assessor de eventos</option>
                              <option value="100">EVENTOS -> Bandas e cantores</option>
                              <option value="101">EVENTOS -> Bartenders</option>
                              <option value="102">EVENTOS -> Brindes e Lembrancinhas</option>
                              <option value="103">EVENTOS -> Buffet completo</option>
                              <option value="104">EVENTOS -> Churrasqueiro</option>
                              <option value="105">EVENTOS -> Decoração</option>
                              <option value="106">EVENTOS -> Djs</option>
                              <option value="107">EVENTOS -> Equipamentos para festas</option>
                              <option value="108">EVENTOS -> Fotografia</option>
                              <option value="109">EVENTOS -> Garçons e copeiras</option>
                              <option value="110">EVENTOS -> Gravação de vídeos</option>
                              <option value="112">EVENTOS -> Recepcionistas</option>
                              <option value="113">EVENTOS -> Segurança</option>
                              <option value="114">MODA E BELEZA -> Artesanato</option>
                              <option value="115">MODA E BELEZA -> Cabeleireiros  </option>
                              <option value="116">MODA E BELEZA -> Corte e costura</option>
                              <option value="117">MODA E BELEZA -> Depilação</option>
                              <option value="118">MODA E BELEZA -> Design de sobrancelhas</option>
                              <option value="119">MODA E BELEZA -> Esotérico</option>
                              <option value="120">MODA E BELEZA -> Esteticista</option>
                              <option value="121">MODA E BELEZA -> Manicure e pedicure</option>
                              <option value="122">MODA E BELEZA -> Maquiadores</option>
                              <option value="124">MODA E BELEZA -> Personal stylist</option>
                              <option value="125">MODA E BELEZA -> Sapateiro</option>
                              <option value="126">MODA E BELEZA -> Artesanato</option>
                              <option value="127">SAÚDE -> Acompanhante de idosos</option>
                              <option value="128">SAÚDE -> Enfermeira</option>
                              <option value="129">SAÚDE -> Fisioterapeuta</option>
                              <option value="130">SAÚDE -> Fonoaudiólogo</option>
                              <option value="131">SAÚDE -> Nutricionista</option>
                              <option value="132">SAÚDE -> Psicólogo</option>
                              <option value="133">SAÚDE -> Quiroprático</option>
                              <option value="134">SERVIÇOS DOMÉSTICOS -> Adestrador de cães</option>
                              <option value="135">SERVIÇOS DOMÉSTICOS -> Babá</option>
                              <option value="136">SERVIÇOS DOMÉSTICOS -> Cozinheira</option>
                              <option value="137">SERVIÇOS DOMÉSTICOS -> Diarista</option>
                              <option value="138">SERVIÇOS DOMÉSTICOS -> Limpeza de piscina</option>
                              <option value="139">SERVIÇOS DOMÉSTICOS -> Motorista</option>
                              <option value="141">SERVIÇOS DOMÉSTICOS -> Passadeira</option>
                              <option value="142">SERVIÇOS DOMÉSTICOS -> Passeador de cães</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <p><a class="btn btn-lg btn-primary" href="#" type="submit" role="button" id="pesquisar"><i class="glyphicon glyphicon-search"></i> Pesquisar</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>


      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner"  role="listbox" id="slide_100">
        <div class="item active" id="slide_100">
          <img class="first-slide" src="img/slide3_2.png"  id="img-responsive"  alt="First slide">
          <img class="first-slide" src="img/slide3.png"    id="img-responsive_1"  alt="First slide">

        </div>
        <div class="item" style="height: 100%;">
          <img class="second-slide"  class="img-responsive"  id="img-responsive" src="img/slide2_2.png" alt="Second slide">
            <img class="first-slide" src="img/slide2.jpg"  id="img-responsive_1" alt="First slide">

        </div>
        <div class="item" id="slide_100">
          <img class="third-slide"  class="img-responsive" id="img-responsive"  src="img/slide1_2.png" alt="Third slide">
            <img class="first-slide" src="img/slide1.png"  id="img-responsive_1" alt="First slide">

        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>


  <!-- banner -->
    <div  id="banner_mao" style="margin-top: -2px">
            <img src="img/bot-o-google-play-2.png" width="10%" id="botao_google">
            <img src="img/bot-o-app-store.png" width="10%" id="botao_apple" >
            <img src="img/banner_mao_2.png" id="banner_mao_imagem">

        <!--- banner para celular -->

            <div id="banner_mao_imagem_esconde" style="width: 100%; background: url('img/background_banner.jpg') no-repeat;background-position-x: -600px;background-color: #f7921c;">

               <!-- <img id="celular" src="img/celular_banner.png" style="height: 100%;width:75%;position: absolute" class="col-xs-12">-->

                <div class="col-xs-12">
                  <h1 style="color:#fff;text-align: center;font-weight: 900;;width: 100%">UORKE</h1>
                  <p style="color:#fff;text-align: center;font-size: 25px;margin-top:1%;width: 100%" >Você já imaginou ter acesso aos contatos?</p>
                 <p style="color:#fff;text-align: center;font-size: 25px;margin-top:-2%;width: 100%"> Baixe o aplicativo agora mesmo.</p>

                <div class="col-xs-12" ><center><img src="img/bot-o-google-play-2.png"  id="botao_google2"></center></div>
                <div class="col-xs-12 "  ><center><img src="img/bot-o-app-store.png"  id="botao_apple2" ></center></div>
                </div>
            </div>
    </div>

  <!-- como funciona -->
      <div id="seguro_parte_baixo" >



  <div id="como_funciona" >
  </div>
      <div class="container-fluid" id="cf">
          <p id="titulo_cf" style="margin-bottom: 30px">COMO FUNCIONA?</p>
          <div class="row" >
              <div class="col-sm-6 col-md-6 col-sm-12 col-xs-12 linha_divide" id="depo"  >
                  <div class="col-lg-12">
                      <img src="img/camada-14.png" class="cf_icones" width="100px">
                      <p class="texto_cf"><span style="color: #f7921c; font-weight: bold;font-size: 30px;">1</span>Realize uma pesquisa inserindo a categoria do profissional que procura, em seguida são apresentados os profissionais mais próximos de sua localização.</p>
                      <img src="img/notas.png" class="cf_icones"  width="100px">
                      <p class="texto_cf"><span style="color: #f7921c; font-weight: bold;font-size: 30px;">2</span>Selecione o profissional, você obterá os dados para contato negociando diretamente o serviço, avalie o profissional de acordo com sua satisfação.</p>
                     </div>
              </div>
              <p class="hidden-lg hidden-md hidden-sm col-xs-12">&nbsp;</p>
              <div class="hidden-lg hidden-md hidden-sm col-xs-12" >
                  <p><a class="btn btn-lg btn-primary" href="cadastrar_cliente.php" role="button" id="cadastrar_app">Cadastre-se como Contratante</a></p>
              </div>
              <p class="hidden-lg hidden-md hidden-sm col-xs-12">&nbsp;</p>
              <div class="col-sm-6 col-md-6 col-sm-12 col-xs-12" id="depo">
                  <div class="col-lg-12">
                      <img src="img/cel.png" class="cf_icones" width="60px">
                  <p class="texto_cf"><span style="color: #f7921c;font-weight: bold;font-size: 30px;">1</span>Realize o cadastro criando vantagens na busca por clientes, baixe e utilize o aplicativo gratuitamente para enviar e receber novas oportunidades.</p>
                      <img src="img/hand.png" class="cf_icones" width="100px">
                  <p class="texto_cf"><span style="color: #f7921c; font-weight: bold;font-size: 30px;">2</span>Preste os melhores serviços, satisfazendo seus clientes e buscando as melhores avaliações, amplie seus negócios com novos clientes.</p>
                  </div>
              </div>

          </div>
            <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
                <p><a class="btn btn-lg btn-primary" href="cadastrar_cliente.php" role="button" id="cadastrar_app">Cadastre-se como Contratante</a></p>
            </div>
          <p class="hidden-lg hidden-md hidden-sm col-xs-12">&nbsp;</p>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p><a class="btn btn-lg btn-primary" href="cadastrar.php" role="button" id="cadastrar_app">Cadastre-se como Profissional</a></p>
            </div>
      </div> <!-- fecha o tudo -->

          <p>&nbsp;</p>
          <p>&nbsp;</p>
  <!-- depoimentos -->
        <div id="depomentos">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 col-md-4" id="depo">
                        <p id="aspas" style="display: none">"</p>
                        <img src="img/foto_p.png" width="60%">
                        <h1 class="titulo_prof">João Filho - Pintor</h1>
                        <p class="sub_titulo_prof">Conseguiu comprar sua casa própria através da Uorke, hoje sou um pintor reconhecido na minha região, tudo isso graças a Uorke</p>
                    </div>
                    <div class="col-sm-4 col-md-4" id="depo">
                        <p id="aspas2" style="display: none">"</p>
                    <img src="img/foto_c.png" width="60%">
                    <h1 class="titulo_prof">José Almeida - Cliente</h1>
                    <p class="sub_titulo_prof">Depois que conheci o Uorke, tenho serviços de qualidade e profissionais a minha disposição a qualquer momento e em qualquer lugar.</p>
                </div>
                    <div class="col-sm-4 col-md-4" id="depo">
                        <p id="aspas3" style="display: none">"</p>
                        <img src="img/foto_j.png" width="60%">
                        <h1 class="titulo_prof">Júnior - jardineiro</h1>
                        <p class="sub_titulo_prof">Consegui comprar meu carro através da Uorke, hoje sou um profissional reconhecido na minha região.</p>
                    </div>
                </div>
            </div>
        </div>
  </div>



  </div>
  <!-- fecha depoimentos -->

  <!-- rodape -->
  <?php include 'rodape.php'; ?>
  <!--fecha rodape-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

  </body>
</html>
