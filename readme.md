# Projeto Uorke - Naville Brasil

Projeto de busca de profissionais para diversas necessidades.

  - Cadastro de Profissionais
  - Cadastro de Solicitantes
  - Solicitação de Profissional via Aplicativo

Status
----
Em desenvolvimento!

### Pré requisitos do ambiente

* [PHP >= 5.6](https://secure.php.net)
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension (--enable-mbstring)
* [NodeJS](https://nodejs.org)
* [Composer](https://getcomposer.org)
* [Bower](http://bower.io)
* [gulp](http://gulpjs.com/)
* [npm](https://www.npmjs.com/)
* [phpDocumentor](https://www.phpdoc.org/)

### Rodar o web service com o servidor embutido do PHP 7.0

```sh
  $ php artisan serve

  $php -S localhost:8000 -t public

  $php -S localhost:8000 -t public public/index.php
```

### Abrir no browser

Com o servidor embutido do PHP:
[http://localhost:8000](http://localhost:8000)

## Documentação

Acesse a pasta `Documents` para mais informações sobre o projeto.

## Autor

#### [Danilo Righetto](https://danilo-righetto.github.io/) - Analista de Sistemas
