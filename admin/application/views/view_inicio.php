<!-- div principal -->
<div class="row div-home">

    <div class="col-lg-12 col-md-12">
        <div class="col-lg-1 col-md-1"></div>
        <div class="col-lg-10 col-md-10">

            <div class="col-lg-12 col-md-12 mdl-grid">

                <!-- novos profissionais -->
                <div class="col-lg-3 col-md-3 div-primeira-linha-home">
                    <a href="<?php echo base_url() ?>main/redirecionar/15">
                        <img src="<?php echo base_url() ?>style/img/novos_profissionais.png" width="95%">
                        <div class="div-h4-home" style="background-color: #bc3902">
                            <h4 class=" h4-primeira-linha" align="center">NOVOS PROFISSIONAIS</h4>
                        </div>
                    </a>
                </div>

                <!-- encontrar profissionais -->
                <div class="col-lg-3 col-md-3 div-primeira-linha-home">
                    <a href="<?php echo base_url() ?>main/redirecionar/11">
                        <img src="<?php echo base_url() ?>style/img/encontrar_profissionais.png" width="95%">
                        <div class="div-h4-home-comunicado" style="background-color: #b54800">
                            <h4 class="h4-primeira-linha" align="center">ENCONTRAR PROFISSIONAIS</h4>
                        </div>
                    </a>
                </div>

                <!-- grupos -->
                <div class="col-lg-3 col-md-3 div-primeira-linha-home">
                    <a href="<?php echo base_url() ?>main/redirecionar/3">
                        <img src="<?php echo base_url() ?>style/img/grupos.png" width="95%">
                        <div class="div-h4-home-roteiro" style="background-color: #9e0303">
                            <h4 class=" h4-primeira-linha" align="center">GRUPOS</h4>
                        </div>
                    </a>
                </div>


                <!-- usuarios -->
                <div class="col-lg-3 col-md-3 div-primeira-linha-home">
                    <a href="<?php echo base_url() ?>main/redirecionar/2">
                        <img src="<?php echo base_url() ?>style/img/usuarios.png" width="95%">
                        <div class="div-h4-home-input" style="background-color: #1a801d">
                            <h4 class=" h4-primeira-linha" align="center">USUARIOS</h4>
                        </div>
                    </a>
                </div>






            </div>

        </div>
        <div class="col-lg-1 col-md-1"></div>
    </div>

    <div class="col-lg-12 col-md-12">
        <div class="col-lg-1 col-md-1"></div>
        <div class="col-lg-10 col-md-10">


            <div class="col-lg-12 col-md-12 mdl-grid">

                <!-- novos clientes -->
                <div class="col-lg-3 col-md-3 div-primeira-linha-home">
                    <a href="<?php echo base_url() ?>main/redirecionar/13">
                        <img src="<?php echo base_url() ?>style/img/novos_clientes.png" width="95%">
                        <div class="div-h4-home-destaque" style="background-color: #053c96">
                            <h4 class="h4-primeira-linha" align="center">NOVOS CLIENTES</h4>
                        </div>
                    </a>
                </div>



                <!-- encontrar clientes -->
                <div class="col-lg-3 col-md-3 div-primeira-linha-home">
                    <a href="<?php echo base_url() ?>main/redirecionar/10">
                        <img src="<?php echo base_url() ?>style/img/encontrar_clientes.png" width="95%">
                        <div class="div-h4-home-roteiro" style="background-color: #0755d4">
                            <h4 class=" h4-primeira-linha" align="center">ENCONTRAR CLIENTES</h4>
                        </div>
                    </a>
                </div>

                <!-- relatorios -->
                <div class="col-lg-3 col-md-3 div-primeira-linha-home">
                    <a href="<?php echo base_url() ?>main/redirecionar/">
                        <img src="<?php echo base_url() ?>style/img/relatorios.png" width="95%">
                        <div class="div-h4-home" style="background-color: #666666">
                            <h4 class=" h4-primeira-linha" align="center">RELATÓRIOS</h4>
                        </div>
                    </a>
                </div>

            </div>

        </div>
        <div class="col-lg-1 col-md-1"></div>
    </div>

</div>

<!-- div naville -->
<div class="col-lg-12 col-md-12" style="background-color: white; padding-top: .5%; padding-bottom: .5%; ">

    <div class="col-lg-8 col-md-8">

        <div class="col-lg-12 col-md-12" style="padding-top: 0.5%; padding-bottom: 0.5%">

            <div class="col-lg-9 col-md-9">
                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;Key Account: <b>Yago de Pinho Del'Labon</b><br />
                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;<a href="mailto:yago.pinho@keyaccount-br.naville.marketing" style="color: #7a0b09">yago.pinho@keyaccount-br.naville.marketing</a>
            </div>
            <div class="col-lg-3 col-md-3" style="border-left: 1px solid #b3b3b3">
                &nbsp;&nbsp;Tel.: <span style="font-size: 15px"><b>11 2688-1888</b></span><br />
                &nbsp;&nbsp;Cel.: <span style="font-size: 15px"><b>11 97713-2989</b></span>
            </div>

        </div>

    </div>
    <div class="col-lg-1 col-md-1"></div>
    <div class="col-lg-3 col-md-3" style="padding-top: 0.5%;">
        <a href="http://www.naville.marketing">
            <img src="<?php echo base_url() ?>style/img/logo_naville.png" width="65%" alt="" style="margin-top: 2%">
        </a>
    </div>

</div>

<style>

    @import url('https://fonts.googleapis.com/css?family=Open+Sans:300');

    .container{
        width: 100%;
    }

    .div-home{
        background-color: #f1f4f7;
        padding-top: 3%;
        padding-bottom: 4%;

    }

    .div-primeira-linha-home{
        display: inline-block;
        margin-bottom: 2%;
    }

    .div-h4-home-destaque{
        background-color: #1a801d;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .div-h4-home-comunicado{
        background-color: #b58f06;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .div-h4-home{
        background-color: #801008;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .div-h4-home-roteiro{
        background-color: #801008;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .div-h4-home-input{
        background-color: #055673;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

   

    a{
        text-decoration: none;
    }

    a:hover{
        text-decoration: none;
    }

    .h4-primeira-linha{
        color: white;
        font-family: 'Open Sans', sans-serif;
        font-size: 15px;
    }

    @media only screen and (max-width: 1200px){

        .h4-primeira-linha{
            font-size: 18px;
        }

    }

</style>