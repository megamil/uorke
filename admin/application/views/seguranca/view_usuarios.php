<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Usuários</h1>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/8">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Usuário
		</a>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th></th>
		<th>Nome</th>
		<th>E-mail</th>
		<th>Login</th>
		<th>Status</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais as $usuario) {
			echo '<tr>';
			echo '<td><a href="'.base_url().'main/redirecionar/6/'.$usuario->id_usuario.'"<button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></td>';
			echo '<td>'.$usuario->nome_usuario.'</td>';
			echo '<td>'.$usuario->email_usuario.'</td>';
			echo '<td>'.$usuario->login_usuario.'</td>';
			echo '<td>'.$usuario->ativo_usuario.'</td>';
			echo '</tr>';
		}

	?>
	</tbody>
</table>