<script type="text/javascript">
	history.replaceState({pagina: "lista_grupos"}, "Lista dos Grupos", "<?php echo base_url() ?>main/redirecionar/3");
</script>

<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Novo Grupo</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php echo form_open('controller_grupos/criar_grupo'); ?>

<div class="row">

	<div class="col-md-5">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_grupo">Nome do grupo</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="nome_grupo" name="nome_grupo" placeholder="Nome Grupo" aviso="Nome Grupo" value="<?php echo $this->session->flashdata('nome_grupo'); ?>">
		</div>
	</div>

	<div class="col-md-5">
		<div class="form-group has-feedback">
			<label class="control-label" for="descricao_grupo">Descrição do Grupo</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="descricao_grupo" name="descricao_grupo" placeholder="Descrição do Grupo" aviso="Descrição do Grupo" value="<?php echo $this->session->flashdata('descricao_grupo'); ?>">
		</div>
	</div>
</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição"> <i class="glyphicon glyphicon-floppy-disk"></i> Criar Grupo </button>
	</div>
</div>

<?php echo form_close(); ?>