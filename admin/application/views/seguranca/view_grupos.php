<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Grupos</h1>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/7">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Grupo
		</a>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th></th>
		<th>Nome Grupo</th>
		<th>Descrição Grupo</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais as $grupo) {
			echo '<tr>';
			echo '<td><a href="'.base_url().'main/redirecionar/5/'.$grupo->id_grupo.'"<button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></td>';
			echo '<td>'.$grupo->nome_grupo.'</td>';
			echo '<td>'.$grupo->descricao_grupo.'</td>';
			echo '</tr>';
		}

	?>
	</tbody>
</table>