<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACwzPtYB_taaPTgqbQYlKlf91Q78JOOQo&libraries=places"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(document).on('keyup','.localizacao_profissional',function(){

          geocoder = new google.maps.Geocoder();
          input = document.getElementById("localizacao_profissional");
          autocomplete = new google.maps.places.Autocomplete(input);
          localizacao_profissional = document.getElementById("localizacao_profissional").value;
          geocoder.geocode( { 'localizacao_profissional': localizacao_profissional }, function(r, s) {
	        if (s === google.maps.GeocoderStatus.OK) {
	          endereco = r[0].formatted_localizacao_profissional;
	          //console.log(endereco);
	        } else {
	          endereco = "Status: "+s;
	        }
          });


        });

    });

	history.replaceState({pagina: "lista_profissionais"}, "Lista dos Profissionais", "<?php echo base_url() ?>main/redirecionar/11");
</script>

<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Novo Profissional</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php echo form_open_multipart('controller_profissionais/criar_profissional'); ?>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="imagem">Foto</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="file" class="form-control obrigatorio" id="imagem" name="imagem" placeholder="Foto" aviso="Foto" value="<?php echo $this->session->flashdata('imagem'); ?>">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_profissional">Nome</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="nome_profissional" name="nome_profissional" placeholder="Nome" aviso="Nome" value="<?php echo $this->session->flashdata('nome_profissional'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="email_profissional">E-mail</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_email" id="email_profissional" name="email_profissional" placeholder="E-mail" aviso="E-mail" value="<?php echo $this->session->flashdata('email_profissional'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="telefone_profissional">Telefone</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio mascara_cel" id="telefone_profissional" name="telefone_profissional" placeholder="Telefone" aviso="Telefone" value="<?php echo $this->session->flashdata('telefone_profissional'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="senha_profissional">Senha</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="password" class="form-control obrigatorio" id="senha_profissional" name="senha_profissional" placeholder="Senha" aviso="Senha" value="<?php echo $this->session->flashdata('senha_profissional'); ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cep_profissional">CEP</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control mascara_cep obrigatorio" id="cep_profissional" name="cep_profissional" placeholder="CEP" aviso="CEP" value="<?php echo $this->session->flashdata('cep_profissional'); ?>">
		</div>
	</div>

	<div class="col-md-9">
		<div class="form-group has-feedback">
			<label class="control-label" for="localizacao_profissional">Endereço</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="localizacao_profissional" name="localizacao_profissional" placeholder="Endereço" aviso="Endereço" value="<?php echo $this->session->flashdata('localizacao_profissional'); ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="fk_sub_categoria">Profissão</label> 
			<select class="form-control obrigatorio" name="fk_sub_categoria" aviso="Profissão">
                <option>Selecione</option>
                <?php 

	                foreach ($dados_iniciais as $indice => $categoria) {
	                    echo '<option value="'.$categoria->id_sub_categoria.'">'.$categoria->categorias.'</option>';
	                }

                 ?>
            </select>
		</div>
	</div>

	<div class="col-md-9">
		<div class="form-group has-feedback">
			<label class="control-label" for="breve_descricao_profissional">Descrição</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<textarea type="text" class="form-control obrigatorio" id="breve_descricao_profissional" name="breve_descricao_profissional" placeholder="Descrição" aviso="Descrição"><?php echo $this->session->flashdata('breve_descricao_profissional'); ?></textarea>
		</div>
	</div>
</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição"> <i class="glyphicon glyphicon-floppy-disk"></i> Criar Profissional </button>
	</div>
</div>

<?php echo form_close(); ?>