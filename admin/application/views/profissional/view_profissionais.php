<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Profissionais</h1>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/15">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Profissional
		</a>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th></th>
		<th>Profissional</th>
		<th>E-mail</th>
		<th>Profissão</th>
		<th>Ranking</th>
		<th>Status</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais as $profissional) {
			echo '<tr>';
			echo '<td><a href="'.base_url().'main/redirecionar/14/'.$profissional->id_usuario.'"<button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></td>';
			echo '<td>'.$profissional->nome_usuario.'</td>';
			echo '<td>'.$profissional->email_usuario.'</td>';
			echo '<td>'.$profissional->sub_categoria.'</td>';

			if ($profissional->ranking == "" || $profissional->ranking == null) {
				$ranking = 0;
			} else {
				$ranking = $profissional->ranking;
			}

			echo '<td><span style="display:none">'.$ranking.'</span>';

			for ($i=0; $i < $ranking; $i++) { 
				echo '<i class="glyphicon glyphicon-star" style="color:#FFD700"></i>';
			}

			for ($i=0; $i < (5 - $ranking); $i++) { 
				echo '<i class="glyphicon glyphicon-star-empty" style="color:#FFD700"></i>';
			}

			echo '</td>';

			if ($profissional->ativo_usuario == 1) {
				echo '<td>Ativo</td>';
			} else {
				echo '<td>Inativo</td>';
			}
			
			echo '</tr>';
		}

	?>
	</tbody>
</table>