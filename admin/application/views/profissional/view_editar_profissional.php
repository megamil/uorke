<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACwzPtYB_taaPTgqbQYlKlf91Q78JOOQo&libraries=places"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(document).on('keyup','.localizacao_profissional',function(){

          geocoder = new google.maps.Geocoder();
          input = document.getElementById("localizacao_profissional");
          autocomplete = new google.maps.places.Autocomplete(input);
          localizacao_profissional = document.getElementById("localizacao_profissional").value;
          geocoder.geocode( { 'localizacao_profissional': localizacao_profissional }, function(r, s) {
	        if (s === google.maps.GeocoderStatus.OK) {
	          endereco = r[0].formatted_localizacao_profissional;
	          //console.log(endereco);
	        } else {
	          endereco = "Status: "+s;
	        }
          });


        });

        $('#fk_sub_categoria').val(<?php echo $this->session->flashdata('fk_sub_categoria'); ?>).trigger('change');

    });

</script>

<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Editar Perfil Profissional</h1>
	</div>
	<?php if($this->session->userdata('grupo') != 2): ?>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
	<?php endif; ?>
</div>
<hr>

<?php if (is_null($this->session->flashdata('fk_plano'))) {?>
	<?php echo form_open('controller_pagseguro/pagando'); ?>
		<div class="row">
			<div class="col-md-4">

				<select class="form-control obrigatorio" name="fk_plano" id="fk_plano">
					<?php foreach ($dados_iniciais['planos'] as $key => $plano)
						echo '<option value="'.$plano->id_plano.'">'.$plano->nome.' R$'.$plano->valor.'</option>';
					?>
				</select>
				
			</div>

			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="assinar" title="Assinar Plano"> Assinar </button>
			</div>

		</div>
	<?php echo form_close(); ?>
<?php } else {
          echo '<p style="font-size:20px;color: #f7921c;font-weight: bolder">';
			echo $this->session->flashdata('plano');
			echo '</p>';
			} ?>


<?php echo form_open_multipart('controller_profissionais/editar_profissional'); ?>

<!-- Modal Foto -->
<div class="modal fade" id="foto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Foto</h4>
      </div>
      <div class="modal-body" align="center">
        <img src="<?php echo base_url().'fotos/profissionais/foto_perfil_profissional_id_'.$this->session->flashdata('id_profissional');?>.png">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" style="background: #f7921c;padding: 10px 20px;text-shadow: 0 0 0 #000;border:none;" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Foto -->

<input type="hidden" name="id_profissional" value="<?php echo $this->session->flashdata('id_profissional'); ?>">

<div class="row">

    <style>
    </style>
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="imagem">Foto</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<div class="input-group">
				
				<input type="file" class="form-control" id="imagem" name="imagem" aviso="Foto">
	      		<span class="input-group-btn">
	        		<a class="btn btn-success botao_jean" type="button" data-toggle="modal" style="background: #f7921c;border-color: #f7921c;text-shadow: 0 0 0 #000;" data-target="#foto">Ver Atual</a>
	      		</span>

	      	</div>
		</div>
	</div>

	

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_profissional">Nome</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="nome_profissional" name="nome_profissional" placeholder="Nome" aviso="Nome" value="<?php echo $this->session->flashdata('nome_profissional'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="email_profissional">E-mail</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_email" id="email_profissional" name="email_profissional" placeholder="E-mail" aviso="E-mail" value="<?php echo $this->session->flashdata('email_profissional'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="telefone_profissional">Telefone</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio mascara_cel" id="telefone_profissional" name="telefone_profissional" placeholder="Telefone" aviso="Telefone" value="<?php echo $this->session->flashdata('telefone_profissional'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="senha_profissional">Senha</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="password" class="form-control" id="senha_profissional" name="senha_profissional" placeholder="Senha" aviso="Senha" value="">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cep_profissional">CEP</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control mascara_cep obrigatorio" id="cep_profissional" name="cep_profissional" placeholder="CEP" aviso="CEP" value="<?php echo $this->session->flashdata('cep_profissional'); ?>">
		</div>
	</div>

	<div class="col-md-9">
		<div class="form-group has-feedback">
			<label class="control-label" for="localizacao_profissional">Endereço</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="localizacao_profissional" name="localizacao_profissional" placeholder="Endereço" aviso="Endereço" value="<?php echo $this->session->flashdata('localizacao_profissional'); ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="fk_sub_categoria">Profissão</label> 
			<select class="form-control obrigatorio" name="fk_sub_categoria" id="fk_sub_categoria" aviso="Profissão">
                <option>Selecione</option>
                <?php 

	                foreach ($dados_iniciais['categorias'] as $indice => $categoria) {
	                    echo '<option value="'.$categoria->id_sub_categoria.'">'.$categoria->categorias.'</option>';
	                }

                 ?>
            </select>
		</div>
	</div>

	<div class="col-md-9">
		<div class="form-group has-feedback">
			<label class="control-label" for="breve_descricao_profissional">Descrição</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<textarea type="text" class="form-control obrigatorio" id="breve_descricao_profissional" name="breve_descricao_profissional" placeholder="Descrição" aviso="Descrição"><?php echo $this->session->flashdata('breve_descricao_profissional'); ?></textarea>
		</div>
	</div>
</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" style="background: #f7921c;border-color: #f7921c;text-shadow: 0 0 0 #000; id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição" style="background: #f7921c;border-color: #f7921c;text-shadow: 0 0 0 #000;> <i class="glyphicon glyphicon-floppy-disk"></i> Editar Profissional </button>
	</div>
</div>

<?php if($this->session->userdata('grupo') != 2): ?>

<div class="row">
	<div class="col-md-3">
		<h3>Avaliações</h3>
	</div>
</div>

<div class="row">
	<div class="col-md-12">

		<table class="table table-bordered table-hover" align="center">
			<thead align="center">
				<th>Cliente Atendido:</th>
				<th>Atendimento</th>
				<th>Avaliado</th>
				<th>Nota Atendimento</th>
				<th>Nota Organização</th>
				<th>Nota Pontualidade</th>
				<th>Nota Serviço</th>
				<th>Nota Simpatia</th>
				<th>Depoimento</th>
			</thead>
			<tbody align="center">	
			<?php 

				foreach ($dados_iniciais['avaliacoes'] as $avaliacoes) {
					echo '<tr>';
						echo '<td>'.$avaliacoes->nome_usuario.'</td>';
						echo '<td>'.$avaliacoes->data_atendimento.'</td>';
						echo '<td>'.$avaliacoes->data_avaliacao.'</td>';
						echo '<td>'.$avaliacoes->avaliacao_atendimento.'</td>';
						echo '<td>'.$avaliacoes->avaliacao_organizacao.'</td>';
						echo '<td>'.$avaliacoes->avaliacao_pontualidade.'</td>';
						echo '<td>'.$avaliacoes->avaliacao_servico.'</td>';
						echo '<td>'.$avaliacoes->avaliacao_simpatia.'</td>';
						echo '<td>'.$avaliacoes->depoimento_atendimento.'</td>';
					echo '</tr>';
				}

			?>
			</tbody>
		</table>

	</div>
</div>
<?php endif; ?>
<?php echo form_close(); ?>