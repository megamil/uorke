<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php

    if (isset($titulo_aplicacao)){

        echo '<title>'.$titulo_aplicacao.'</title>';

     } else {

        echo '<title>Encontre Profissional</title>';

     }

  ?>
  <link rel="shortcut icon" href="<?php echo base_url() ?>style/img/favicon.png">
	<link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/select2.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/estilo.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/plugin_tinymce.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/daterangepicker.css">


	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/bootstrap.min.js"></script>
  <!-- Aceita Mascaras nas listas -->
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.mask.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/script.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/sweetalert.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/select2.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/moment.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/daterangepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.priceformat.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/tinymce/tinymce.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/plugin_tinymce.js"></script>


  <!-- DataTable -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/DataTables/datatables.min.css"/>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/DataTables/DataTables/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/datatable.js"></script>

  <script type="text/javascript" DEFER="DEFER">
    /*Só é executado após o carregamento total da página*/
    $(document).ready(function(){

      $( "#load_geral" ).fadeOut( "slow", function() {
        $('.principal').show();
      });

    });
  </script>

  <!-- Usado quando o usuário desabilitou o JavaScript -->
  <noscript>
    <meta http-equiv="refresh" content=1;url="http://enable-javascript.com/pt/">
    <div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
      <h1 style="color: white;">HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.</h1>
      <iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

    </div>
  </noscript>

</head>
<body>

<?php

  if ($this->session->flashdata('tipo_alerta') != "") {
    echo "<script type=\"text/javascript\">
      $(document).ready(function(){
        $.toast({
            icon: '".$this->session->flashdata('tipo_alerta')."',
            heading: '".$this->session->flashdata('titulo_alerta')."',
            text: '".$this->session->flashdata('mensagem_alerta')."',
            showHideTransition: 'fade',
            position: 'top-right',
        });
      });
    </script>";

    $this->session->set_flashdata('tipo_alerta','');
    $this->session->set_flashdata('titulo_alerta','');
    $this->session->set_flashdata('mensagem_alerta','');

  }

?>

<?php if (isset($menu) && $this->session->userdata('grupo') == 1) { ?>

<nav class="navbar navbar-default navbar-fixed-top" style="background: url('bg.png');background-color: transparent !important">
  <div class="container-fluid" style="background: url('bg.png');background-color: transparent !important">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header" style="background: url('bg.png');background-color: transparent !important">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span  class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" style="margin-top: -7px;" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>style/img/logo_menu_uorke.png" width="130px"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-left menu_superior">
        <?php echo $menu; ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<?php } else { ?>
<style>
    .navbar-default .navbar-toggle .icon-bar{
        background: #fff;
        border:none;
    }
</style>
  <nav class="navbar navbar-default navbar-fixed-top"  style="height: 75px;background: url('<?php echo base_url(); ?>../img/bg_topo.png');background-color: transparent !important">
    <div class="container-fluid"  style="background: transparent;">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header" style="margin-left: 5%;margin-top: 15px">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span style="background: #fff" class="sr-only">Toggle navigation</span>
          <span  style="color: #fff" class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="margin-top: -7px;" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>style/img/logo_menu_uorke.png" width="130px"></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

          <ul class="nav navbar-nav navbar-right" style="margin-right: 200px;font-size: 20px;margin-top: 20px">
        <li>
          <a href="<?php echo base_url(); ?>/main/logout" style="color: white;">
            Sair
          </a>
        </li>      </ul>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

<?php } ?>

<!--Load para troca entre views-->
<div id="load_geral" align="center">
  <img src="<?php echo base_url() ?>style/img/loading.gif" id="img_load_geral">
</div>

<!--Div Principal-->
<div class="container principal" hidden="hidden">