<script type="text/javascript">
	history.replaceState({pagina: "lista_clientes"}, "Lista dos Clientes", "<?php echo base_url() ?>main/redirecionar/10");
</script>
<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Editar Cliente</h1>
	</div>
	<?php if ($this->session->userdata('grupo') != 3): ?>
		<div class="col-md-4" align="right">
			<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
			<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
		</div>
	<?php endif ?>
</div>
<hr>

<?php echo form_open_multipart('controller_cliente/editar_cliente'); ?>

<input type="hidden" name="id_cliente" value="<?php echo $this->session->flashdata('id_cliente'); ?>">

<!-- Modal Foto -->
<div class="modal fade" id="foto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Foto</h4>
      </div>
      <div class="modal-body" align="center">
        <img src="<?php echo base_url().'fotos/clientes/foto_perfil_cliente_id_'.$this->session->flashdata('id_cliente');?>.png">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Foto -->

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="imagem">Foto</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<div class="input-group">
				<input type="file" class="form-control" id="imagem" name="imagem" aviso="Foto">
	      		<span class="input-group-btn">
	        		<a class="btn btn-success" type="button" data-toggle="modal" data-target="#foto"">Ver Atual</a>
	      		</span>
	      	</div>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_cliente">Nome</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="nome_cliente" name="nome_cliente" placeholder="Nome" aviso="Nome" value="<?php echo $this->session->flashdata('nome_cliente'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="email_cliente">E-mail</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_email" id="email_cliente" name="email_cliente" placeholder="E-mail" aviso="E-mail" value="<?php echo $this->session->flashdata('email_cliente'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="telefone_cliente">Telefone</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio mascara_cel" id="telefone_cliente" name="telefone_cliente" placeholder="Telefone" aviso="Telefone" value="<?php echo $this->session->flashdata('telefone_cliente'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="senha_cliente">Senha</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="password" class="form-control" id="senha_cliente" name="senha_cliente" placeholder="Senha" aviso="Senha">
		</div>
	</div>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição"> <i class="glyphicon glyphicon-floppy-disk"></i> Editar Cliente </button>
	</div>
</div>

<?php echo form_close(); ?>