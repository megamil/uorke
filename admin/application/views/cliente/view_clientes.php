<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Clientes</h1>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/13">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Cliente
		</a>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th></th>
		<th>Nome</th>
		<th>E-mail</th>
		<th>Cadastrado em:</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais as $cliente) {
			echo '<tr>';
			echo '<td><a href="'.base_url().'main/redirecionar/12/'.$cliente->id_usuario.'"<button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></td>';
			echo '<td>'.$cliente->nome_usuario.'</td>';
			echo '<td>'.$cliente->email_usuario.'</td>';
			echo '<td>'.$cliente->data_cadastro_usuario.'</td>';
			echo '</tr>';
		}

	?>
	</tbody>
</table>