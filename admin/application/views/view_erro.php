<style type="text/css">
	.panel {
		margin-top: 50px;
	}
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$.toast({
		    heading: 'Erro 404',
		    text: 'Link inválido',
		    showHideTransition: 'fade',
		    position: 'top-right',
		    icon: 'error'
		});
	});
</script>

<div class="panel panel-danger">
	<div class="panel-heading"> 
		<h3 class="panel-title"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> Erro 404</h3> 
	</div>
	<div class="panel-body">
		<strong>Erro 404</strong> Página não encontrada. <br>
		<a href="<?php echo base_url(); ?>">Voltar</a>
	</div>
</div>