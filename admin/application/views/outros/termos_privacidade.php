<strong>REYNALDO APARECIDO MARQUES (“Encontre Profissional”)</strong> inscrita no CNPJ/MF sob o nº. 26.994.963/0001-41, é uma pessoa jurídica de direito privado prestadora de serviços de anúncios online, realizados entre promitentes contratantes (“Contratante”) e promitentes Prestadores de serviço (“Prestadores”), por meio do aplicativo e do site Encontre Profissional (“Plataforma”).
Por intermédio destes Termos e Condições Gerais de Uso (“Termos”), Encontre Profissional apresenta aos usuários em geral, aos Contratantes e aos Prestadores (todos em conjunto denominados “Usuários”) as condições essenciais para o uso dos serviços oferecidos na Plataforma.
Ao utilizar a Plataforma ou utilizar os serviços ofertados por Encontre Profissional, os Usuários aceitam e se submetem às condições destes Termos e às Políticas de Privacidade, bem como a todos os documentos anexos a estes.

<br>
<br>
<strong>OBJETO</strong>
<br>
<br>

Os serviços objeto dos presentes Termos consistem em:
Permitir aos Contratantes que utilizem a Plataforma para livremente e sem direcionamento ou interferência busquem orçamentos de Prestadores;
No Modelo de Assinatura, ofertar e hospedar espaços na Plataforma para que os Prestadores anunciem seus serviços (“Serviços”);
Viabilizar o contato direto entre Prestadores e Contratantes interessados em adquirir os Serviços, por meio da divulgação das informações de contato de uma parte à outra.
Encontre Profissional, portanto, possibilita que os Usuários contatem-se e negociem entre si diretamente, sem intervir no contato, na negociação ou na efetivação dos negócios, não sendo, nesta qualidade, fornecedor de quaisquer Serviços anunciados por seus Usuários na Plataforma.
Na qualidade de classificado de Serviços, Encontre Profissional não impõe ou interfere em qualquer negociação sobre condição, valor, qualidade, forma ou prazo na contratação entre os Contratantes e Prestadores, tampouco garante a qualidade, ou entrega dos Serviços contratados entre os Usuários.
Ao se cadastrar, o Usuário poderá utilizar todos os serviços disponibilizados na Plataforma disponíveis para sua região, declarando, para tanto, ter lido, compreendido e aceitado os Termos.

<br>
<br>
<strong>CAPACIDADE PARA CADASTRAR-SE:</strong>
<br>
<br>

Os serviços de Encontre Profissional estão disponíveis para pessoas físicas e pessoas jurídicas regularmente inscritas nos cadastros de contribuintes federal e estaduais que tenham capacidade legal para contratá-los. Não podem utilizá-los, assim, pessoas que não gozem dessa capacidade, inclusive menores de idade ou pessoas que tenham sido inabilitadas do Encontre Profissional, temporária ou definitivamente. Ficam, desde já, os Usuários advertidos das sanções legais cominadas no Código Civil.
É vedada a criação de mais de um cadastro por Usuário. Em caso de multiplicidade de cadastros elaborados por um só Usuário, Encontre Profissional reserva-se o direito de, a seu exclusivo critério e sem necessidade de prévia anuência dos ou comunicação aos Usuários, inabilitar todos os cadastros existentes e impedir eventuais cadastros futuros vinculados a estes.
Somente será permitida a vinculação de um cadastro a um CPF, um telefone e um e-mail, não podendo haver duplicidade de dados em nenhum caso.
Encontre Profissional pode unilateralmente excluir o cadastro dos Usuários.

<br><br>
<strong>CADASTRO</strong>
<br><br>

É necessário o preenchimento completo de todos os dados pessoais exigidos por Encontre Profissional no momento do cadastramento, para que o Usuário esteja habilitado a utilizar a Plataforma.
É de exclusiva responsabilidade dos Usuários fornecer, atualizar e garantir a veracidade dos dados cadastrais, não cabendo a Encontre Profissional qualquer tipo de responsabilidade civil e criminal resultante de dados inverídicos, incorretos ou incompletos fornecidos pelos Usuários.
Encontre Profissional se reserva o direito de utilizar todos os meios válidos e possíveis para identificar seus Usuários, bem como de solicitar dados adicionais e documentos que estime serem pertinentes a fim de conferir os dados pessoais informados.
Caso Encontre Profissional considere um cadastro, ou as informações nele contidas, suspeito de conter dados errôneos ou inverídicos, Encontre Profissional se reserva o direito de suspender, temporária ou definitivamente, o Usuário responsável pelo cadastramento, assim como impedir e bloquear qualquer publicidade ou cadastro de Serviços e cancelar anúncios publicados por este, sem prejuízo de outras medidas que entenda necessárias e oportunas. No caso de aplicação de quaisquer destas sanções, não assistirá aos Usuários direito a qualquer tipo de indenização ou ressarcimento por perdas e danos, lucros cessantes ou danos morais.
O Usuário acessará sua conta por meio de apelido (login) e senha, comprometendo-se a não informar a terceiros esses dados, responsabilizando-se integralmente pelo uso que deles seja feito.
O Usuário compromete-se a notificar Encontre Profissional imediatamente, por meio dos canais de contato mantidos por Encontre Profissional na Plataforma, a respeito de qualquer uso não autorizado de sua conta. O Usuário será o único responsável pelas operações efetuadas em sua conta, uma vez que o acesso só será possível mediante a utilização de senha de seu exclusivo conhecimento.
Em nenhuma hipótese será permitida a cessão, a venda, o aluguel ou outra forma de transferência da conta. Não se permitirá, ainda, a criação de novos cadastros por pessoas cujos cadastros originais tenham sido cancelados por infrações às políticas de Encontre Profissional.
Encontre Profissional se reserva o direito de, unilateralmente e sem prévio aviso, recusar qualquer solicitação de cadastro e de cancelar um cadastro previamente aceito.

<br><br>
<strong>Privacidade</strong>
<br><br>

Todas as suas informações pessoais recolhidas, serão usadas para o ajudar a tornar a sua visita no nosso site o mais produtiva e agradável possível.
A garantia da confidencialidade dos dados pessoais dos utilizadores do nosso site é importante para o Encontre Profissional.
Todas as informações pessoais relativas aos usuários, assinantes, clientes ou visitantes que usem o Encontre Profissional serão tratadas em concordância com a Lei da Proteção de Dados Pessoais de 26 de outubro de 1998 (Lei n.º 67/98).
A informação pessoal recolhida pode incluir o seu nome, e-mail, número de telefone e/ou telemóvel, morada, data de nascimento e/ou outros.
O uso do Encontre Profissional pressupõe a aceitação deste Acordo de privacidade. A equipa do Encontre Profissional reserva-se ao direito de alterar este acordo sem aviso prévio. Deste modo, recomendamos que consulte a nossa política de privacidade com regularidade de forma a estar sempre atualizado.

<br><br>
<strong>Os Cookies</strong>
<br><br>

Utilizamos cookies para armazenar informação, tais como as suas preferências pessoas quando visita o nosso website. Isto poderá incluir um simples popup.
Você detém o poder de desligar os seus cookies, nas opções do seu browser, ou efetuando alterações nas ferramentas de programas Anti-Virus, como o Norton Internet Security. No entanto, isso poderá alterar a forma como interage com o nosso website, ou outros websites. Isso poderá afetar ou não permitir que faça logins em programas.

<br><br>
<strong>MODELO DE ASSINATURA - SERVIÇOS</strong>
<br><br>

No Modelo de Assinatura, o Prestador terá visibilidade para o contratante que busca um Prestador dentro de suas necessidades um determinado profissional na sua respectiva categoria. Este prestador pode disponibilizar em seu perfil, textos, descrições, fotos e outras informações relevantes do Serviço oferecido, sempre que tal prática não viole nenhum dispositivo deste Termo ou das demais políticas de Encontre Profissional.
Encontre Profissional não verifica a veracidade das informações dos Serviços oferecidos pelos Prestadores, tampouco se responsabiliza pela acuidade das informações prestadas.



<br><br>
<strong>TARIFAS - MODELO DE ASSINATURA</strong>
<br><br>

O cadastramento dos Usuários no Encontre Profissional é gratuito.
No modelo de assinatura, o Encontre Profissional disponibilizará aos Prestadores um valor de assinatura onde o Usuário ao procurar um prestador de serviço em sua região somente os Prestadores que pagam suas assinaturas serão disponibilizados pelo aplicativo.

<br><br>
<strong>POLÍTICA DE CANCELAMENTO - MODELO DE ASSINATURA</strong>
<br><br>

No Modelo de Assinatura, o Prestador poderá cancelar o plano contratado a qualquer momento pelos e-mails e telefones disponibilizados no site.
Em todos os casos, será devolvido o valor proporcional ao tempo transcorrido do plano contratado, considerada a data da contratação até o dia do efetivo cancelamento do plano, descontada multa no valor de 20% (vinte por cento) do saldo remanescente em favor de Encontre Profissional
O Prestador que não deseje realizar a renovação automática do Modelo de Assinatura deverá, com antecedência mínima de 3 dias úteis, informar à Encontre Profissional que não deseja efetuar a renovação. Após esse período, será cobrada multa no valor de 20% do valor contratado em razão da rescisão.
<br><br>
<strong>OBRIGAÇÕES DOS USUÁRIOS</strong>
<br><br>
O Prestador deve ter capacidade legal para prestar o Serviço.
Em virtude de Encontre Profissional não figurar como parte nas transações de contratação dos Serviços que se realizam entre os Usuários, a responsabilidade por todas as obrigações delas decorrentes, sejam fiscais, trabalhistas ou de qualquer outra natureza, será exclusivamente do Contratante, do Prestador ou de ambos, conforme o caso. Na hipótese de interpelação judicial que tenha como Réu Encontre Profissional, cujos fatos fundem-se em ações do Prestador, este será chamado ao processo, devendo arcar com todos os ônus que daí decorram, incluindo despesas com taxas, emolumentos, acordos, honorários advocatícios entre outros. Por não figurar como parte nas transações que se realizam entre os Usuários, Encontre Profissional também não pode obrigar os Usuários a honrarem suas obrigações ou a efetivarem a negociação.
O Prestador deverá ter em mente que, na medida em que atue como um fornecedor de serviços, sua oferta o vincula, nos termos do artigo 30 do Código de Defesa do Consumidor e do artigo 429 do Código Civil, cujo cumprimento poderá ser exigido judicialmente pelo Contratante
Encontre Profissional não se responsabiliza pelas obrigações tributárias que recaiam sobre as atividades dos Usuários. Assim como estabelece a legislação pertinente em vigor, o Contratante deverá exigir nota fiscal do Prestador em suas transações. O Prestador, nos moldes da lei vigente, responsabilizar-se-á pelo cumprimento da integralidade das obrigações oriundas de suas atividades, notadamente aquelas referentes a tributos incidentes.
<br><br>
<strong>RESPONSABILIDADES</strong>
<br><br>
Encontre Profissional não se responsabiliza por vícios ou defeitos técnicos e/ou operacionais oriundos do sistema do Usuário ou de terceiros.
Encontre Profissional não é responsável pela entrega dos Serviços anunciados pelos Prestadores na Plataforma.
Encontre Profissional tampouco se responsabiliza pela existência, quantidade, qualidade, estado, integridade ou legitimidade dos Serviços oferecidos ou contratados pelos Usuários, assim como pela capacidade para contratar dos Usuários ou pela veracidade dos dados pessoais por eles fornecidos. Encontre Profissional, por não ser proprietária, depositante ou detentora dos produtos oferecidos, não outorga garantia por vícios ocultos ou aparentes nas negociações entre os Usuários. Cada Usuário conhece e aceita ser o único responsável pelos Serviços que anuncia ou pelas ofertas que realiza.
Encontre Profissional não será responsável por ressarcir seus Usuários por quaisquer gastos com ligações telefônicas, pacotes de dados, SMS, mensagens, emails, correspondência ou qualquer outro valor despendido pelo Usuário em razão de contato com Encontre Profissional ou quaisquer outros Usuário, por qualquer motivo que o seja.
Encontre Profissional não poderá ser responsabilizada pelo efetivo cumprimento das obrigações assumidas pelos Usuários. Os Usuários reconhecem e aceitam que, ao realizar negociações com outros Usuários, fazem-no por sua conta e risco, reconhecendo o Encontre Profissional como mero fornecedor de serviços de disponibilização de espaço virtual para que contratante possa localizar prestadores ao seu redor.
Em nenhum caso Encontre Profissional será responsável pelo lucro cessante ou por qualquer outro dano e/ou prejuízo que o Usuário possa sofrer devido às negociações realizadas ou não realizadas por meio de Plataforma, decorrentes da conduta de outros Usuários.
Por se tratar de negociações realizadas por meio eletrônico entre dois Usuários que não se conheciam previamente à negociação, Encontre Profissional recomenda que toda transação seja realizada com cautela e prudência.
Caso um ou mais Usuários ou algum terceiro inicie qualquer tipo de reclamação ou ação legal contra outro ou outros Usuários, todos e cada um dos Usuários envolvidos nas reclamações ou ações eximem de toda responsabilidade Encontre Profissional e seus diretores, gerentes, empregados, agentes, operários, representantes e procuradores.