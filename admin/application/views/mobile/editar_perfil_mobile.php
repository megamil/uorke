<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACwzPtYB_taaPTgqbQYlKlf91Q78JOOQo&libraries=places"></script>

<script type="text/javascript">
    $(document).ready(function(){

        <?php 

        if (isset($alerta)) {
            echo 'alert(\''.$alerta.'\')';
        }

         ?>

        $(document).on('keyup','.address',function(){

            geocoder = new google.maps.Geocoder();

          //console.log($(this).attr('cod'));

          input = document.getElementById("address");
          autocomplete = new google.maps.places.Autocomplete(input);
          address = document.getElementById("address").value;
          geocoder.geocode( { 'address': address }, function(r, s) {
            if (s === google.maps.GeocoderStatus.OK) {
              endereco = r[0].formatted_address;
              //console.log(endereco);
            } else {
              endereco = "Status: "+s;
            }
          });


        });

    });
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #e2e6f5; ">
            <h3 class="text-center">EDITE SEU PERFIL</h3>
            <div class="col-lg-1"></div>
            <div class="col-lg-10" style="border-top: 1px solid black">

                <?php echo form_open_multipart('Controller_profissionais/alterar_perfil'); ?>

                <input type="hidden" name="id_profissional" value="<?php echo $usuario->id_profissional; ?>">

                    <div align="center">
                        <label for="imagem">Foto</label>
                        <br>
                        <img style="margin-left: auto; margin-right: auto" src="<?php echo base_url(); ?>fotos/profissionais/foto_perfil_profissional_id_<?php echo $usuario->id_profissional; ?>.png" width="200px" heigth="200px">
                        <br>
                        <br>
                        <input class="form-control" type="file" id="imagem" name="imagem">
                        <br>
                    </div>

                    <hr>

                    <!-- nome -->
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control obrigatorio" placeholder="Dígite seu nome" name="nome" id="nome" value="<?php echo $usuario->nome_profissional; ?>" aviso="Nome"><br />

                    <!-- email -->
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control obrigatorio validar_email" placeholder="Ex: nome@email.com" name="email" id="email" value="<?php echo $usuario->email_profissional; ?>" aviso="E-mail"><br />

                    <!-- telefone -->
                    <label for="telefone">Telefone</label>
                    <input type="tel" class="form-control obrigatorio mascara_tel" placeholder="(__) _____-____" name="telefone" id="telefone" value="<?php echo $usuario->telefone_profissional; ?>" aviso="Telefone"><br />

                     <!-- Senha -->
                    <label for="senha">Senha</label>
                    <input type="password" class="form-control" placeholder="******" name="senha" id="senha"><br />

                    <!-- localizacao -->
                    <label for="localizacao">Localização</label>
                    <input type="text" class="form-control obrigatorio address" placeholder="Dígite sua localização" name="localizacao" id="address" value="<?php echo $usuario->localizacao_profissional; ?>" aviso="Localização"><br />

                    <!-- cep -->
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control obrigatorio mascara_cep" placeholder="Dígite seu cep" name="cep" id="cep" style="width: 40%" value="<?php echo $usuario->cep_profissional; ?>" aviso="CEP"><br />

                    <!-- qual sua profissao -->
                    <label for="">Qual sua profissão</label>
                    <select class="form-control obrigatorio" name="subcategoria" aviso="Profissão">
                        <option>Selecione</option>
                        <?php 

                        foreach ($categorias as $indice => $categoria) {
                            if ($categoria->id_sub_categoria == $usuario->fk_sub_categoria) {
                                echo '<option value="'.$categoria->id_sub_categoria.'" selected>'.$categoria->categorias.'</option>';
                            } else {
                                echo '<option value="'.$categoria->id_sub_categoria.'">'.$categoria->categorias.'</option>';
                            }
                            
                        }

                         ?>
                    </select><br />

                    <!-- informacoes adicionais -->
                    <label for="">Informações adicionais</label>
                    <textarea class="form-control obrigatorio" name="descricao" rows="3" placeholder="Deixe uma breve descrição" aviso="Descrição"><?php echo $usuario->breve_descricao_profissional; ?></textarea><br />

                    <!-- enviar -->
                    <button type="submit" class="btn btn-success center-block" id="validar_Enviar" style="width: 100%">CONFIRMAR</button>
                </form>

            </div>
            <div class="col-lg-1"></div>
        </div>