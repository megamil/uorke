<!DOCTYPE html>
<html lang="pt-br">
<head>
<title>UORKE</title>
    <meta name="theme-color" content="#f7921c">
	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://devnaville-br2.16mb.com/uorke/img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/bt2.css" rel="stylesheet">
    <link href="css/bt3.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">

    <style>
        body{
            background: #fff;
        }
        .textos{
            width: 30%;
            text-align: center;
            margin: 0 auto;
            font-size: 18px;
        }
        .textos_titulos{
            font-weight: bolder;
            text-align: center;
            padding: 1% 0;
            width: 100%;
            margin: 0 auto;
            font-size: 23px;
        }
        .t2{
        }
        .t1{
           width: 80%;
            border-radius: 8px;
            padding: 5px 0;
            border:1px solid #000;
            text-align: center;
            background: none;
        }
        #spc{
            min-height: 100%;
            width: 100%;
            position: absolute;
            padding:  5% 2% ;
            margin-top: -50px;
            background-size: cover;
        }
        #pagar{
            width: 700px;
            margin: 0 auto;
            padding: 20px;
            text-align: center;
            border-radius: 8px;
        }
        .assisnar{
            width: 50%;
        }
        @media screen and (max-width: 650px){
            #pagar{
                width: 100%;
            }
            .assisnar{
                width: 70%;
            }
        }
    </style>

</head>
 <body>
    <script>
        function gerarToken(){
           	var brand = null;
			
            var sessao_id = '<?php echo $id;?>';
            var numero = document.getElementById('cartao_numero').value;
            var codigo = document.getElementById('cartao_codigo').value;
            var mes = document.getElementById('cartao_mes').value;
            var ano = document.getElementById('cartao_ano').value;
            var bin = document.getElementById('cartao_numero').value.substr(0,6);
            
            try{
                PagSeguroDirectPayment.setSessionId(sessao_id);
                document.getElementById('hash_card').value = PagSeguroDirectPayment.getSenderHash();
                PagSeguroDirectPayment.getBrand({
                        cardBin: bin,
                        success: function(response) {
                            brand = response.brand;
                            document.getElementById('cartao_bandeira').value = brand.name;
                            var param = {
                                            cardNumber: numero,
                                            cvv: codigo,
                                            expirationMonth: mes,
                                            expirationYear: ano,
                                            success: function(response) {
                                                 document.getElementById('token').value = response.card.token;
                                            },
                                            error: function(response) {
                                                alert("Erro ao gerar token");
                                            },
                                            complete: function(response) {}
                                        }
                            param.brand = brand.name;
                            PagSeguroDirectPayment.createCardToken(param);
                        },
                        error: function(response) {
                            alert("Erro ao pegar bandeira");
                        },
                        complete: function(response) {}
                });
            }
            catch(err) {
                alert(err.message+"snbcjhdv");
            }
        }
    </script>
 <?php echo form_open('controller_pagseguro/pagar'); ?>
    <div id="tudo">
        <div id="spc">
            <div id="pagar">
                <input type="hidden" name="nome" id="nome" value="<?php echo $prof['nome_usuario']; ?>">
                <input type="hidden" name="documento" id="documento" value="<?php echo $prof['cpf_profissional']; ?>">
                <input type="hidden" name="tipo_documento" id="tipo_documento" value="CPF">
                <input type="hidden" name="ddd" id="ddd" value="<?php echo substr($prof['telefone_usuario'],0,2); ?>">
                <input type="hidden" name="telefone" id="telefone" value="<?php echo substr($prof['telefone_usuario'],2); ?>">
                <input type="hidden" name="email" id="email" value="<?php echo $prof['email_usuario']; ?>">
                <input type="hidden" name="logradouro" id="logradouro" value="<?php echo $prof['localizacao']; ?>">
                <input type="hidden" name="numero" id="numero" value="<?php echo $prof['numero']; ?>">
                <input type="hidden" name="bairro" id="bairro" value="<?php echo $prof['bairro']; ?>">
                <input type="hidden" name="cep" id="cep" value="<?php echo $prof['cep']; ?>">
                <input type="hidden" name="cidade" id="cidade" value="<?php echo $prof['cidade']; ?>">
                <input type="hidden" name="estado" id="estado" value="<?php echo $prof['estado']; ?>">
                <input type="hidden" name="pais" id="pais" value="<?php echo ($prof['pais'] == 'BR') ? 'BRA' : $prof['pais']; ?>">
                <input type="hidden" name="hash_card" id="hash_card">
                <input type="hidden" name="token" id="token">
                <input type="hidden" name="id_plano" id="id_plano" value="<?php echo $plano['id_plano']; ?>">
                <input type="hidden" name="code_plano" id="code_plano" value="<?php echo $plano['code']; ?>">
                <input type="hidden" name="id_profissional" id="id_profissional" value="<?php echo $prof['id_usuario']; ?>">

                <h3>Pagamento</h3
                <p>&nbsp;</p>
                <label>Numero do cartão </label><br>
                <input type="text" class="t1" name="cartao_numero" id="cartao_numero" maxlength="16">
                </br>
                </br>
                <label>Código </label><br>
                <input type="tel" class="t1" maxlength="3" name="cartao_codigo" id="cartao_codigo">
                </br>
                </br>
                <label>Mês de validade</label><br>
                <input type="text" class="t1" name="cartao_mes" id="cartao_mes" maxlength="2">
                </br>
                <label>Ano </label><br>
                <input type="text" class="t1" name="cartao_ano" id="cartao_ano" maxlength="4">
                </br>
                <label>Bandeira</label><br>
                <input type="text" class="t1" name="cartao_bandeira" id="cartao_bandeira" onfocus="gerarToken()" readonly>
                </br>
                </br>
                <label>Nome do titular do cartão</label><br>
                <input type="text" class="t1" name="cartao_nome" id="cartao_nome" maxlength="100">
                </br>
                </br>
                <label>Nascimento do titular do cartão</label><br>
                <input type="text" class="t1" name="dtNasc" id="dtNasc">
                </br>
                </br>
                <label>DDD do titular do cartão</label><br>
                <input type="text" class="t1" name="cartao_ddd" id="cartao_ddd" maxlength="2">
                </br>
                </br>
                <label>Telefone do titular do cartão</label><br>
                <input type="text" class="t1" name="cartao_telefone" id="cartao_telefone" maxlength="15">
                </br>
                </br>
                <label>CPF do titular do cartão</label><br>
                <input type="text" class="t1" name="cartao_cpf" id="cartao_cpf" maxlength="14">
                </br>
                </br>
                <button  class="btn btn-lg btn-primary assisnar" type="submit" value="Assisnar"  style="border:none;background: none"  type="submit"  ><img src="http://devnaville-br2.16mb.com/uorke/img/pagseguro.png" style="width:100%;"></button>
            </div>
        </div>
    </div>
<?php  echo form_close(); ?>
</body>

<script type="text/javascript">
    /* Máscaras ER */
    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        return v;
    }
    function mtel3(cpf){
        cpf=cpf.replace(/\D/g,"");
        cpf=cpf.replace(/(\d{4})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{4})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{4})(\d{1,4})$/,"$1.$2");

        return cpf;
    }
    function mtel2(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        return v;
    }
    function mtel4(v){
        v=v.replace(/\D/g,"");
        v=v.replace(/^(\d{2})(\d)/g,"($1)"); //Coloca parênteses em volta dos dois primeiros dígitos
        return v;
    }

    function mtel5(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
    function mtel6(cpf){
        cpf=cpf.replace(/\D/g,"");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return cpf;
    }

    function id( el ){
        return document.getElementById( el );
    }
    window.onload = function(){
        id('cartao_codigo').onkeyup = function(){
            mascara( this, mtel );
        }
      
        id('cartao_mes').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('cartao_ano').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('cartao_ddd').onkeyup = function(){
            mascara( this, mtel4 );
        }
        id('cartao_telefone').onkeyup = function(){
            mascara( this, mtel5 );
        }
        id('cartao_cpf').onkeyup = function(){
            mascara( this, mtel6 );
        }
    }



</script>