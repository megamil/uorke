<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">

	<head>
		
		<title>Login</title>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>style/img/favicon.png">
		<meta charset="utf-8">
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo base_url(); ?>style/css/login.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>

	</head>





	<body style="background-image: url('<?php echo base_url(); ?>style/img/bg_cadastro.png');
                                         background-size: 100%;
                                         background-position: center;
                                         background-repeat: no-repeat">

		<form method="post" action="<?php echo base_url(); ?>main/login">

			<div class="container">

				<div id="login">

					<div class="row">
						<div class="col-md-6" align="center">
                            <img src="<?php echo base_url(); ?>style/img/logo_menu_uorke.png" width="300px">
                        </div>
						
						<div class="col-md-6" >

						</div>
					</div>

					<div class="row central">

						<div class="col-md-6" align="center">
                            <div class="form-group" >
                                <input type="text" class="form-control" style="width: 40%" id="usuario" name="usuario" placeholder="Usuário" autofocus>
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control" style="width: 40%" id="senha" name="senha" placeholder="Senha">
                            </div>

                            <br />
                                <button class="btn" style="background-color: #3a77d0; color: white; width: 40%; margin-bottom: 10px;">ENTRAR</button><br />
                            <a href="#" id="esqueciSenha" style="text-shadow: 1px 1px 1px black">Esqueci minha senha</a>
                        </div>

						<div class="col-md-6" >
							


						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-md-4" align="center">
						<img src="<?php echo base_url() ?>style/img/loading.gif" id="img_load" width="100px" hidden>
					</div>
				</div>

			</div>

			



		</form>

	</body>

	<?php 
	//Caso a tentativa de login falhe
	if(isset($falha)){
		echo "<script type=\"text/javascript\">
				$(document).ready(function(){
					$.toast({
					    heading: 'Falha no login',
					    text: [
					        'Usuário ou senha inválidos', 
					        'Ou usuário inativo'
					    	],
					    showHideTransition: 'fade',
					    position: 'top-right',
					    icon: 'error'
					});
				});
			</script>";
	}

	//Saiu do sistema.
	if(isset($saiu)){
		echo "<script type=\"text/javascript\">
				$(document).ready(function(){
					$.toast({
					    heading: 'Até logo ".$saiu."!',
					    text: 'Login encerrado.',
					    showHideTransition: 'fade',
					    position: 'top-right',
					    icon: 'success'
					});
				});
			</script>";
	}

	?>

	<script type="text/javascript">
		$(document).ready(function(){

			$('#esqueciSenha').click(function(){

				if ($('#usuario').val() != "") {

					$('#login').hide();
					$('#img_load').show();

					$.ajax({
				        url: "<?php echo base_url(); ?>controller_usuarios/esqueci_Senha",
				        type: "post",
				        data: {login: $("#usuario").val()},
				      	datatype: 'json',
				        success: function(data){

				        	if (data['status'] == 1) {
				        		$.toast({
								    heading: 'Senha alterada com sucesso!',
								    text: data['resultado'],
								    showHideTransition: 'fade',
								    position: 'top-right',
								    icon: 'success'
								}); 
				        	} else {

				        		console.log(data['resultado']);

				        		$.toast({
								    heading: 'Senha não alterada',
								    text: "Falha ao enviar e-mail.",
								    showHideTransition: 'fade',
								    position: 'top-right',
								    icon: 'error'
								}); 

				        	}

					         
					        $('#login').show();
							$('#img_load').hide();
				         

				        },
				        error:function(){

					        $.toast({
							    heading: 'Falha',
							    text: 'Falha ao alterar senha.',
							    showHideTransition: 'fade',
							    position: 'top-right',
							    icon: 'error'
							}); 

							$('#login').show();
							$('#img_load').hide();
				            
				        }   
				      });

				} else {
					$.toast({
					    heading: 'Digite o usuário.',
					    text: 'Usuário em branco.',
					    showHideTransition: 'fade',
					    position: 'top-right',
					    icon: 'error'
					});
				}

			});

			$(function () {
			  $('[data-toggle="tooltip"]').tooltip()
			})

		});
	</script>

</html>