<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_clientes extends CI_Model {

		function __construct() {
		    parent::__construct();
		}

		public function view_clientes(){
			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query("SELECT 
										id_usuario,
										nome_usuario,
										email_usuario,
										date_format(data_cadastro_usuario, '%d/%m/%Y as %H:%i:%s') as data_cadastro_usuario
										FROM seg_usuarios where fk_grupo_usuario = 3;")->result();

		}

		public function novoCliente($valores = null){

			$this->db->insert('seg_usuarios',$valores);
			return $this->db->insert_id();

		}

		public function atualizarCliente($valores = null){
			$this->db->where(array('id_usuario' => $valores['id_usuario']));
			return $this->db->update('seg_usuarios',$valores);
		}

		public function view_editar_cliente($where = null){

			$cliente = $this->db->get_where('seg_usuarios', array('id_usuario' => $where[0]))->row();

			if (isset($cliente)) {
				$this->session->set_flashdata('id_cliente',             $cliente->id_usuario);
				$this->session->set_flashdata('nome_cliente',           $cliente->nome_usuario);
				$this->session->set_flashdata('email_cliente',          $cliente->email_usuario);
				$this->session->set_flashdata('telefone_cliente',       $cliente->telefone_usuario);
				$this->session->set_flashdata('senha_cliente',          $cliente->senha_usuario);
			}

			return array('status' => 'ok');

		}

	}