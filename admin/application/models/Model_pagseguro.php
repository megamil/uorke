<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_pagseguro extends CI_Model {

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();
			    return false;
			} else {
			    $this->db->trans_commit();
			    return true;
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
			return false;
		}

		############################### Querys ###############################

		public function inserePagamento($valores = null) {

			$this->db->insert('cad_pagamentos',$valores);
			return $this->db->insert_id();
		}

		public function insereNotificacao($valores = null) {

			$this->db->insert('cad_notificacao',$valores);
			return $this->db->insert_id();
		}

		public function updatePagamento($valores = null) {
			$this->db->where(array('id_pagamento' => $valores['id_pagamento']));
			return $this->db->update('cad_pagamentos',$valores);
		}

		public function getProfissional($id_usuario = null) {
			return $this->db->query('select * from seg_usuarios where id_usuario = '.$id_usuario)->row_array();
		}

		public function getPlano($id_plano = null) {
			return $this->db->query('select * from cad_planos where id_plano = '.$id_plano)->row_array();
		}

		public function getAssinaturas($id_profissional = null) {
			return $this->db->query('select * from cad_pagamentos where fk_profissional = '.$id_profissional)->result_array();
		}

		public function getAssinaturasAtivas() {
			return $this->db->query('select cpg.id_pagamento, cpg.data_pagamento, cpg.fk_plano, cpl.nome, cpg.code_assinatura, cpg.fk_profissional, su.nome_usuario, cpg.fk_status from cad_pagamentos cpg left join cad_planos cpl on (cpg.fk_plano = cpl.id_plano) left join seg_usuarios su on (cpg.fk_profissional = su.id_usuario) where cpg.fk_status = 1')->result_array();
		}

		public function getAssinaturaAtiva($id_profissional = null) {
			return $this->db->query('select * from cad_pagamentos where fk_status = 1 and fk_profissional = '.$id_profissional)->result_array();
		}

		public function getPlanos() {
			return $this->db->query('select * from cad_planos')->result_array();
		}
	}