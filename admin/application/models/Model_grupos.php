<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_grupos extends CI_Model {

		function __construct() {
		    parent::__construct();
		}

		// Lista dos grupos
		public function view_grupos(){
			return $this->db->get('seg_grupos')->result();
		}

		public function view_editar_grupo($where = null){
			
			$grupo = $this->db->get_where('seg_grupos', array('id_grupo' => $where[0]))->row();

			if (isset($grupo)) {
				$this->session->set_flashdata('id_grupo',$grupo->id_grupo);
				$this->session->set_flashdata('nome_grupo',$grupo->nome_grupo);
				$this->session->set_flashdata('descricao_grupo',$grupo->descricao_grupo);
			}

			//Para redirecionar corretamente quando não existem outros conteúdos a serem retornados
			return array('ok' => true);

		}

		public function update($valores = null){
			$this->db->where(array('id_grupo' => $valores['id_grupo']));
			return $this->db->update('seg_grupos',$valores);

		}

		public function create($valores = null){

			$this->db->insert('seg_grupos',$valores);
			return $this->db->insert_id();

		}


	}