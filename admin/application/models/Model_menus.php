<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_menus extends CI_Model {

		function __construct()
		{
		    parent::__construct();
		    
		}

		public function get_menus($where = "") {

			return $this->db->query('select titulo_menu as titulo, id_menu as id, menu_acima as anterior from seg_menu where menu_acima '.$where.' order by id')->result();

		}

		public function get_lista_aplicacoes($id = null,$grupo = null){

			return $this->db->query('select sa.* from seg_aplicacao sa
									inner join seg_aplicacoes_menu as sam on sam.fk_aplicacao = id_aplicacao
									inner join seg_menu on fk_menu = id_menu
									inner join seg_aplicacoes_grupos as sag on sag.fk_aplicacao = id_aplicacao
									where menu_acima = '.$id.' and fk_grupo = '.$grupo)->result();

		}

		public function get_link($id = null,$grupo = null) { //VALIDAR ACESSO DO USUÁRIO********

			$consulta = $this->db->query('select 
											m.titulo_menu,
											a.link_aplicacao,
											a.id_aplicacao
											from seg_aplicacoes_menu am
											inner join seg_menu m on m.id_menu = am.fk_menu
											inner join seg_aplicacao a on a.id_aplicacao = am.fk_aplicacao
											inner join seg_aplicacoes_grupos as sag on sag.fk_aplicacao = id_aplicacao
											where fk_menu = '.$id.' and fk_grupo = '.$grupo.'
											group by titulo_menu, link_aplicacao, id_aplicacao');

			if($consulta->num_rows() > 0) { // não tem sub-menu, é um link direto

				return $consulta;

			} else { //Tem sub-menu, deve chamar o else e iniciar a recursividade, se necessário.

				return false;

			}

		}


	}