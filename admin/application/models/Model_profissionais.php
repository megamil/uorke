<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_profissionais extends CI_Model {

		function __construct() {
		    parent::__construct();
		}

		/*Mobile*/
		public function listar_subcategorias(){

			return $this->db->query("select sub_categoria as categorias, id_sub_categoria from cad_sub_categorias  order by sub_categoria")->result();

		}

		public function profissional($id_profissional = null){

			return $this->db->query('select * from seg_usuarios where id_usuario = '.$id_profissional)->row();

		}

		public function alterarPerfil($valores = null){

			$this->db->where(array('id_usuario' => $valores['id_profissional']));
			return $this->db->update('seg_usuarios',$valores);

		}
		/*Mobile*/

		/*Painel*/
		public function view_profissionais(){

			return $this->db->query('SELECT 
										su.id_usuario,
										su.nome_usuario,
										su.email_usuario,
										su.ativo_usuario,
										(select media from view_avaliacao_media v where v.id_usuario = su.id_usuario) ranking,
										csc.sub_categoria 
										from seg_usuarios su
										left join cad_prof_subcate cps on (su.id_usuario = cps.fk_profissional)
										left join cad_sub_categorias csc on (cps.fk_sub_categoria = csc.id_sub_categoria)
										where fk_grupo_usuario = 2')->result();

		}

		public function view_novo_profissional(){

			return $this->db->query("select concat(categoria,' -> ',sub_categoria) as categorias, id_sub_categoria from cad_categorias inner join cad_sub_categorias on id_categoria = fk_categoria where id_categoria < 3 order by categoria ")->result();

		}

		public function atualizarProfissional($valores = null){
			$this->db->where(array('id_usuario' => $valores['id_usuario']));
			return $this->db->update('seg_usuarios',$valores);
		}

		public function view_editar_profissional($where = null){

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			$this->db->select("*,(select fk_plano from cad_pagamentos where fk_status = 1 and fk_profissional = id_usuario) as fk_plano");
			$this->db->select("(select nome from cad_planos where id_plano = fk_plano) as plano");

			$profissional = $this->db->get_where('seg_usuarios', array('id_usuario' => $where[0]))->row();

			if (isset($profissional)) {
				$this->session->set_flashdata('id_profissional',             $profissional->id_usuario);
				$this->session->set_flashdata('nome_profissional',           $profissional->nome_usuario);
				$this->session->set_flashdata('email_profissional',          $profissional->email_usuario);
				$this->session->set_flashdata('telefone_profissional',       $profissional->telefone_usuario);
				$this->session->set_flashdata('senha_profissional',          $profissional->senha_usuario);
				$this->session->set_flashdata('localizacao_profissional',    $profissional->localizacao);
				$this->session->set_flashdata('cep_profissional',            $profissional->cep);
				$this->session->set_flashdata('breve_descricao_profissional',$profissional->breve_descricao_profissional);
				$this->session->set_flashdata('fk_plano',$profissional->fk_plano);
				$this->session->set_flashdata('plano',$profissional->plano);
			}

			return array('categorias' => $this->db->query("select concat(categoria,' -> ',sub_categoria) as categorias, id_sub_categoria from cad_categorias inner join cad_sub_categorias on id_categoria = fk_categoria where id_categoria < 3 order by categoria")->result(),

						'avaliacoes' => $this->db->query("select
												date_format(data_atendimento, '%d/%m/%Y as %H:%i:%s') as data_atendimento,
												date_format(data_avaliacao, '%d/%m/%Y as %H:%i:%s') as data_avaliacao,
												avaliacao_atendimento,
												avaliacao_organizacao,
												avaliacao_pontualidade,
												avaliacao_servico,
												avaliacao_simpatia,
												depoimento_atendimento,
												nome_usuario
												from cad_atendimentos
												inner join seg_usuarios on fk_cliente = id_usuario
												where status_atendimento = 6 and 
													fk_profissional = ".$where[0])->result(),

					'planos' => $this->db->query("select * from cad_planos")->result());

		}

		public function novoProfissional($valores = null){
			$this->db->insert('seg_usuarios',$valores);
			return $this->db->insert_id();
		}

		public function catProf($valores = null) {
			$this->db->insert('cad_prof_subcate', $valores);
		}

		public function lista_profissionais($sub_categoria = null,$cidade = null){

			$where = " and lower(localizacao) like lower('%{$cidade}%')";
			if(is_null($cidade)){
				$where = "";
			}

			return $this->db->query("select 
										id_usuario,
										nome_usuario,
									    breve_descricao_profissional as bio,
									    concat(categoria,' / ',sub_categoria) as categoria
									    from seg_usuarios
									    inner join cad_prof_subcate on fk_profissional = id_usuario
									    inner join cad_sub_categorias on id_sub_categoria = fk_sub_categoria
									    inner join cad_categorias on id_categoria = fk_categoria
									    where fk_sub_categoria = {$sub_categoria}".$where)->result();

		}

		/*Painel*/


	}