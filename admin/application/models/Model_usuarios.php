<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_usuarios extends CI_Model {

		function __construct() {
			
		    parent::__construct();
		    
		}

		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();
			    return false;
			} else {
			    $this->db->trans_commit();
			    return true;
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
			return false;
		}

		public function view_usuarios(){

			$this->db->select("id_usuario,nome_usuario,email_usuario,login_usuario,CASE WHEN ativo_usuario='t' THEN 'Ativo'
            ELSE 'Inativo'
       		END as ativo_usuario");
			return $this->db->get_where('seg_usuarios',array('fk_grupo_usuario' => 1))->result();

		}

		public function view_novo_usuario(){
			//Lista dos grupos para o select
			return $this->db->get('seg_grupos')->result();

		}

		public function view_editar_usuario($where = null){

			$usuario = $this->db->get_where('seg_usuarios',array('id_usuario' => $where[0]))->row();

			if (isset($usuario)) {
				$this->session->set_flashdata('id_usuario',$usuario->id_usuario);
				$this->session->set_flashdata('nome_usuario',$usuario->nome_usuario);
				$this->session->set_flashdata('email_usuario',$usuario->email_usuario);
				$this->session->set_flashdata('telefone_usuario',$usuario->telefone_usuario);
				$this->session->set_flashdata('login_usuario',$usuario->login_usuario);
				$this->session->set_flashdata('senha_usuario',$usuario->senha_usuario);
				$this->session->set_flashdata('fk_grupo_usuario',$usuario->fk_grupo_usuario);
				$this->session->set_flashdata('ativo_usuario',$usuario->ativo_usuario);
			}

			//Lista dos grupos para o select
			return $this->db->get('seg_grupos')->result();

		}

		public function view_editar_perfil(){
			//Lista dos grupos para o select

			$usuario = $this->db->get_where('seg_usuarios',array('id_usuario' => $this->session->userdata('usuario')))->row();

			if (isset($usuario)) {
				$this->session->set_flashdata('id_usuario',$usuario->id_usuario);
				$this->session->set_flashdata('nome_usuario',$usuario->nome_usuario);
				$this->session->set_flashdata('email_usuario',$usuario->email_usuario);
				$this->session->set_flashdata('telefone_usuario',$usuario->telefone_usuario);
				$this->session->set_flashdata('login_usuario',$usuario->login_usuario);
				$this->session->set_flashdata('senha_usuario',$usuario->senha_usuario);
				$this->session->set_flashdata('fk_grupo_usuario',$usuario->fk_grupo_usuario);
				$this->session->set_flashdata('ativo_usuario',$usuario->ativo_usuario);
			}

			//Para redirecionar corretamente quando não existem outros conteúdos a serem retornados
			return array('ok' => true);

		}

		public function update($valores = null){
			$this->db->where(array('id_usuario' => $valores['id_usuario']));
			return $this->db->update('seg_usuarios',$valores);

		}

		public function create($valores = null){

			$this->db->insert('seg_usuarios',$valores);
			return $this->db->insert_id();

		}

		public function senha_Email($senha = null,$login = null) {

			$dados = $this->db->query('select id_usuario as id,email_usuario as email from seg_usuarios where login_usuario =  \''.$login.'\' and ativo_usuario = true;');

			try {

				if($dados->num_rows() > 0) {

					$this->db->query('update seg_usuarios set senha_usuario = \''.$senha.'\' where id_usuario = '.$dados->row()->id.';');
					return $dados->row()->email;

				} else {
					return "";
				}
				
			} catch (Exception $e) {

				echo $e;
				return "";
				
			}

		}


	}