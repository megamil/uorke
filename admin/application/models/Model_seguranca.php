<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_seguranca extends CI_Model {

	private $usuario;
	private $senha;
	private $id_aplicacao;

	function __construct() {
	    parent::__construct();
	}

	public function validar_login(){

		$this->db->select('*');
		$this->db->from('seg_usuarios');
		$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
		$this->db->where('login_usuario',$this->get_('usuario'));
		$this->db->where('senha_usuario',$this->get_('senha'));
		$login = $this->db->get()->row();

		if (isset($login) && $login->ativo_usuario) {
			return $login;
		} else {
			return false;
		}

	}

	public function validar_acesso(){

		$this->db->select(array('link_aplicacao','link_model','titulo_aplicacao'));
		$this->db->from('seg_aplicacao');
		$this->db->join('seg_aplicacoes_grupos','id_aplicacao = fk_aplicacao');
		$this->db->join('seg_controllers','id_controller = fk_controller');
		$this->db->join('seg_models','id_model = fk_model');
		$this->db->where('fk_grupo',$this->session->userdata('grupo'));
		$this->db->where('fk_aplicacao',$this->get_('id_aplicacao'));	

		$link_aplicacao = $this->db->get()->row();								

		if (isset($link_aplicacao)) {
			return array('retorno' => $link_aplicacao);
		} else {

			return array('retorno' => false,
						'detalhes_acesso' => $this->db->query('select (select descricao_aplicacao 
								from seg_aplicacao
								where id_aplicacao = '.$this->get_('id_aplicacao').') as descricao_aplicacao,

								(select descricao_grupo
								from seg_grupos
								where id_grupo = '.$this->session->userdata('grupo').') as descricao_grupo')->row());

		}

	}

	public function set_($campo,$valor){
		$this->$campo = $valor;
	}

	public function get_($campo){
		return $this->$campo;
	}

}