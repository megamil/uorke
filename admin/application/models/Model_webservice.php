<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_webservice extends CI_Model {

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();
			    return false;
			} else {
			    $this->db->trans_commit();
			    return true;
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
			return false;
		}

		############################### Querys ###############################

		public function buscarLocais($cidade = null){

			return $this->db->query("select distinct(cidade) as id, cidade as text from seg_usuarios where lower(localizacao) like lower('%{$cidade}%')")->result();

		}

		public function loginExistente($id = null, $telefone = null, $senha = null){

			$this->db->select('count(*) as total');
			$this->db->from('cad_clientes');
			$this->db->where('id_face_cliente',$id);
			$this->db->where('senha_cliente',sha1($senha));
			$login = $this->db->get()->row();

			if (isset($login) && $login->ativo_cliente) {
				return true;
			} else {
				return false;
			}

		}

		public function loginTelefone($telefone = null, $senha = null){

			$login = $this->db->query("select
										(select count(*) = 1 from cad_clientes where telefone_cliente = '{$telefone}' and senha_cliente = '{$senha}' and ativo_cliente = 1) cliente,

									    (select count(*) = 1 from cad_profissional where telefone_profissional = '{$telefone}' and senha_profissional = '{$senha}' and ativo_profissional = 1) profissional")->row();

			if ($login->cliente == 1) {
				
				return $this->db->query("SELECT nome_cliente nome, id_cliente id, 'c' tipo from cad_clientes where telefone_cliente = '{$telefone}' and senha_cliente = '{$senha}' and ativo_cliente = 1")->row();

			} else if ($login->profissional == 1) {

				return $this->db->query("SELECT nome_profissional nome, id_profissional id, 'p' tipo from cad_profissional where telefone_profissional = '{$telefone}' and senha_profissional = '{$senha}' and ativo_profissional = 1")->row();

			} else {

				return $this->db->query("SELECT '' nome, 0 id, 'null' tipo")->row();

			}


		}

		public function validar_login_facebook($id_facebook = null){

			$this->db->select('count(*) as total, id_usuario, ativo_usuario');
			$this->db->from('seg_usuarios');
			$this->db->where('id_face_usuario',$id_facebook);
			$login = $this->db->get()->row();

			if (isset($login) && $login->ativo_usuario && $login->total == 1) {
				return $login->id_usuario;
			} else {
				return false;
			}

		}

		public function validar_logincomum($email = null, $senha = null){

			$this->db->select('count(*) as total, id_usuario, ativo_usuario');
			$this->db->from('seg_usuarios');
			$this->db->where('email_usuario',$email);
			$this->db->where('senha_usuario',$senha);
			$login = $this->db->get()->row();

			if (isset($login) && $login->ativo_usuario && $login->total == 1) {
				
				return $login->id_usuario;

			} else {

				return false;

			}

		}

		public function getGrupoUsuario($id_usuario = null) {
			$this->db->select('fk_grupo_usuario');
			$this->db->from('seg_usuarios');
			$this->db->where('id_usuario',$id_usuario);
			return $this->db->get()->row();
		}

		public function loadCliente($id = null) {

			return $this->db->get_where('seg_usuarios',array('id_usuario' => $id))->row();

		}

		public function loadProfissional($id = null){

			return $this->db->get_where('seg_usuarios',array('id_usuario' => $id))->row();

			// return array(
			// 	'usuario' => $this->db->get_where('seg_usuarios',array('id_usuario' => $id))->row(),
			// 	'categorias' => $this->db->get_where('cad_prof_subcate',array('fk_profissional' => $id))->result()
			// );

		}

		public function novoCliente($valores = null){

			$this->db->insert('seg_usuarios',$valores);
			return $this->db->insert_id();

		}

		public function editarCliente($valores = null){

			$this->db->where(array('id_usuario' => $valores['id_usuario']));
			return $this->db->update('seg_usuarios',$valores);

		}

		public function novoProfissional($valores = null){

			$this->db->insert('seg_usuarios',$valores);

			return $this->db->insert_id();

		}

		public function editarProfissional($valores = null){

			$this->db->where(array('id_usuario' => $valores['id_usuario']));
			$this->db->update('seg_usuarios',$valores);

			$this->db->query('delete from cad_prof_subcate where fk_profissional = '.$valores['id_usuario']);

		}

		public function addSubCategoria($id_profissional = null, $fk_sub_categoria = null){

			return $this->db->insert('cad_prof_subcate',array('fk_profissional' => $id_profissional,'fk_sub_categoria' => $fk_sub_categoria));

		}

		//Quando clicar em ligar ou chat será considerado atendimento
		//OK, TESTADO
		public function contatoAtendimento($valores = null){

			return $this->db->insert('cad_atendimentos',$valores);

		}

		public function historicoAtendimentos($fk_cliente = null){

			return $this->db->query('SELECT 
										distinct(id_usuario) as id,
									    nome_usuario as nome,
									    cidade,
									    telefone_usuario as telefone,
									    
                                        (select categoria from cad_categorias where id_categoria =  (select fk_categoria from cad_sub_categorias where ca.fk_sub_categoria = id_sub_categoria))categoria,
                                        
									    (select sub_categoria from cad_sub_categorias where ca.fk_sub_categoria = id_sub_categoria) sub_categoria,

				
									    (select media from view_avaliacao_media where id_usuario = ca.fk_profissional) as avaliacao 

										 FROM cad_atendimentos ca
										 inner join seg_usuarios on id_usuario = ca.fk_profissional
                                         inner join cad_prof_subcate cps on (cps.fk_profissional = id_usuario and cps.fk_sub_categoria = ca.fk_sub_categoria)

										 where ativo_usuario = 1
										 and fk_cliente = '.$fk_cliente.';')->result();

		}

		public function listaCategorias(){
			return $this->db->get('cad_categorias')->result();
		}

		//Era usado no EP_CLIENTE
		public function listaSubCategorias($fk_categoria = null){

			if ($fk_categoria == 0) {
				//Limitando 1 e 2 temporariamente (Reforma e Assistencia)
				return $this->db->query('SELECT id_sub_categoria id, sub_categoria
										FROM cad_sub_categorias 
										where fk_categoria in (1,2,4,7,10)')->result();
			} else {
				return $this->db->query('SELECT id_sub_categoria id, sub_categoria
										FROM cad_sub_categorias 
										where fk_categoria = '.$fk_categoria)->result();
			}

			

		}
		//Era usado no EP_CLIENTE

		public function mainFunction($q, $s) {

			if ($s) {
				return $this->db->query("{$q}")->result();
			} else {
				return $this->db->query("{$q}");
			}

			
		}

		public function listaAtendimentosAvaliar($cliente = null){

			return $this->db->query('SELECT 
										distinct(id_atendimento) as id_atendimento,
										id_usuario as id,
										id_atendimento,
									    nome_usuario as nome,
									    cidade,
									    categoria,
									    sub_categoria,

									    (select media from view_avaliacao_media where id_usuario = ca.fk_profissional) as avaliacao 

										 FROM cad_atendimentos ca
										 inner join seg_usuarios on id_usuario = ca.fk_profissional
                                         inner join cad_prof_subcate cps on cps.fk_profissional = ca.fk_profissional
										 left join cad_sub_categorias on cps.fk_sub_categoria = id_sub_categoria
						                 left join cad_categorias on id_categoria = fk_categoria

										 where status_atendimento = 5 
										 and ativo_usuario = 1
										 and fk_cliente = '.$cliente)->result();

		}

		public function listarProfissionaisSubcategoria($fk_sub_categoria = null,$lat_usuario = null,$long_usuario = null,$raio = null){


			return $this->db->query("select 
									   id_usuario as id,
									   nome_usuario as nome,
									   telefone_usuario as telefone,
									   cidade,
									   categoria,
									   sub_categoria,
										round(geo({$lat_usuario}, {$long_usuario},lat_usuario,long_usuario),2) as distancia,

						                (select media from view_avaliacao_media where id_usuario = fk_profissional) as avaliacao 

										from seg_usuarios
											inner join cad_prof_subcate on fk_profissional = id_usuario
						                    inner join cad_sub_categorias on fk_sub_categoria = id_sub_categoria
						                    inner join cad_categorias on id_categoria = fk_categoria

										    where fk_sub_categoria = {$fk_sub_categoria} 
											and ativo_usuario = 1
											and geo({$lat_usuario}, {$long_usuario},lat_usuario,long_usuario) < {$raio}")->result();

		}

		public function avaliando($valores = null){

			//Soma as avaliações para poupar calculo no futuro.
			$atual = $this->db->query('select nome_usuario, email_usuario, id_usuario,soma_avaliacao_pontualidade,soma_avaliacao_simpatia,soma_avaliacao_atendimento,soma_avaliacao_servico,soma_avaliacao_organizacao,total_avaliacoes from seg_usuarios inner join cad_atendimentos on fk_profissional = id_usuario
				where id_atendimento = '.$valores['id_atendimento'].' and status_atendimento = 5')->row_array();

			if (isset($atual['id_usuario']) && !is_null($atual['id_usuario']) && $atual['id_usuario'] > 0) {

				//Cria uma nova avaliação.
				$this->db->set('data_avaliacao', 'NOW()', FALSE);
				$this->db->where(array('id_atendimento' => $valores['id_atendimento'], 'status_atendimento' => 5));
				$this->db->update('cad_atendimentos',$valores);

				//Pega o valor total de cada uma das avaliações e soma com essa.
				$atualizar = array (

					'soma_avaliacao_pontualidade' => ($valores['avaliacao_pontualidade']+$atual['soma_avaliacao_pontualidade']),
					'soma_avaliacao_simpatia'	  => ($valores['avaliacao_simpatia']+$atual['soma_avaliacao_simpatia']),
					'soma_avaliacao_atendimento'  => ($valores['avaliacao_atendimento']+$atual['soma_avaliacao_atendimento']),
					'soma_avaliacao_servico'	  => ($valores['avaliacao_servico']+$atual['soma_avaliacao_servico']),
					'soma_avaliacao_organizacao'  => ($valores['avaliacao_organizacao']+$atual['soma_avaliacao_organizacao']),
					'total_avaliacoes' 			  => (1 + $atual['total_avaliacoes'])

				);

				$this->db->where(array('id_usuario' => $atual['id_usuario']));
				$this->db->update('seg_usuarios',$atualizar);

				return array(

					'nome' => $atual['nome_usuario'],
					'email' => $atual['email_usuario']

				);

			} else {

				return false;

			}

		}

		public function validarEmail($email = null){

			return $this->db->query("select (count(*) > 0) as existe from seg_usuarios where email_usuario = '{$email}';")->row()->existe;

		}

		public function senha_Email($senha = null, $email = null){

			$existe = $this->db->query("select count(*) as qtd from seg_usuarios where email_usuario = '{$email}'")->row()->qtd;

			if ($existe == 1) {
				return $this->db->query("update seg_usuarios set senha_usuario = '{$senha}' where email_usuario = '{$email}'");
			} else {
				return false;
			}


		}

	}