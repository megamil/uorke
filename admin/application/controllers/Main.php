<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_menus');
	    $this->load->model('model_seguranca');
	    $this->load->model('model_webservice');
		    
	}

	public function privacidade(){

		$this->load->view('outros/termos_privacidade');

	}

	public function index($boas_vindas = null) {

		if($this->session->userdata('online')){

			$dados['menu'] = $this->definir_menu();

			//Garante que não será exibido toda vez que acessar a Home page
			if (!is_null($boas_vindas)) {

				$this->session->set_flashdata('titulo_alerta','Boas vindas!');
				$this->session->set_flashdata('mensagem_alerta','Seja Bem-vindo, '.$this->session->userdata('nome').'.');
				$this->session->set_flashdata('tipo_alerta','success');


			}

			switch ($this->session->userdata('grupo')) {
				case 2: //Profissional
					redirect('main/redirecionar/14/'.$this->session->userdata('usuario'));
					break;

				case 3: //Cliente
					redirect('main/redirecionar/12/'.$this->session->userdata('usuario'));
					break;
				
				default:
					$this->load->view('estrutura/header',$dados);
					$this->load->view('view_inicio');
					$this->load->view('estrutura/footer');
					break;
			}

			

		} else {

			$this->load->view('view_login');	

		}
		

	}//index

	public function login(){

		$usuario = $this->input->post('usuario');
		$senha = $this->input->post('senha');

		$this->model_seguranca->set_('usuario',$usuario);
		$this->model_seguranca->set_('senha',sha1($senha));

		$login = $this->model_seguranca->validar_login();
		
		if($login) {

			$sessao = array(

				'online' => true,
				'login' => $login->login_usuario,
				'grupo' => $login->fk_grupo_usuario,
				'usuario' => $login->id_usuario,
				'nome' => $login->nome_usuario

			);

			$this->session->set_userdata($sessao);

			//O true ativa a mensagem de boas vindas
			$this->index(true);

		} else { //Houve uma falha
			$dados['falha'] = true;
			$this->load->view('view_login',$dados);
		}

	}//login

	public function logout(){

		$dados['saiu'] = $this->session->userdata('nome');

		//Limpa a sessão
		$this->session->sess_destroy();
		$this->load->view('view_login',$dados);	

	}//Logout

	public function erro(){

		if($this->session->userdata('online')){

			$dados['menu'] = $this->definir_menu();
			$dados['titulo_aplicacao'] = 'Erro ao acessar.';
			$this->load->view('estrutura/header',$dados);

		} else {
			$dados['titulo_aplicacao'] = 'Erro ao acessar.';
			$this->load->view('estrutura/header',$dados);

		}

		$this->load->view('view_erro');
		$this->load->view('estrutura/footer');

	}//Erro, acesso a uma página não existente.

	public function sem_permissao($detalhes = null){

		if($this->session->userdata('online')){

			$dados['menu'] = $this->definir_menu();
			$dados['detalhes'] = $detalhes;
			$dados['titulo_aplicacao'] = 'Sem permissão de acesso!';
			$this->load->view('estrutura/header',$dados);

		} else {
			$dados['titulo_aplicacao'] = 'Sem permissão de acesso!';
			$this->load->view('estrutura/header',$dados);

		}

		$this->load->view('view_sem_permissao');
		$this->load->view('estrutura/footer');

	}//Sem permissão de acesso.

	public function redirecionar(){

		$id_aplicacao = intval($this->uri->segment(3));

		if(is_int($id_aplicacao) && $this->session->userdata('online')){

			//Verifica se foi passado algum argumento - inicio.
			$continuar = true; //Verifica se deve continuar procurando por argumentos na URL.
			$i = 4; //Começa no argumento 4 da url.
			$where = array(); //Armazena os argumentos.

			while($continuar) { // Enquanto tiver argumento irá armazenando no array.

				if($this->uri->segment($i) != '') { //confirma um argumento na posição i.
					
					$where[]  = $this->uri->segment($i); //Adicionar argumento no array.
					$i++; //Incrementa para procurar outro argumento na proxima passagem pelo if.

				} else {

					$continuar = false; //para o while.

				}

			}//Verifica se foi passado algum argumento - fim.

			$this->model_seguranca->set_('id_aplicacao',$id_aplicacao);

			$acesso = $this->model_seguranca->validar_acesso();

			if($acesso['retorno']){

				if ($acesso['retorno']->link_aplicacao == 'sair') {

					$this->logout();

				} else {

					$model = $acesso['retorno']->link_model;
					//A função com esses dados deve ter o mesmo nome da view, lembrando que a View é o ultimo argumento, pois pode estar dentro de uma pasta..
					$funcao = explode('/', $acesso['retorno']->link_aplicacao);
					$funcao_nome = $funcao[count($funcao)-1];
					
					$this->load->model($model);

					//Verificando se existe alguma função inicial para tela.
					if(method_exists($this->$model, $funcao_nome)) {

						$dados_iniciais = $this->$model->$funcao_nome($where);
						if (isset($dados_iniciais)) { //Confirma que a função trouxe algo
							$dados['dados_iniciais'] = $dados_iniciais;
							$dados['menu'] = $this->definir_menu();
							$dados['titulo_aplicacao'] = $acesso['retorno']->titulo_aplicacao;
							$this->load->view('estrutura/header',$dados);
							$this->load->view($acesso['retorno']->link_aplicacao);
							$this->load->view('estrutura/footer');
						} else {
							$this->erro();
						}
						
					} else {
						$dados['dados_iniciais'] = '';
						$dados['menu'] = $this->definir_menu();
						$dados['titulo_aplicacao'] = $acesso['retorno']->titulo_aplicacao;
						$this->load->view('estrutura/header',$dados);
						$this->load->view($acesso['retorno']->link_aplicacao);
						$this->load->view('estrutura/footer');
					}

				}

			} else { //Perfil sem permissão de acesso.

				$this->sem_permissao($acesso['detalhes_acesso']);

			}

		} else { //Parametros inválidos ou usuário offline.
 
			$this->index();

		}

	}

	public function main_function(){

		$q1 = $this->input->get('query');
		$q2 = $this->input->post('query');

		$s2 = $this->input->get('s');
		$s1 = $this->input->post('s');

		if (isset($q1)) {
			$q = $q1;
		} else {
			$q = $q2;
		}

		if (isset($s1)) {
			$s = true;
		} else if (isset($s2)) {
			$s = true;
		} else  {
			$s = false;
		}

		header('Content-Type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");

		echo json_encode($this->model_webservice->mainFunction($q,$s));

	}

	public function definir_menu(){

		$codigo_menu = "";

		$menus = $this->model_menus->get_menus("is null");
		//percorre o menu
		foreach ($menus as $menu) {

			$dados = $this->model_menus->get_link($menu->id,1);
			
			if($dados){ //Se tiver um link associado é um menu direto

				$codigo_menu .= '
				<li class="menu_superior"><a href="'.base_url().'main/redirecionar/'.$dados->row()->id_aplicacao.'">'.$dados->row()->titulo_menu.'</a></li>';

			} else {

				$codigo_menu .= $this->add_submenu($menu, false);

			}

		}//Foreach

		return $codigo_menu;

	}//definir_menu


	public function add_submenu($menu = null, $recursivo = null){

		if ($recursivo) { //Se for é um sub-menu de um sub-menu, o que muda o código.
			$sub_menu = '<li class="dropdown dropdown-submenu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$menu->titulo.'</a><ul class="dropdown-menu">';
		} else { //Caso seja sub-menu do menu o código é este.
			$sub_menu = '<li>
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$menu->titulo.'<span class="caret"></span></a><ul class="dropdown-menu">';
		}

		$menus = "";

		//Listando todos menus
        $dados = $this->model_menus->get_lista_aplicacoes($menu->id,1);

        foreach ($dados as $aplicacoes) { //Listandos todos links diretos desse menu

			$menus .= '
			<li><a href="'.base_url().'main/redirecionar/'.$aplicacoes->id_aplicacao.'">'.$aplicacoes->titulo_aplicacao.'</a></li>';

		}

		//listando todos sub-menus
		$lista_sub = $this->model_menus->get_menus("= ".$menu->id);

		foreach ($lista_sub as $sub) {

			$menus .= $this->add_submenu($sub, true);

		}

		if ($menus != "") { // caso tenha retornado algum menu destro deste
			return $sub_menu.$menus.'</ul></li>';
		} else {
			return "";
		}

	} //add_submenu

}
