<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_grupos extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_grupos');
		    
	}

	public function criar_grupo(){

		$this->form_validation->set_rules('nome_grupo','Nome do Grupo','required');
		$this->form_validation->set_rules('descricao_grupo','Descrição do Grupo','required');

		$dados = array (
					'nome_grupo' => $this->input->post('nome_grupo'),
					'descricao_grupo' => $this->input->post('descricao_grupo')
				);

		if ($this->form_validation->run()) {
			
			$id = $this->model_grupos->create($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Criado');
			$this->session->set_flashdata('mensagem_alerta','Grupo criado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			redirect('main/redirecionar/5/'.$id);

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
						$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao criar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/7');

		}

	}

	public function editar_grupo(){

		$this->form_validation->set_rules('nome_grupo','Nome do Grupo','required');
		$this->form_validation->set_rules('descricao_grupo','Descrição do Grupo','required');

		$dados = array (
					'nome_grupo' => $this->input->post('nome_grupo'),
					'descricao_grupo' => $this->input->post('descricao_grupo'),
					'id_grupo' => $this->input->post('id_grupo')
				);

		if ($this->form_validation->run()) {
			
			$this->model_grupos->update($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Atualizado');
			$this->session->set_flashdata('mensagem_alerta','Grupo atualizado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			redirect('main/redirecionar/5/'.$this->input->post('id_grupo'));

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
						$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao atualizar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/5/'.$this->input->post('id_grupo'));

		}

	}

}