<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_testes extends CI_Controller  {
/*Usado para upload de imagens pelo Tinemce*/
	public function upload_imagem(){

		$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/';

		//Tira os ascentos e espaços etc..
		$nome = strtolower( ereg_replace("[^a-zA-Z0-9-]", "-", strtr(utf8_decode(trim($_FILES['imagem']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-")) );;

		//Gera um nome único para o arquivo
		$arquivo = basename(time().uniqid(md5()).$nome);
		$uploadfile = $uploaddir . $arquivo;
		$editor = $_POST['editor'];

		echo '<pre>';
		if (move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)) {
		    echo "Arquivo válido.\n";
		} else {
		    echo "Arquivo inválido!\n";
		}

		echo 'DEBUG:';
		print_r($_FILES);

		echo '</pre>';

		$image_type = array('image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/bmp');

		echo '<script>';
		if(in_array($_FILES['imagem']['type'], $image_type)) {
			echo 'window.parent.'.$editor.'.insertContent("<img src=\''.base_url().'upload\/'.$arquivo.'\' />");';
		} else {
			echo 'window.parent.'.$editor.'.insertContent("Erro: Arquivo num formato inválido");';
			unlink($uploadfile);
		}
		echo 'window.parent.'.$editor.'.plugins.upload.finish();';
		echo '</script>';
		
	}

}