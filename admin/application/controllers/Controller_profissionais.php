<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_profissionais extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_profissionais');

	}

	public function criar_profissional(){

		$this->form_validation->set_rules('nome_profissional',           'Profissional','required');
		$this->form_validation->set_rules('email_profissional',          'E-mail',      'required');
		$this->form_validation->set_rules('telefone_profissional',       'Telefone',    'required');
		$this->form_validation->set_rules('senha_profissional',          'Senha',       'required');
		$this->form_validation->set_rules('cep_profissional',            'Cep',         'required');
		$this->form_validation->set_rules('localizacao_profissional',    'Endereço',    'required');
		$this->form_validation->set_rules('fk_sub_categoria',            'Profissão',   'required');
		$this->form_validation->set_rules('breve_descricao_profissional','Descrição',   'required');

		$dados = array (
					'nome_usuario'            => $this->input->post('nome_profissional'),
					'email_usuario'           => $this->input->post('email_profissional'),
					'login_usuario'           => $this->input->post('email_profissional'),
					'cep_profissional'           => $this->input->post('cep_profissional'),
					'numero'           => $this->input->post('numero'),
					'telefone_usuario'        => preg_replace('/[^0-9]/', '', $this->input->post('telefone_profissional')),
					'senha_usuario'           => sha1($this->input->post('senha_profissional')),
					'cep'             => $this->input->post('cep_profissional'),
					'localizacao'     => $this->input->post('localizacao_profissional'),
					'breve_descricao_profissional' => $this->input->post('breve_descricao_profissional'),
					'fk_grupo_usuario' => 2
				);

		if ($this->form_validation->run()) {
			
			$id = $this->model_profissionais->novoProfissional($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Criado');
			$this->session->set_flashdata('mensagem_alerta','Profissional criado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			$this->armazenarImagem($id,$_FILES['imagem']['name']);

			$sub_cat = array(
				'fk_profissional' =>$id,
				'fk_sub_categoria' => $this->input->post('fk_sub_categoria'));
			$this->model_profissionais->catProf($sub_cat);

			redirect('main/redirecionar/14/'.$id);

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
						$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao criar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/15');

		}

	}

	public function editar_profissional(){

		$this->form_validation->set_rules('nome_profissional',           'Profissional','required');
		$this->form_validation->set_rules('email_profissional',          'E-mail',      'required');
		$this->form_validation->set_rules('telefone_profissional',       'Telefone',    'required');
		$this->form_validation->set_rules('cep_profissional',            'Cep',         'required');
		$this->form_validation->set_rules('localizacao_profissional',    'Endereço',    'required');
		$this->form_validation->set_rules('fk_sub_categoria',            'Profissão',   'required');
		$this->form_validation->set_rules('breve_descricao_profissional','Descrição',   'required');

		$dados = array (
					'id_usuario'              => $this->input->post('id_profissional'),
					'nome_usuario'            => $this->input->post('nome_profissional'),
					'login_usuario'           => $this->input->post('email_profissional'),
					'email_usuario'           => $this->input->post('email_profissional'),
					'telefone_usuario'        => preg_replace('/[^0-9]/', '', $this->input->post('telefone_profissional')),
					'cep'             => $this->input->post('cep_profissional'),
					'localizacao'     => $this->input->post('localizacao_profissional'),
					'breve_descricao_profissional' => $this->input->post('breve_descricao_profissional'),
					'fk_grupo_usuario' => 2
				);

		if($this->input->post('senha_profissional') != ""){
			$dados['senha_usuario'] = sha1($this->input->post('senha_profissional'));
		}

		if ($this->form_validation->run()) {
			
			$this->model_profissionais->atualizarProfissional($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Criado');
			$this->session->set_flashdata('mensagem_alerta','Profissional criado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			$this->armazenarImagem($this->input->post('id_profissional'),$_FILES['imagem']['name']);

			redirect('main/redirecionar/14/'.$this->input->post('id_profissional'));

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
						$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao criar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/14/'.$this->input->post('id_profissional'));

		}

	}


	//Mobile
	public function editar_perfil(){

		$this->load->view('estrutura/header');

		if ($this->input->get('tipo') == 'profissional') {
			
			$dados = array(
				'categorias' => $this->model_profissionais->listar_subcategorias(),
				'usuario' => $this->model_profissionais->profissional($this->input->get('id'))
			);

		} else if($this->input->get('tipo') == 'cliente'){ //CONTRUIR

			$dados = array(
				'categorias' => $this->model_profissionais->listar_subcategorias(),
				'usuario' => $this->model_profissionais->cliente($this->input->get('id'))
			);

		}
		
		$this->load->view('mobile/editar_perfil_mobile',$dados);

	}

	public function alterar_perfil(){

		$id = $this->input->post('id_profissional');
		$nome = $this->input->post('nome');
		$email = $this->input->post('email');
		$telefone = $this->input->post('telefone');
		$senha = $this->input->post('senha');
		$localizacao = $this->input->post('localizacao');
		$cep = $this->input->post('cep');
		$subcategoria = $this->input->post('subcategoria');
		$descricao = $this->input->post('descricao');

		$dados = array(

			'id_profissional'          => $id,
			'nome_profissional'        => $nome,
			'email_profissional'       => $email,
			'telefone_profissional'    => $telefone,
			'senha_profissional'       => $senha,
			'localizacao_profissional' => $localizacao,
			'cep_profissional'         => $cep,
			'fk_sub_categoria'         => $subcategoria,
			'breve_descricao_profissional' => $descricao

		);

		$this->model_profissionais->alterarPerfil($dados);

		$dados = array(
			'categorias' => $this->model_profissionais->listar_subcategorias(),
			'usuario' => $this->model_profissionais->profissional($this->input->post('id_profissional'))
		);

		$this->armazenarImagem($id,$_FILES['imagem']['name']);

		$this->session->set_flashdata('titulo_alerta','Alterado com succeso');
		$this->session->set_flashdata('mensagem_alerta','Usuário "'.$nome.'" atualizado com sucesso!.');
		$this->session->set_flashdata('tipo_alerta','success');

		$this->load->view('estrutura/header');
		$this->load->view('mobile/editar_perfil_mobile',$dados);

	}
	//Mobile

	public function armazenarImagem($id = null, $imagem = null){
		if(isset($imagem) && $imagem != ''){

			$config['upload_path']   = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'fotos/profissionais';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['file_name']     = 'foto_perfil_profissional_id_'.$id.'.png';

			if(file_exists($config['upload_path'].'/'.$config['file_name'])){
		 
		    	unlink($config['upload_path'].'/'.$config['file_name']);

		    	unlink($config['upload_path'].'/'.'foto_perfil_profissional_id_'.$id.'_thumb.png');
		    
			}
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('imagem');

			//Thumbnail
			$config['image_library'] = 'gd2';
			$config['source_image'] = $config['upload_path'].'/'.$config['file_name'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['height']       = 300;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();

		}
	}


	//Usado para listar na tela do cliente os profissionais localizados.
	public function buscando(){

		$cidade = $this->input->get('cidade');

		if (!isset($cidade)) {
			$cidade = null;
		}

		$profissionais = $this->model_profissionais->lista_profissionais($this->input->get('subcategoria'),$cidade);

        foreach ($profissionais as $chave => $profissional) {
        	
            echo(' 
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12" style="padding: 0; " id="usuarios">
                        <img src="admin/fotos/profissionais/foto_perfil_profissional_id_'.$profissional->id_usuario.'.png" class="img_usuario">
                        <div style="margin: 3%">
                            <h3 style="font-weight: bolder; color: #000">'.$profissional->nome_usuario.'</h3>
                            <p style="color: (31, 31, 31, 0.7); font-weight: bolder; margin-top: -8px">'.$profissional->categoria.'</p>
                            <p style="color: #b2b2b2;padding-bottom: 5%">'.$profissional->bio.'</p>
                        </div>
                    </div>
            ');

        }

	}

}