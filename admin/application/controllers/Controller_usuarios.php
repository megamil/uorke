<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_usuarios extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_usuarios');
		    
	}

	public function criar_usuario(){

		$this->form_validation->set_rules('nome_usuario','Nome do Usuário','required');
		$this->form_validation->set_rules('email_usuario','E-mail do Usuário','required|is_unique[seg_usuarios.email_usuario]');
		$this->form_validation->set_rules('telefone_usuario','Telefone do Usuário','required');
		$this->form_validation->set_rules('login_usuario','Login do Usuário','required|is_unique[seg_usuarios.login_usuario]');
		$this->form_validation->set_rules('senha_usuario','Senha do Usuário','required');
		$this->form_validation->set_rules('ativo_usuario','Status do Usuário','required');
		$this->form_validation->set_rules('fk_grupo_usuario','Grupo do Usuário','required');

		$dados = array (
					'nome_usuario' => $this->input->post('nome_usuario'),
					'email_usuario' => $this->input->post('email_usuario'),
					'telefone_usuario' => $this->input->post('telefone_usuario'),
					'login_usuario' => $this->input->post('login_usuario'),
					'senha_usuario' => sha1($this->input->post('senha_usuario')),
					'ativo_usuario' => $this->input->post('ativo_usuario'),
					'fk_grupo_usuario' => $this->input->post('fk_grupo_usuario')
				);

		if ($this->form_validation->run()) {
			
			$id = $this->model_usuarios->create($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Criado');
			$this->session->set_flashdata('mensagem_alerta','Usuário "'.$this->input->post('nome_usuario').'" criado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			redirect('main/redirecionar/6/'.$id);

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
						$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao criar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/8');

		}

	}

	public function editar_usuario(){
	
		$this->form_validation->set_rules('nome_usuario','Nome do usuario','required');

		if($this->input->post('email_usuario') != $this->input->post('email_inicial')){
			$this->form_validation->set_rules('email_usuario','E-mail do usuario','required|is_unique[seg_usuarios.email_usuario]');
		}

		$this->form_validation->set_rules('telefone_usuario','Telefone do usuario','required');

		if($this->input->post('login_usuario') != $this->input->post('login_inicial')){
			$this->form_validation->set_rules('login_usuario','Login do usuario','required|is_unique[seg_usuarios.login_usuario]');	
		}
		
		$this->form_validation->set_rules('ativo_usuario','Status do usuario','required');
		$this->form_validation->set_rules('fk_grupo_usuario','Grupo do usuario','required');

		$dados = array (
					'id_usuario' => $this->input->post('id_usuario'),
					'nome_usuario' => $this->input->post('nome_usuario'),
					'email_usuario' => $this->input->post('email_usuario'),
					'telefone_usuario' => $this->input->post('telefone_usuario'),
					'login_usuario' => $this->input->post('login_usuario'),
					'ativo_usuario' => $this->input->post('ativo_usuario'),
					'fk_grupo_usuario' => $this->input->post('fk_grupo_usuario')
				);

		//Houve alteração de senha?
		if($this->input->post('senha_usuario') != ""){
			$dados['senha_usuario'] = sha1($this->input->post('senha_usuario'));
		} 

		if ($this->form_validation->run()) {
			
			$this->model_usuarios->update($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Editado');
			$this->session->set_flashdata('mensagem_alerta','Usuário "'.$this->input->post('nome_usuario').'" editado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			redirect('main/redirecionar/6/'.$this->input->post('id_usuario'));

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao criar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			redirect('main/redirecionar/6/'.$this->input->post('id_usuario'));

		}

	}

	public function editar_perfil(){

		$this->form_validation->set_rules('nome_usuario','Nome do usuario','required');

		if($this->input->post('email_usuario') != $this->input->post('email_inicial')){
			$this->form_validation->set_rules('email_usuario','E-mail do usuario','required|is_unique[seg_usuarios.email_usuario]');
		}

		$this->form_validation->set_rules('telefone_usuario','Telefone do usuario','required');

		if($this->input->post('login_usuario') != $this->input->post('login_inicial')){
			$this->form_validation->set_rules('login_usuario','Login do usuario','required|is_unique[seg_usuarios.login_usuario]');	
		}

		$dados = array (
					'id_usuario' => $this->input->post('id_usuario'),
					'nome_usuario' => $this->input->post('nome_usuario'),
					'email_usuario' => $this->input->post('email_usuario'),
					'telefone_usuario' => $this->input->post('telefone_usuario'),
					'login_usuario' => $this->input->post('login_usuario')
				);

		//Houve alteração de senha?
		if($this->input->post('senha_usuario') != ""){
			$dados['senha_usuario'] = sha1($this->input->post('senha_usuario'));
		} 

		if ($this->form_validation->run()) {
			
			$this->model_usuarios->update($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Editado');
			$this->session->set_flashdata('mensagem_alerta','Usuário "'.$this->input->post('nome_usuario').'" editado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			redirect('main/redirecionar/1');

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao criar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			redirect('main/redirecionar/1');

		}

	}

	public function esqueci_Senha() {

		$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0';
		$caractereVetor = explode(',',$caracteres);
		$senha = '';

		while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
			
			$indice = mt_rand(0, count($caractereVetor));
			$senha .= $caractereVetor[$indice-1];

		}

		$this->model_usuarios->start();
		$email = $this->model_usuarios->senha_Email(sha1($senha),$this->input->post('login'));

		//Usuário inativo ou desativado
		if($email == ""){

			//echo 'Usuário está desabilitado ou não existe, entre em contato com o administrador!';
			$json = array(
				'resultado' => "Dados incorretos ou perfil inativo",
				'status' => 0
			);

		} else { //Senha enviada para o E-mail.

			// Detalhes do Email. 
			$this->email->from('uorkeuorke@gmail.com', 'Troca de senha')
								->to($email)
								->subject('Troca de senha')
								->message('Recebemos sua solicitação de nova senha. <br> 
											Sua nova senha agora é: '.$senha."<br>
											<hr>
											".date('d/m/Y H:i:s')); 

			// Enviar... 
			if ($this->email->send()) { 
				if ($this->model_usuarios->commit()) {
					$json = array(
						'resultado' => "Nova Senha enviada para o E-mail: (".$email.")",
						'status' => 1
					);
				} else {
					$json = array(
					'resultado' => "Falha ao alterar senha.",
					'status' => 1
				);
				}
			} else {
				$this->model_usuarios->rollback();
				$json = array(
					'resultado' => "Erro ao enviar senha. <br>".$this->email->print_debugger(),
					'status' => 0
				);
			}

		}

		header('Content-Type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");
		echo json_encode($json);


	}



}