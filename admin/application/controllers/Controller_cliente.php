<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_cliente extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_clientes');
		    
	}

	public function criar_cliente(){

		$this->form_validation->set_rules('nome_cliente',           'cliente','required');
		$this->form_validation->set_rules('email_cliente',          'E-mail',      'required');
		$this->form_validation->set_rules('telefone_cliente',       'Telefone',    'required');
		$this->form_validation->set_rules('senha_cliente',          'Senha',       'required');

		$dados = array (
					'nome_usuario'            => $this->input->post('nome_cliente'),
					'email_usuario'           => $this->input->post('email_cliente'),
					'login_usuario'           => $this->input->post('email_cliente'),
					'telefone_usuario'        => $this->input->post('telefone_cliente'),
					'senha_usuario'           => sha1($this->input->post('senha_cliente')),
					'fk_grupo_usuario' => 3
				);

		if ($this->form_validation->run()) {
			
			$id = $this->model_clientes->novoCliente($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Criado');
			$this->session->set_flashdata('mensagem_alerta','Cliente criado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			$this->armazenarImagem($id,$_FILES['imagem']['name']);

			redirect('main/redirecionar/12/'.$id);

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
						$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao criar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/13');

		}

	}

	public function editar_cliente(){

		$this->form_validation->set_rules('nome_cliente',           'cliente','required');
		$this->form_validation->set_rules('email_cliente',          'E-mail',      'required');
		$this->form_validation->set_rules('telefone_cliente',       'Telefone',    'required');

		$dados = array (
					'id_usuario'              => $this->input->post('id_cliente'),
					'nome_usuario'            => $this->input->post('nome_cliente'),
					'email_usuario'           => $this->input->post('email_cliente'),
					'login_usuario'           => $this->input->post('email_cliente'),
					'telefone_usuario'        => $this->input->post('telefone_cliente'),
					'senha_usuario'           => sha1($this->input->post('senha_cliente')),
					'fk_grupo_usuario' => 3
				);

		if ($this->form_validation->run()) {
			
			$this->model_clientes->atualizarCliente($dados);

			$this->session->set_flashdata('titulo_alerta','Registro Criado');
			$this->session->set_flashdata('mensagem_alerta','Cliente editado com sucesso!.');
			$this->session->set_flashdata('tipo_alerta','success');

			$this->armazenarImagem($this->input->post('id_cliente'),$_FILES['imagem']['name']);

			redirect('main/redirecionar/12/'.$this->input->post('id_cliente'));

		} else {

			//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
						$erros = str_replace('
', '', validation_errors());

			$this->session->set_flashdata('titulo_alerta','Falha ao editar');
			$this->session->set_flashdata('tipo_alerta','error');
			$this->session->set_flashdata('mensagem_alerta','Erro(s) no formulário: '.$erros);

			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/12/'.$this->input->post('id_cliente'));

		}

	}

	public function armazenarImagem($id = null, $imagem = null){
		if(isset($imagem) && $imagem != ''){

			$config['upload_path']   = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'fotos/clientes';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name']     = 'foto_perfil_cliente_id_'.$id.'.png';

			if(file_exists($config['upload_path'].'/'.$config['file_name'])){
		 
		    	unlink($config['upload_path'].'/'.$config['file_name']);
		    
			}
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('imagem');

		}
	}

}