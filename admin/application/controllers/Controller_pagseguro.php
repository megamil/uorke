<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_pagseguro extends CI_Controller {

	function __construct() {
			// Construct the parent class
			parent::__construct();
			$this->load->model('model_pagseguro');
			
		}

	private $token_oficial 	= "TOKEN DO PAGSEGURO";
	public $url 			= "https://ws.sandbox.pagseguro.uol.com.br";
	public $autenticacao 	= "email=reynaldomarques@gmail.com&token=C7E6DEF9A19E48E3861EE7AF20673B99";
	
	public $email_token = "";//NÃO MODIFICAR
	public $statusCode = array(0=>"Pendente",
								1=>"Aguardando pagamento",
								2=>"Em análise",
								3=>"Pago",
								4=>"Disponível",
								5=>"Em disputa",
								6=>"Devolvida",
								7=>"Cancelada");

	public function pagar() {
		$pagamento = array(
			'fk_profissional' => $this->input->post('id_profissional'),
			'fk_plano' => $this->input->post('id_plano'),

		);

		$id_pagamento = $this->model_pagseguro->inserePagamento($pagamento);

		$tel = array(
			'areaCode' => $this->input->post('ddd'),
			'number' => $this->input->post('telefone')
		);

		$endereco = array(
			'street' => $this->input->post('logradouro'),
			'number' => $this->input->post('numero'),
			'complement' => $this->input->post('complemento'),
			'district' => $this->input->post('bairro'),
			'city' => $this->input->post('cidade'),
			'state' => $this->input->post('estado'),
			'country' => $this->input->post('pais'),
			'postalCode' => $this->input->post('cep')
		);

		$documento = array( array(
			'type' => $this->input->post('tipo_documento'),
			'value' => $this->input->post('documento'))
		);

		$sender = array(
			'name' => $this->input->post('nome'),
			'email' => $this->input->post('email'),
			'ip' => $this->input->post('ip'),
			'hash' => $this->input->post('hash_card'),
			'phone' => $tel,
			'address' => $endereco,
			'documents' => $documento
		);

		$cartao_tel = array(
			'areaCode' => $this->input->post('cartao_ddd'),
			'number' => $this->input->post('cartao_telefone')
		);

		$cartao_documento = array( array(
			'type' => $this->input->post('tipo_documento'),
			'value' => preg_replace('/[^0-9]/','',$this->input->post('cartao_cpf'))
		));

		$holder = array(
			'name' => $this->input->post('cartao_nome'),
			'birthDate' => $this->input->post('dtNasc'),
			'documents' => $cartao_documento,
			'phone' => $cartao_tel,
			'billingAddress' => $endereco

		);

		$cartaoCredito = array(
			'token' => $this->input->post('token'),
			'holder' => $holder
		);

		$meioPagamento = array(
			'type' => 'CREDITCARD',
			'creditCard' => $cartaoCredito
		);

		$dados = array(
			'plan' => $this->input->post('code_plano'),
			'reference' => $id_pagamento,
			'sender' => $sender,
			'paymentMethod' => $meioPagamento,
		);

		$this->executaCheckout(json_encode($dados), $id_pagamento);
		
		if($this->session->userdata('online')){
			redirect('main/redirecionar/14/'.$this->session->userdata('usuario'));
		} else {
			echo '<script type="text/javascript">alert(\'Sucesso - Você é um assinante!\')</script>';
		}
	}	

	public function executaCheckout($json = null, $id_pagamento) {

		$url = "{$this->url}/pre-approvals?{$this->autenticacao}";
		
		echo $json;

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'));
		$json = curl_exec($curl);
		curl_close($curl);

		if(isset(json_decode($json)->error)&&(json_decode($json)->error)) {
			echo $json;
		} else {
			$dados = array(
			'id_pagamento' => $id_pagamento,
			'code_assinatura' => json_decode($json)->code);

			$this->model_pagseguro->updatePagamento($dados);

			return $json;
		}
	}

	public function notificacao() {
		$dados = array(
			'notificationType' =>  $this->input->post('notificationType'),
			'notificationCode' => preg_replace('/[^A-Za-z0-9]/','',$this->input->post('notificationCode'))
		);
		$this->model_pagseguro->insereNotificacao($dados);
	}

	public function getSessionId() {
		$url = "{$this->url}/v2/sessions?{$this->autenticacao}";
		
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		
		$xml = curl_exec($curl);
		 
		curl_close($curl);

		$xml_obj = simplexml_load_string($xml);

		if(count($xml_obj -> error) > 0){
			//Insira seu código de tratamento de erro, talvez seja útil enviar os códigos de erros.
			echo $xml."<br><br>";
			echo "Erro-> ".var_export($xml_obj->errors,true);
			exit;
		}

		return $xml_obj->id;
	}

	public function pagando() {

		$plano = $this->input->post('fk_plano');
		$profissional = $this->session->userdata('usuario');
		$profissional_post = $this->input->post('id');


		if (!isset($plano)) {
			$plano = $this->input->get('fk_plano');
		}

		if(isset($profissional)){
			
		} else if (isset($profissional_post)) {

			$profissional = $profissional_post;

		} else {

			$profissional = $this->input->get('id');

		}

		$dados = array(
			'id' => $this->getSessionId(),
			'plano' => $this->model_pagseguro->getPlano($plano),
			'prof' => $this->model_pagseguro->getProfissional($profissional)
		);

		$this->load->view('pagamento/view_pagseguro', $dados);
	}

	public function consultaAssinatura($code = null) {

		//$code = $this->input->post('code');

		$url = "{$this->url}/pre-approvals/{$code}?{$this->autenticacao}";
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'));

		$json = curl_exec($curl);

		curl_close($curl);

		$status = json_decode($json)->status;
		$id_pagamento = json_decode($json)->reference;

		$dados = array(
					'id_pagamento' => $id_pagamento,
					'code_assinatura' => $code,
					'data_status' => date('Y-m-d H:i:s')
				);

		switch ($status) {
			case 'ACTIVE':
				$dados['fk_status'] = 1;
				break;
			
			case 'CANCELLED':
				$dados['fk_status'] = 2;
				break;

			case 'CANCELLED_BY_RECEIVER':
				$dados['fk_status'] = 3;				
				break;

			default:
				$dados['fk_status'] = 2;
				break;
		}

		$this->model_pagseguro->updatePagamento($dados);

		echo $status;
	}

	public function cancelaAssinatura() {
		$code = $this->input->post('code');

		$url = "{$this->url}/pre-approvals/{$code}/cancel?{$this->autenticacao}";
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'));

		$json = curl_exec($curl);

		curl_close($curl);

		$this->consultaAssinatura($code);
	}

	public function buscaAssinaturas($id_profissional = null) {
		
		$assinaturas = $this->model_pagseguro->getAssinaturas(2);

		foreach ($assinaturas as $assinatura => $value) {
			$this->consultaAssinatura($value['code_assinatura']);
		}
	}

	public function buscaAssinaturasAtivas() {
		print_r($this->model_pagseguro->getAssinaturasAtivas());
	}

	public function consultaPagamentos($code = null) {
		$code = $this->input->post('code');

		$url = "{$this->url}/pre-approvals/{$code}/payment-orders?{$this->autenticacao}";
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'));

		$json = curl_exec($curl);

		curl_close($curl);

		echo $json;
	}

	public function consultaNotificacao($code = null) {

		$code = $this->input->post('code');

		$url = "{$this->url}/pre-approvals/notifications/{$code}?{$this->autenticacao}";
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'));

		$json = curl_exec($curl);

		curl_close($curl);

		echo $json;
	}

	public function listaPlanos() {
		$planos = $this->model_pagseguro->getPlanos();

		print_r($planos);
	}
}