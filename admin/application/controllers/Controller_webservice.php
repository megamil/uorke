<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_webservice extends CI_Controller  {

		function __construct() {
			// Construct the parent class
			parent::__construct();
			$this->load->model('model_webservice');
			
		}

		public function buscar_Locais(){

			$cidade = $this->input->get('cidade');

			echo json_encode($this->model_webservice->buscarLocais($cidade));

		}

		public function login_cliente(){

			$id = $this->input->post("id");
			$senha = $this->input->post("senha");

			if($this->model_webservice->loginExistente($id,$senha)){

			}


		}

		//Cadastro pelo APP, com campos do Facebook opcionais
		public function novo_cliente(){

			$this->form_validation->set_rules('nome',           'cliente',	   'required');
			$this->form_validation->set_rules('email',          'E-mail',      'required|is_unique[seg_usuarios.email_usuario]');
			$this->form_validation->set_rules('telefone',       'Telefone',    'required');
			$this->form_validation->set_rules('senha',          'Senha',       'required');

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			if ($this->form_validation->run()) {

				$dados = array (

					//'facebook_usuario' => $this->campoPreenchido($this->input->post('facebook_cliente')),
					'id_face_usuario' => $this->campoPreenchido($this->input->post('id_facebook')),
					//'token_face_usuario' => $this->campoPreenchido($this->input->post('token_face_cliente')),

					'nome_usuario' => $this->input->post('nome'),
					'email_usuario' => $this->input->post('email'),
					'telefone_usuario' => preg_replace("/[^0-9]/", "", $this->input->post('telefone')),
					'login_usuario' => $this->input->post('email'),
					'senha_usuario' => sha1(($this->input->post('senha'))),
					'ativo_usuario' => 1,
					'fk_grupo_usuario' => 3

				);

				$this->model_webservice->start();


				$id = $this->model_webservice->novoCliente($dados);

				if ($this->input->post('id_facebook') != "" && (is_null($_FILES['imagem']['name'])) || $_FILES['imagem']['name'] == "") { //Login com facebook, e não fez upload de imagem.
					//Copia
					copy('http://graph.facebook.com/'.$this->input->post('id_facebook').'/picture?type=large', 
						$_SERVER['DOCUMENT_ROOT'].'/'.base_url().'fotos/clientes/foto_perfil_cliente_id_'.$id.'.png');
				} else {
					$this->armazenarImagemCliente($id,$_FILES['imagem']['name']);
				}	

				if ($this->model_webservice->commit()) {
					echo json_encode(array('status' => true));
				} else {
					echo json_encode(array('status' => false, 'erro' => 'Falha ao inserir dados.'));
				}

			} else {
				echo json_encode(array('status' => false, 'erro' => validation_errors()));

			}

		}	

		public function load_cliente(){

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			$dados = $this->model_webservice->loadCliente($this->input->get('id'));

			echo json_encode($dados);

		}

		public function editar_cliente() {


			$this->form_validation->set_rules('nome',           'cliente',	   'required');

			$email1 = $this->input->post('email');
			$email2 = $this->input->post('email2');

			if($email1 != $email2){
				$this->form_validation->set_rules('email',          'E-mail',      'required|is_unique[seg_usuarios.email_usuario]');
			}

			$this->form_validation->set_rules('telefone',       'Telefone',    'required');

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			if ($this->form_validation->run()) {

				$dados = array (

					//'facebook_usuario' => $this->campoPreenchido($this->input->post('facebook_cliente')),
					'id_usuario' => $this->input->post('id_usuario'),
					'id_face_usuario' => $this->campoPreenchido($this->input->post('id_facebook')),
					//'token_face_usuario' => $this->campoPreenchido($this->input->post('token_face_cliente')),

					'nome_usuario' => $this->input->post('nome'),
					'email_usuario' => $this->input->post('email'),
					'telefone_usuario' => preg_replace("/[^0-9]/", "", $this->input->post('telefone')),
					'login_usuario' => $this->input->post('email'),
					'ativo_usuario' => 1,
					'fk_grupo_usuario' => 3

				);

				$senha = $this->input->post('senha');

				if ($senha != "") {
					$dados['senha_usuario'] = sha1($senha);
				}

				$this->model_webservice->start();


				$id = $this->model_webservice->editarCliente($dados);

				if ($this->input->post('id_facebook') != "" && (is_null($_FILES['imagem']['name'])) || $_FILES['imagem']['name'] == "") { //Login com facebook, e não fez upload de imagem.
					//Copia
					copy('http://graph.facebook.com/'.$this->input->post('id_facebook').'/picture?type=large', 
						$_SERVER['DOCUMENT_ROOT'].'/'.base_url().'fotos/clientes/foto_perfil_cliente_id_'.$id.'.png');
				} else if (!is_null($_FILES['imagem']['name']) || $_FILES['imagem']['name'] != "") {
					$this->armazenarImagemCliente($id,$_FILES['imagem']['name']);
				}	

				if ($this->model_webservice->commit()) {
					echo json_encode(array('status' => true));
				} else {
					echo json_encode(array('status' => false, 'erro' => 'Falha ao editar dados.'));
				}

			} else {
				echo json_encode(array('status' => false, 'erro' => validation_errors()));

			}

		}

		public function novo_profissional(){

			$this->form_validation->set_rules('nome_profissional',           'Profissional',	   		'required');
			$this->form_validation->set_rules('email_profissional',          'E-mail',      	'required|is_unique[seg_usuarios.email_usuario]');
			$this->form_validation->set_rules('telefone_profissional',       'Telefone',    	'required');
			$this->form_validation->set_rules('senha_profissional',       	 'Senha',    		'required');
			$this->form_validation->set_rules('localizacao_profissional', 	 'Localização',     'required');
			$this->form_validation->set_rules('cep_profissional',         	 'CEP',    			'required');
			$this->form_validation->set_rules('breve_descricao_profissional','Descrição',       'required');

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");			

			if ($this->form_validation->run()) {

				$dados = array (

					'nome_usuario' => $this->input->post('nome_profissional'),
					'email_usuario' => $this->input->post('email_profissional'),
					'telefone_usuario' => preg_replace("/[^0-9]/", "", $this->input->post('telefone_profissional')),
					'senha_usuario' => sha1($this->input->post('senha_profissional')),
					'localizacao' => $this->input->post('localizacao_profissional'),
					'login_usuario' => $this->input->post('email_profissional'),

					'lat_usuario' => $this->input->post('lat_usuario'),
					'long_usuario' => $this->input->post('long_usuario'),

					'cpf_profissional' => preg_replace("/[^0-9]/", "", $this->input->post('cpf_profissional')),
					'numero' => $this->input->post('numero'),

					
					'cep' 	 =>	$this->input->post('cep_profissional'),
					'bairro' => $this->input->post('bairro_profissional'),
					'cidade' => $this->input->post('cidade_profissional'),
					'estado' => $this->input->post('estado_profissional'),
					'pais' 	 =>	$this->input->post('pais_profissional'),

					'breve_descricao_profissional' => $this->input->post('breve_descricao_profissional'),
					'ativo_usuario' => 1,
					'fk_grupo_usuario' => 2

				);

				$this->model_webservice->start();

				$id = $this->model_webservice->novoProfissional($dados);
				$this->armazenarImagemProf($id,$_FILES['imagem']['name']);

				if(count($this->input->post('fk_sub_categoria')) > 0){
					foreach ($this->input->post('fk_sub_categoria') as $key => $fk_sub_categoria) {
						$this->model_webservice->addSubCategoria($id,$fk_sub_categoria);
					}
				}

				if ($this->model_webservice->commit()) {
					echo json_encode(array('status' => true));
				} else {
					echo json_encode(array('status' => false, 'erro' => 'Falha ao inserir dados.'));
				}

			} else {
				
				echo json_encode(array('status' => false, 'erro' => validation_errors()));

			}

		}

		public function load_profissional(){

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			$dados = $this->model_webservice->loadProfissional($this->input->get('id'));

			echo json_encode($dados);

		}

		public function editar_profissional(){

			$this->form_validation->set_rules('nome_profissional',           'Profissional',	   		'required');
			
			$this->form_validation->set_rules('telefone_profissional',       'Telefone',    	'required');
			$this->form_validation->set_rules('localizacao_profissional', 	 'Localização',     'required');
			$this->form_validation->set_rules('cep_profissional',         	 'CEP',    			'required');
			$this->form_validation->set_rules('breve_descricao_profissional','Descrição',       'required');

			$email1 = $this->input->post('email_profissional');
			$email2 = $this->input->post('email_profissional2');

			if($email1 != $email2){
				$this->form_validation->set_rules('email',          'E-mail',      'required|is_unique[seg_usuarios.email_usuario]');
			}
				

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");			

			if ($this->form_validation->run()) {

				$dados = array (

					'id_usuario' => $this->input->post('id_usuario'),
					'nome_usuario' => $this->input->post('nome_profissional'),
					'nome_usuario' => $this->input->post('nome_profissional'),
					'email_usuario' => $this->input->post('email_profissional'),
					'telefone_usuario' => preg_replace("/[^0-9]/", "", $this->input->post('telefone_profissional')),
					'localizacao' => $this->input->post('localizacao_profissional'),
					'login_usuario' => $this->input->post('email_profissional'),

					'lat_usuario' => $this->input->post('lat_usuario'),
					'long_usuario' => $this->input->post('long_usuario'),

					'cpf_profissional' => preg_replace("/[^0-9]/", "", $this->input->post('cpf_profissional')),
					'numero' => $this->input->post('numero'),

					
					'cep' 	 =>	$this->input->post('cep_profissional'),
					'bairro' => $this->input->post('bairro_profissional'),
					'cidade' => $this->input->post('cidade_profissional'),
					'estado' => $this->input->post('estado_profissional'),
					'pais' 	 =>	$this->input->post('pais_profissional'),

					'breve_descricao_profissional' => $this->input->post('breve_descricao_profissional'),
					'ativo_usuario' => 1,
					'fk_grupo_usuario' => 2

				);

				$senha = $this->input->post('senha');

				if ($senha != "") {
					$dados['senha_usuario'] = sha1($senha);
				}

				$this->model_webservice->start();

				$this->model_webservice->editarProfissional($dados);

				if (!is_null($_FILES['imagem']['name']) || $_FILES['imagem']['name'] != "") {
					$this->armazenarImagemProf($dados['id_usuario'],$_FILES['imagem']['name']);
				}

				if(count($this->input->post('fk_sub_categoria')) > 0){
					foreach ($this->input->post('fk_sub_categoria') as $key => $fk_sub_categoria) {
						$this->model_webservice->addSubCategoria($dados['id_usuario'],$fk_sub_categoria);
					}
				}
				

				if ($this->model_webservice->commit()) {
					echo json_encode(array('status' => true));
				} else {
					echo json_encode(array('status' => false, 'erro' => 'Falha ao editar dados.'));
				}

			} else {
				
				echo json_encode(array('status' => false, 'erro' => validation_errors()));

			}

		}

		//Login por telefone CLiente / Profissional
		public function login_telefone(){

			$telefone = $this->input->post('telefone');
			$senha = sha1($this->input->post('senha'));

			$dados = $this->model_webservice->loginTelefone($telefone,$senha);

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			echo json_encode($dados);


		}

		//Novo Login
		public function login_app(){

			$id_facebook = $this->input->post('id_facebook');
			$email = $this->input->post('email');
			$senha = $this->input->post('senha');

			if (isset($id_facebook)) { //Login com facebook
				
				$login_face = $this->model_webservice->validar_login_facebook($id_facebook);
				
				if ($login_face) {
					
					$resultado = array( 
						'id_usuario' => $login_face,
						'status' => 1,
						'grupo' => $this->model_webservice->getGrupoUsuario($login_face),
						'mensagem' => 'Login Facebook realizado com sucesso.'
					);

				} else {
					
					$resultado = array( 
						'id_usuario' => 0,
						'status' => 0,
						'grupo' => 0,
						'mensagem' => 'Login Facebook falhou'
					);

				}

				
				
			} else if(isset($email) && isset($senha)) { //Login comum

				$logincomum = $this->model_webservice->validar_logincomum($email,$senha);

				if ($logincomum) {
					
					$resultado = array( 
						'id_usuario' => $logincomum,
						'status' => 1,
						'grupo' => $this->model_webservice->getGrupoUsuario($logincomum),
						'mensagem' => 'Login realizado com sucesso.'
					);

				} else { 
					
					$resultado = array( 
						'id_usuario' => 0,
						'status' => 0,
						'grupo' => 0,
						'mensagem' => 'Login falhou'
					);

				}

			} else { // campos em brando
				
				$resultado = array( 
					'id_usuario' => 0,
					'status' => 0,
					'grupo' => 0,
					'mensagem' => 'Envie os campos',
					'email' => $email,
					'senha' =>$senha
				);

			}

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");
			echo json_encode($resultado);

		}

		//Quando clicar em ligar ou chat será considerado atendimento
		//OK, TESTADO
		public function contato(){

			$dados = array(
				'fk_cliente' => $this->input->post('fk_cliente'),
				'fk_profissional' => $this->input->post('fk_profissional'),
				'fk_sub_categoria' => $this->input->post('fk_sub_categoria'),
				'status_atendimento' => 5 //Em andamento
			);

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			$resultado = array('status' => $this->model_webservice->contatoAtendimento($dados));
			echo json_encode($resultado);

		}

		public function historico_atendimentos(){

			$dados = $this->model_webservice->historicoAtendimentos($this->input->post('fk_cliente'));

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");
			
			echo json_encode($dados);

		}

		//OK, TESTADO
		public function lista_categorias(){

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			echo json_encode($this->model_webservice->listaCategorias());

		}
		//OK, TESTADO
		public function lista_sub_categorias(){

			$fk_categoria = $this->input->post('fk_categoria');

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			echo json_encode($this->model_webservice->listaSubCategorias($fk_categoria));

		}

		public function avaliar(){

			$this->model_webservice->start();

			$dados = array(

					'id_atendimento'=> $this->input->post('id_atendimento'),
					'avaliacao_pontualidade'=> $this->input->post('avaliacao_pontualidade'),
					'avaliacao_simpatia'=> $this->input->post('avaliacao_simpatia'),
					'avaliacao_atendimento'=> $this->input->post('avaliacao_atendimento'),
					'avaliacao_servico'=> $this->input->post('avaliacao_servico'),
					'avaliacao_organizacao'=> $this->input->post('avaliacao_organizacao'),
					'depoimento_atendimento'=> $this->input->post('depoimento_atendimento'),
					'status_atendimento'=> 6

				);

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			$dados_profissional = $this->model_webservice->avaliando($dados);

			if ($dados_profissional) { 
				
				// Detalhes do Email. 
				$this->email->from('uorkeuorke@gmail.com', 'UORKE'); 
				$this->email->to($dados_profissional['email']); 
				$this->email->subject('UORKE | Novo Depoimento Recebido'); 
				$this->email->message('
					<html>
						<head>
						    <title>E-mail</title>
						    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
						    <style>
						        body{
						            font-family: \'Open Sans\', sans-serif;
						        }
						    </style>
						</head>
						<body>
						<p style="margin-top: 30px">&nbsp;</p>
						<p style="text-align: center"><img src="http://'.$_SERVER['HTTP_HOST'].base_url().'style/img/logo_menu_uorke.png" width="130px"></p>
						<p style="margin-top: 30px">&nbsp;</p>
						<p style="width: 60%;margin: 0 auto; text-align: justify">

						<p style="margin-top: 30px">&nbsp;</p>
						<p style="text-align: center;width: 320px;margin: 0 auto;font-weight: bolder;color: #696969">
						    Olá '.$dados_profissional['nome'].'. <br> 
										Você acaba de receber um depoimento sobre um atendimento realizado: <br>
										"'.$this->input->post('depoimento_atendimento').'"
										<hr>
										'.date('d/m/Y H:i:s').'
						</p>

						<p style="margin-top: 30px">&nbsp;</p>
						<h3 style="text-align: center;font-weight: bolder;">
						    Dúvidas?
						</h3 style="text-align: center">
						<p style="text-align: center;margin-top: -15px;font-weight: bolder;color: #696969">Entre em contato com: <span style="color:#47A9D6"> contato@uorke.com</span></p>
						<p style="text-align: center;color:#47A9D6;font-weight: bolder;">wwww.uorke.com.br</p>


						</p>
						</body>
					</html>'); 


				// Enviar... 
				if ($this->email->send()) { 
					
					echo json_encode(array('status' => $this->model_webservice->commit()));

				} else {
					
					echo json_encode(array('status' => $this->model_webservice->rollback(), 'mensagem' => 'Falha ao enviar e-mail: '.$this->email->print_debugger()));

				}

			} else { //Falha ao editar

				echo json_encode(array('status' => $this->model_webservice->rollback(), 'mensagem' => 'Falha ao editar atendimento ID '.$dados['id_atendimento']));

			}

			
		}

		public function main_function(){

			$q1 = $this->input->post('query');
			$q2 = $this->input->get('query');

			$s2 = $this->input->post('s');
			$s1 = $this->input->get('s');

			if (isset($q1)) {
				$q = $q1;
			} else {
				$q = $q2;
			}

			if (isset($s1)) {
				$s = true;
			} else if (isset($s2)) {
				$s = true;
			} else  {
				$s = false;
			}

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			echo json_encode($this->model_webservice->mainFunction($q,$s));

		}

		//para listar os profissionais que atendem determinada subcategoria.
		//OK, Testado
		public function listar_profissionais_subcategoria(){

			$fk_sub_categoria = $this->input->post('fk_sub_categoria');
			$lat_usuario = $this->input->post('lat_usuario');
			$long_usuario = $this->input->post('long_usuario');
			$raio = 5;

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			echo json_encode(
				$this->model_webservice->listarProfissionaisSubcategoria($fk_sub_categoria,$lat_usuario,$long_usuario,$raio)
			);

		}

		public function lista_atendimentos_avaliar(){

			$id_cliente = $this->input->post('fk_cliente');

			header('Content-Type: application/json; charset=utf-8');
			header("access-control-allow-origin: *");

			echo json_encode($this->model_webservice->listaAtendimentosAvaliar($id_cliente));

		}

		public function campoPreenchido($campo = null){

			if (isset($campo)) {
				return $campo;
			} else {
				return null;
			}

		}

		//Armazena imagem pro
		public function armazenarImagemProf($id = null, $imagem = null){
			if(isset($imagem) && $imagem != ''){

				$config['upload_path']   = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'fotos/profissionais';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['file_name']     = 'foto_perfil_profissional_id_'.$id.'.png';

				if(file_exists($config['upload_path'].'/'.$config['file_name'])){
			 
			    	unlink($config['upload_path'].'/'.$config['file_name']);
			    	unlink($config['upload_path'].'/'.'foto_perfil_profissional_id_'.$id.'_thumb.png');
			    
				}
				
				$this->load->library('upload', $config);
				$this->upload->do_upload('imagem');

				//Thumbnail
				$config['image_library'] = 'gd2';
				$config['source_image'] = $config['upload_path'].'/'.$config['file_name'];
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = TRUE;
				$config['height']       = 300;

				$this->load->library('image_lib', $config);

				$this->image_lib->resize();

			}
		}

		//Armazena imagem do cliente, se for do facebook não é chamada está função.
		public function armazenarImagemCliente($id = null, $imagem = null){
			if(isset($imagem) && $imagem != ''){

				$config['upload_path']   = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'fotos/clientes';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name']     = 'foto_perfil_cliente_id_'.$id.'.png';

				if(file_exists($config['upload_path'].'/'.$config['file_name'])){
			 
			    	unlink($config['upload_path'].'/'.$config['file_name']);
			    
				}
				
				$this->load->library('upload', $config);
				$this->upload->do_upload('imagem');

			}

		}

		/*Recuperar senha pelo app.*/
		public function recuperarSenha(){

			$email_post = $this->input->post('email');

			if($email_post == "" || !$this->model_webservice->validarEmail($email_post)){
				
				$json = array(
					'resultado' => "E-mail '{$email_post}' não localizado.",
					'status' => 0
				);

				header('Content-Type: application/json');
				echo json_encode($json);
				die();
			}

			$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0';
			$caractereVetor = explode(',',$caracteres);
			$senha = '';

			while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
				
				$indice = mt_rand(0, count($caractereVetor) - 1);
				$senha .= $caractereVetor[$indice];

			}

			$this->model_webservice->start();
			$this->model_webservice->senha_Email(sha1($senha),$email_post);

			// Detalhes do Email. 
			$this->email->from('uorkeuorke@gmail.com', 'UORKE | Troca de senha'); 
			$this->email->to($this->input->post('email')); 
			$this->email->subject('UORKE | Troca de senha'); 
					$this->email->message('
				<html>
					<head>
					    <title>E-mail</title>
					    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
					    <style>
					        body{
					            font-family: \'Open Sans\', sans-serif;
					        }
					    </style>
					</head>
					<body>
					<p style="margin-top: 30px">&nbsp;</p>
					<p style="text-align: center"><img src="http://'.$_SERVER['HTTP_HOST'].base_url().'style/img/logo_menu_uorke.png" width="130px"></p>
					<p style="margin-top: 30px">&nbsp;</p>
					<p style="width: 60%;margin: 0 auto; text-align: justify">

					<h2 style="text-align: center;font-weight: bolder;">Sua nova senha: <hr> '.$senha.'</h2>

					<p style="margin-top: 30px">&nbsp;</p>
					<p style="text-align: center;width: 320px;margin: 0 auto;font-weight: bolder;color: #696969">
					    Nova senha solicitada em: '.date('d/m/Y H:i:s').'
					</p>

					<p style="margin-top: 30px">&nbsp;</p>
					<h3 style="text-align: center;font-weight: bolder;">
					    Dúvidas?
					</h3 style="text-align: center">
					<p style="text-align: center;margin-top: -15px;font-weight: bolder;color: #696969">Entre em contato com: <span style="color:#47A9D6"> contato@uorke.com</span></p>
					<p style="text-align: center;color:#47A9D6;font-weight: bolder;">wwww.uorke.com.br</p>


					</p>
					</body>
				</html>'); 

			// Enviar... 
			if ($this->email->send()) { 
				$this->model_webservice->commit();
				$json = array(
					'resultado' => "Senha atualizada com sucesso",
					'status' => 1
				);
			} else {
				$this->model_webservice->rollback();
				$json = array(
					'resultado' => "Erro ao enviar senha. <br>".$this->email->print_debugger(),
					'status' => 0
				);
			}

			header('Content-Type: application/json');
			echo json_encode($json);

		}

}