DROP DATABASE u676502882_uorke;
CREATE SCHEMA u676502882_uorke;
use u676502882_uorke;

DROP DATABASE u781453715_uorke;
CREATE SCHEMA u781453715_uorke;
use u781453715_uorke;

/*Segurança Grupos*/
create table seg_grupos(

	id_grupo int not null AUTO_INCREMENT,
	nome_grupo character varying(100) NOT NULL,
	descricao_grupo text NOT NULL,

	CONSTRAINT pk_grupo PRIMARY KEY (id_grupo),
	CONSTRAINT unique_nome_grupo UNIQUE (nome_grupo)

);

/*Segurança Usuários*/
create table seg_usuarios(

	id_usuario int not null AUTO_INCREMENT,
	nome_usuario character varying(100) NOT NULL,
	email_usuario character varying(100) NOT NULL,
	telefone_usuario character varying(11) NOT NULL,
	login_usuario character varying(100) NOT NULL,
	senha_usuario character varying(40) NOT NULL,
	ativo_usuario boolean NOT NULL,
	fk_grupo_usuario int not null,
	facebook_usuario text,
	id_face_usuario bigint(20),
	token_face_usuario text,
	data_cadastro_usuario timestamp,
	breve_descricao_profissional text,
	localizacao text,
	cpf_profissional character varying(11),
	lat_usuario FLOAT(10,6),
	long_usuario FLOAT(10,6),

	soma_avaliacao_pontualidade double default 0,
	soma_avaliacao_simpatia double default 0,
	soma_avaliacao_atendimento double default 0,
	soma_avaliacao_servico double default 0,
	soma_avaliacao_organizacao double default 0,

	total_avaliacoes int default 0,

	cep character varying(8),
	bairro character varying(30),
	cidade character varying(30),
	estado character varying(2),
	pais character varying(4),
	numero character varying(11),

	CONSTRAINT pk_usuario PRIMARY KEY (id_usuario),
	CONSTRAINT unique_email_usuario UNIQUE (email_usuario),
	CONSTRAINT unique_login_usuario UNIQUE (login_usuario),
	CONSTRAINT fk_seg_usuarios_fk_grupo_usuario FOREIGN KEY (fk_grupo_usuario)
      REFERENCES seg_grupos (id_grupo)

);

/*Segurança, cadastro dos models*/
create table seg_models(

	id_model int not null AUTO_INCREMENT,
	link_model character varying(100) not null,
	descricao_model text not null,

	CONSTRAINT pk_model PRIMARY KEY (id_model)

);

/*Segurança, cadastro dos controllers*/
create table seg_controllers(

	id_controller int not null AUTO_INCREMENT,
	link_controller character varying(100) not null,
	descricao_controller text not null,
	fk_model int,

	CONSTRAINT pk_controller PRIMARY KEY (id_controller),
	CONSTRAINT fk_seg_controllers_fk_model FOREIGN KEY (fk_model)
      REFERENCES seg_models (id_model)

);

/*Segurança menu, gerá um menu no aplicativo, as telas devem ser vinculadas*/
create table seg_menu(

	id_menu int not null AUTO_INCREMENT,
	titulo_menu character varying(100) NOT NULL,
	descricao_menu text NOT NULL,
	menu_acima int, 

	CONSTRAINT pk_menu PRIMARY KEY (id_menu),
	CONSTRAINT fk_seg_menu_menu_acima FOREIGN KEY (menu_acima)
      REFERENCES seg_menu (id_menu)

);

/*Segurança, aplicações do menu.*/
create table seg_aplicacao(

	id_aplicacao int not null AUTO_INCREMENT,
	link_aplicacao character varying(100) NOT NULL,
	titulo_aplicacao character varying(100) NOT NULL,
	descricao_aplicacao text NOT NULL,
	fk_controller int,

	CONSTRAINT pk_aplicacao PRIMARY KEY (id_aplicacao),
    CONSTRAINT fk_seg_aplicacao_fk_controller FOREIGN KEY (fk_controller)
      REFERENCES seg_controllers (id_controller)

);

/*Vincula as aplicações ao menu*/
create table seg_aplicacoes_menu(

	id_aplicacoes_menu int not null AUTO_INCREMENT,
	fk_aplicacao int NOT NULL,
	fk_menu int NOT NULL,

	CONSTRAINT pk_aplicacoes_menu PRIMARY KEY (id_aplicacoes_menu),
	CONSTRAINT fk_seg_aplicacao_fk_aplicacao FOREIGN KEY (fk_aplicacao)
      REFERENCES seg_aplicacao (id_aplicacao),
    CONSTRAINT fk_seg_aplicacao_fk_menu FOREIGN KEY (fk_menu)
      REFERENCES seg_menu (id_menu)

);

/*Segurança, Grupo aplicação*/
create table seg_aplicacoes_grupos(

	id_aplicacoes_grupos int not null AUTO_INCREMENT,
	fk_grupo int NOT NULL,
	fk_aplicacao int NOT NULL,

	CONSTRAINT pk_aplicacoes_grupos PRIMARY KEY (id_aplicacoes_grupos),
	CONSTRAINT fk_aplicacoes_grupos_fk_aplicacao FOREIGN KEY (fk_aplicacao)
      REFERENCES seg_aplicacao (id_aplicacao),
    CONSTRAINT fk_aplicacoes_grupos_fk_grupo FOREIGN KEY (fk_grupo)
      REFERENCES seg_grupos (id_grupo)

);

/*Cadastro dos controllers e models*/
insert into seg_models (id_model,link_model,descricao_model) values 
	(1,'Model_menu','Responsável pelos menus.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(2,'Model_seguranca','Responsável pelo acesso as sistema e controle de perfils.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(3,'Model_usuarios','Responsável pelo gerênciamento dos usuários.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(4,'Model_grupos','Responsável pelo gerênciamento dos grupos.');

insert into seg_models (id_model,link_model,descricao_model) values 
	(5,'Model_clientes','Responsável pelo gerênciamento dos clientes.');

insert into seg_models (id_model,link_model,descricao_model) values 
	(6,'Model_profissionais','Responsável pelo gerênciamento dos profissionais.');


insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(1,'Main','Responsável pelo gerênciamento do acesso ao sistema.',2);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(2,'Controller_usuarios','Responsável pelo gerênciamento dos usuários.',3);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(3,'Controller_grupos','Responsável pelo gerênciamento dos grupos.',4);

insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(4,'Controller_clientes','Responsável pelo gerênciamento dos grupos.',5);

insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(5,'Controller_profissionais','Responsável pelo gerênciamento dos grupos.',6);

/*INSERTS PARA USUÁRIO DE TESTES*/
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo) values (1,'Administradores','Administradores do sistema');
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo) values (2,'Profissionais','Profissionais');
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo) values (3,'Clientes','Clientes');

insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario)
values (1,'Usuário Administrador','megamil3d@gmail.com','11962782329','admin','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,1);

/*Definindo os menus.*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima) 
	values (1000,'Mais','Mais, Perfils e Grupos',null);	 /*1000 para Garantir que será o útima menu*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima) 
	values (1,'Editar Perfil','Editar Perfil',1000);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima) 
	values (2,'Usuários','Lista de usuários',1000);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima) 
	values (3,'Grupos','Lista de grupos',1000);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima) 
	values (4,'Sair','Sair do sistema Logout',1000);


insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (9,'seguranca/view_editar_perfil','Relatórios','Relatórios',1);


insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima) 
	values (6,'Clientes','Clientes, Clientes',null);

insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (10,'cliente/view_clientes','Clientes','Clientes',4);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (10,6);

insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima) 
	values (7,'Profissionais','Profissionais, Profissionais',null);

insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (11,'profissional/view_profissionais','Profissionais','Profissionais',5);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (11,7);

insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(9,1); 
    insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(10,1); 
    insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(11,1); 

/*Difinindo aplicações e links*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (1,'seguranca/view_editar_perfil','Editar Perfil','Editar Perfil',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (2,'seguranca/view_usuarios','Usuários','Usuários',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (3,'seguranca/view_grupos','Grupos','Grupos',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (4,'sair','Sair','Sair',1);

/*Aplicações sem menu*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (5,'seguranca/view_editar_grupo','Editar Grupo','Editar Grupo',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (6,'seguranca/view_editar_usuario','Editar Usuário','Editar Usuários',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (7,'seguranca/view_novo_grupo','Novo Grupo','Novo Grupo',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (8,'seguranca/view_novo_usuario','Novo Usuário','Novo Usuários',2);


insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (12,'cliente/view_editar_cliente','Editar Cliente','Editar Cliente',4);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (14,'profissional/view_editar_profissional','Editar Profissional','Editar Profissional',5);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (13,'cliente/view_novo_cliente','Novo Cliente','Novo Cliente',4);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (15,'profissional/view_novo_profissional','Novo Profissional','Novo Profissional',5);

/*Indicando quais aplicações estão ligadas a quais menus.*/
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (1,1);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (2,2);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (3,3);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (4,4); 

/*Dando permissões para o grupo administrador.*/
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(1,1); 
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(2,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(3,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(4,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(5,1); 
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(6,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(7,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(8,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(12,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(13,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(14,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(15,1);


insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(14,2);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(12,3);

			/****** ENCONTRE PROFISSIONAIS ******/

create table cad_categorias(

	id_categoria int not null AUTO_INCREMENT,
	categoria text,

	CONSTRAINT pk_categoria PRIMARY KEY (id_categoria)


);


create table cad_sub_categorias(

	id_sub_categoria int not null AUTO_INCREMENT,
	fk_categoria int,
	sub_categoria text,

	PRIMARY KEY (id_sub_categoria),
	FOREIGN KEY (fk_categoria) references cad_categorias(id_categoria)

);

insert into cad_categorias values (1,'REFORMAS');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Arquiteto');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Automação Residencial');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Chaveiro');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Decorador');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Dedetizador');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Desentupidor');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Eletricista');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Encanador');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Engenheiro');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Gesso e drywall');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Impermeabilizador');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Jardinagem');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Limpeza Pós-Obra');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Marceneiro');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Marido de aluguel');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Montador de móveis');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Mudanças e carretos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Outros');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Paisagista');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Pedreiro');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Pintor');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Piscina');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Segurança eletrônica');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Serralheria e solda');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Tapeceiro');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Terraplanagem');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (1,'Vidraceiro');

insert into cad_categorias values (2,'ASSISTÊNCIA TÉCNICA');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Aparelhos de Som');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Ar condicionado');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Cabeamento e Redes');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Câmera');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Celular');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Computador desktop');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'DVD / Blu-Ray');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Eletrodomésticos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Fogão e Cooktop');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Geladeira e freezer');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Home theater');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Impressora');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Lava Louça');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Lava Roupas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Microondas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Notebooks e laptops');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Outros');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Tablet');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Telefones (não celular)');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Telefonia PABX');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Televisão');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (2,'Video game');

insert into cad_categorias values (3,'AULAS');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Artes e artesanatos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Bem-Estar');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Concursos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Aulas de Dança');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Aulas particulares');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Esportes');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Aulas de idiomas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Aulas de informática');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Lutas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Aulas de música');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (3,'Outros');

insert into cad_categorias values (4,'AUTOS');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (4,'Alarme automotivo');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (4,'Ar Condicionado');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (4,'Funilaria');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (4,'Inspeção veicular');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (4,'Insufilm');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (4,'Martelinho de Ouro');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (4,'Revisão');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (4,'Som Automotivo');

insert into cad_categorias values (5,'CONSULTORIA');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Advogados');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Assessoria de imprensa');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Auxilio administrativo');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Consultor pessoal');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Consultoria especializada');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Contador');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Detetive Particular');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Digitalizar documentos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Economia e finanças');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Outros');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Pesquisas em geral');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Produção de conteúdo');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Segurança do trabalho');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (5,'Tradutores');

insert into cad_categorias values (6,'DESIGN E TECNOLOGIA');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Animação');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Apps para smartphone');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Áudio e vídeo');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Modelagem 2D e 3D');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Convites');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Criação de logos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Desenvolvimento de sites');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Diagramador');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Edição de fotos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Ilustração');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Marketing online');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Materiais promocionais');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Outros');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Produção gráfica');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (6,'Web Design');

insert into cad_categorias values (7,'EVENTOS');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Animação de festas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Assessor de eventos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Bandas e cantores');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Bartenders');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Brindes e Lembrancinhas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Buffet completo');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Churrasqueiro');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Decoração');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Djs');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Equipamentos para festas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Fotografia');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Garçons e copeiras');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Gravação de vídeos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Outros');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Recepcionistas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (7,'Segurança');

insert into cad_categorias values (8,'MODA E BELEZA');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Artesanato');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Cabeleireiros');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Corte e costura');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Depilação');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Design de sobrancelhas');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Esotérico');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Esteticista');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Manicure e pedicure');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Maquiadores');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Outros');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Personal stylist');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Sapateiro');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (8,'Artesanato');

insert into cad_categorias values (9,'SAÚDE');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (9,'Acompanhante de idosos');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (9,'Enfermeira');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (9,'Fisioterapeuta');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (9,'Fonoaudiólogo');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (9,'Nutricionista');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (9,'Psicólogo');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (9,'Quiroprático');

insert into cad_categorias values(10,'SERVIÇOS DOMÉSTICOS');

	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Adestrador de cães');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Babá');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Cozinheira');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Diarista');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Limpeza de piscina');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Motorista');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Outros');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Passadeira');
	insert into cad_sub_categorias (fk_categoria,sub_categoria) values  (10,'Passeador de cães');


	/*Removendo os OUTROS*/
	DELETE FROM cad_sub_categorias WHERE id_sub_categoria = 18;
	DELETE FROM cad_sub_categorias WHERE id_sub_categoria = 44;
	DELETE FROM cad_sub_categorias WHERE id_sub_categoria = 60;
	DELETE FROM cad_sub_categorias WHERE id_sub_categoria = 78;
	DELETE FROM cad_sub_categorias WHERE id_sub_categoria = 95;
	DELETE FROM cad_sub_categorias WHERE id_sub_categoria = 111;
	DELETE FROM cad_sub_categorias WHERE id_sub_categoria = 123;
	DELETE FROM cad_sub_categorias WHERE id_sub_categoria = 140;


create table lista_status(

	id_status int not null AUTO_INCREMENT,
	status text,

	CONSTRAINT pk_status PRIMARY KEY (id_status)

);

insert into lista_status (id_status,status) values 
	(1,'Ativo'),(2,'Inativo'),(3,'Agendado'),(4,'Cancelado'),(5,'Em Andamento'),(6,'Finalizado');


create table cad_pagamentos(

	id_pagamento int not null AUTO_INCREMENT,
	data_pagamento timestamp,
	data_status timestamp,
	fk_plano int,
	fk_status int,
	code_assinatura text,
	fk_profissional int,
	
	CONSTRAINT pk_pagamento PRIMARY KEY (id_pagamento),
	CONSTRAINT fk_profissional_pagamento FOREIGN KEY (fk_profissional)
      REFERENCES seg_usuarios (id_usuario)

);

create table cad_prof_subcate(

	fk_profissional int,
	fk_sub_categoria int,

	CONSTRAINT pk_prof_cate PRIMARY KEY (fk_profissional,fk_sub_categoria),
	CONSTRAINT fk_prof_cate_profissional FOREIGN KEY (fk_profissional)
      REFERENCES seg_usuarios (id_usuario),
    CONSTRAINT fk_prof_cate_categoria FOREIGN KEY (fk_sub_categoria)
      REFERENCES cad_sub_categorias (id_sub_categoria)

);

create table cad_atendimentos(

	id_atendimento int not null AUTO_INCREMENT,
	data_atendimento timestamp default current_timestamp,
	data_avaliacao timestamp,

	avaliacao_pontualidade double,
	avaliacao_simpatia double,
	avaliacao_atendimento double,
	avaliacao_servico double,
	avaliacao_organizacao double,

	depoimento_atendimento text,

	status_atendimento int,
	
	fk_cliente int,
	fk_profissional int,
	fk_sub_categoria int,

	PRIMARY KEY (id_atendimento),

	FOREIGN KEY (fk_cliente)         REFERENCES seg_usuarios (id_usuario),

	FOREIGN KEY (fk_profissional)         REFERENCES seg_usuarios (id_usuario),

    FOREIGN KEY (status_atendimento) REFERENCES lista_status (id_status),

    FOREIGN KEY (fk_sub_categoria)    REFERENCES cad_sub_categorias (id_sub_categoria)
);	

create table cad_notificacao (
	id_notificacao int not null AUTO_INCREMENT,
	notificationType character varying(100) NOT null,
	notificationCode character varying(100) NOT null,
	
	CONSTRAINT pk_notificacao PRIMARY KEY (id_notificacao)
);

create table cad_planos(
	id_plano int not null AUTO_INCREMENT,
	nome varchar(100) not null,
	valor real not null,
	periodo int not null,
	trial int not null,
	code varchar(100),
CONSTRAINT pk_plano PRIMARY KEY (id_plano)
);

INSERT INTO cad_planos (id_plano, nome, valor, periodo, trial, code) VALUES
(1, 'UORKE PLANO MENSAL', 	  59.99, 1, 15, '608DFDBCA1A1595994087F8F6F45988E'),
(2, 'UORKE PLANO TRIMESTRAL', 149.97, 3, 15, '423F5149ECEC9B16648B0F8A2A43BA5D'),
(3, 'UORKE PLANO SEMESTRAL',  239.94, 6, 15, '2242550D40409CDDD4151FB7F065AD8E');

create table cad_status_assinatura(
	id_status int not null,
	descricao varchar(100) not null
);

INSERT INTO cad_status_assinatura (id_status, descricao) VALUES
(1, 'Ativo'),
(2, 'Cancelado'),
(3, 'Cancelado pelo vendedor');

/*Views*/
/*Nova soma*/
create or replace view view_avaliacao_media as
SELECT 

id_usuario,

	ROUND(
		IFNULL((((soma_avaliacao_pontualidade + 
			soma_avaliacao_simpatia + 
			soma_avaliacao_atendimento + 
			soma_avaliacao_servico + 
			soma_avaliacao_organizacao)/5)/total_avaliacoes)
		,0) /*IFNULL RETIRA O NULL E DEIXA 0*/
	,1)  media /*Arredondando para 1 casa decimal*/

FROM seg_usuarios
group by id_usuario;


/*Funções*/
/*Geolocalização, calcula distancia entre dois pontos em kilometros.*/
delimiter //
CREATE FUNCTION Geo(lat_ini DOUBLE(18,10), lon_ini DOUBLE(18,10),lat_fim DOUBLE(18,10), lon_fim DOUBLE(18,10))
RETURNS DOUBLE(18,10)
NOT DETERMINISTIC
BEGIN
DECLARE Theta DOUBLE(18,10);
DECLARE Dist DOUBLE(18,10);
DECLARE Miles DOUBLE(18,10);
DECLARE kilometers DOUBLE(18,10);

SET Theta = lon_ini - lon_fim;
SET Dist  = SIN(RADIANS(lat_ini)) * SIN(RADIANS(lat_fim)) +  COS(RADIANS(lat_ini)) * COS(RADIANS(lat_fim)) * COS(RADIANS(Theta));
SET Dist  = ACOS(Dist);
SET Dist  = DEGREES(Dist);
SET Miles = Dist * 60 * 1.1515;
SET kilometers = Miles * 1.609344;

RETURN kilometers;

END;//
delimiter ;