<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="theme-color" content="#f7921c">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/bt2.css" rel="stylesheet">
    <link href="css/cadastro_cliente.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">
    <link rel="stylesheet" href="css/login.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script>
        function efeito(){
            $('#formulario_todo').animate({'opacity':'1.00'}, 1200, 'linear');
            $('#formulario_todo').css('display', 'table');
        }
    </script>
    <style>
        #formulario_todo{
            opacity: 0;
            display: none;
        }
        .textos_input{
            width: 100%;
            margin: 0 auto;
            padding: 1% 0;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        @media screen and (min-width: 1200px) and (max-width: 3000px) {
            #formulario_todo{
                margin-top: 3%;
            }
        }
    </style>

</head>
<!-- NAVBAR
================================================== -->
<body onload="efeito()">
<div id="tudo" >
    <!---------------------- topo ------------->
    <?php include('topo.php'); ?>

    <div id="formulario_todo" class="container" >
        <form id="formulario_cliente" >
            <center><h2 style="color:#f7921c;margin-bottom: 3%;font-weight: bolder"> Cadastre-se <br>15 dias grátis !</h2></center>
            <p> Email</p>
            <p> <input type="text" class="textos_input"></p>
            <p> Telefone</p>
            <p> <input type="text" class="textos_input"></p>
            <p> Endereço</p>
            <p> <input type="text" class="textos_input"></p>
            <p>  Senha</p>
            <p> <input type="text" class="textos_input"></p>
            <p> Confirmar senha</p>
            <p > <input  style="margin-bottom: 3%"type="text" class="textos_input"></p>

            <a class="btn btn-block btn-social btn-facebook" style="background: #3D5193;padding: 3%;margin:2% 0; color: #fff; ">
                <i class="fa fa-facebook" ></i>  &nbsp; Cadastrar com o Facebook
            </a>
            <p><a class="btn btn-lg btn-primary" href="login.php" style="width: 100%;border:1px solid #f7921c;background: #f7921c" role="button" >Finalizar</a></p>
        </form>
    </div>


</div>
</div>
<script src="js/jq1.js"></script>
<script src="js/jq3.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/jq2.js"></script>
</body>
</html>
