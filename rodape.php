


<!-- rodape -->
<div id="rodape">
    <div class="container-fluid">
        <div class="row">


            <div class="col-sm-3 col-md-3 col-xs-11" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Tecnologias e Design</li>
                    <li><a href="usuarios.php?subcategoria=83" style="color: #ccc;" >Animação</a></li>
                    <li><a href="usuarios.php?subcategoria=84"  style="color: #ccc;">APP para smartphone</a></li>
                    <li><a href="usuarios.php?subcategoria=85" style="color: #ccc;" >Áudio e vídeo</a></li>
                    <li><a href="usuarios.php?subcategoria=86" style="color: #ccc;">Modelagem 2D e 3D</a></li>
                    <li><a href="usuarios.php?subcategoria=87" style="color: #ccc;">Convites</a></li>
                    <li><a href="usuarios.php?subcategoria=88" style="color: #ccc;">Criação de Logos</a></li>
                    <li><a href="usuarios.php?subcategoria=89" style="color: #ccc;" >Desenvolvimento de Sites</a></li>
                    <li><a href="usuarios.php?subcategoria=90" style="color: #ccc;">Diagramador</a></li>
                    <li><a href="usuarios.php?subcategoria=91" style="color: #ccc;">Edição de fotos</a></li>
                    <li><a href="usuarios.php?subcategoria=92" style="color: #ccc;">Ilustração</a></li>
                    <li><a href="usuarios.php?subcategoria=93" style="color: #ccc;">Marketing online</a></li>
                    <li><a href="usuarios.php?subcategoria=94" style="color: #ccc;">Materiais promocionais</a></li>
                    <li><a href="usuarios.php?subcategoria=96" style="color: #ccc;">Produção gráfica</a></li>
                    <li><a href="usuarios.php?subcategoria=97" style="color: #ccc;">Web Design</a></li>
                </ul>
            </div>


            <div class="col-sm-3 col-md-3 col-xs-11" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Moda e Estética</li>
                    <li><a  href="usuarios.php?subcategoria=114" style="color: #ccc;" >Artesanato</a></li>
                    <li><a  href="usuarios.php?subcategoria=115" style="color: #ccc;" >Cabeleireiro </a></li>
                    <li><a href="usuarios.php?subcategoria=116" style="color: #ccc;">Corte e costura</a></li>
                    <li><a href="usuarios.php?subcategoria=117" style="color: #ccc;">Depilação</a></li>
                    <li><a href="usuarios.php?subcategoria=118" style="color: #ccc;">Design de sobrancelhas</a></li>
                    <li><a href="usuarios.php?subcategoria=119" style="color: #ccc;">Estérico</a></li>
                    <li><a href="usuarios.php?subcategoria=120" style="color: #ccc;">Esteticista</a></li>
                    <li><a href="usuarios.php?subcategoria=121" style="color: #ccc;">Manicure e pedicure</a></li>
                    <li><a href="usuarios.php?subcategoria=122" style="color: #ccc;">Maquiadores</a></li>
                    <li><a href="usuarios.php?subcategoria=125" style="color: #ccc;">Sapateiro</a></li>
                    <li><a href="usuarios.php?subcategoria=124" style="color: #ccc;">Personal stylist</a></li>
                    <li><a href="usuarios.php?subcategoria=126" style="color: #ccc;">Artesanato</a></li>
                </ul>
            </div>

            <div class="col-sm-3 col-md-3 col-xs-11" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Consultoria</li>
                    <li><a href="usuarios.php?subcategoria=72" style="color: #ccc;">Consultor pessoal</a></li>
                    <li><a href="usuarios.php?subcategoria=73" style="color: #ccc;">Consultoria especializada</a></li>
                    <li><a href="usuarios.php?subcategoria=74" style="color: #ccc;">Contador</a></li>
                    <li><a href="usuarios.php?subcategoria=75" style="color: #ccc;">Detetive Particular</a></li>
                    <li><a href="usuarios.php?subcategoria=76" style="color: #ccc;">Digitalizar documentos</a></li>
                    <li><a href="usuarios.php?subcategoria=77" style="color: #ccc;">Economia e finanças</a></li>
                    <li><a href="usuarios.php?subcategoria=79" style="color: #ccc;">Pesquisas em geral</a></li>
                    <li><a href="usuarios.php?subcategoria=80" style="color: #ccc;">Produção de conteúdo</a></li>
                    <li><a href="usuarios.php?subcategoria=81" style="color: #ccc;">Segurança do trabalho</a></li>
                    <li><a href="usuarios.php?subcategoria=82" style="color: #ccc;">Tradutores</a></li>
                </ul>
            </div>


            <div class="col-sm-3 col-md-3 col-xs-11  col-lg-3" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Saúde &#38 Bem-estar</li>
                    <li><a href="usuarios.php?subcategoria=127" style="color: #ccc;" >Acompanhante de idosos</a></li>
                    <li><a href="usuarios.php?subcategoria=128" style="color: #ccc;">Enfermeira</a></li>
                    <li><a href="usuarios.php?subcategoria=129" style="color: #ccc;">Fisioterapeuta</a></li>
                    <li><a href="usuarios.php?subcategoria=130" style="color: #ccc;">Fonoaudiólogo</a></li>
                    <li><a href="usuarios.php?subcategoria=131" style="color: #ccc;">Nutricionista</a></li>
                    <li><a href="usuarios.php?subcategoria=132" style="color: #ccc;">Psicólogo</a></li>
                    <li><a href="usuarios.php?subcategoria=133" style="color: #ccc;">Quiroprático</a></li>
                </ul>
            </div>





            <div class="col-lg-12 col-md-12 col-sm-12"></div>





            <div class="col-sm-3 col-md-3 col-xs-11" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Reformas e construções</li>
                    <li><a href="usuarios.php?subcategoria=1" style="color: #ccc;">Arquiteto</a></li>
                    <li><a href="usuarios.php?subcategoria=2" style="color: #ccc;">Automação Residencial</a></li>
                    <li><a href="usuarios.php?subcategoria=3" style="color: #ccc;">Chaveiro</a></li>
                    <li><a href="usuarios.php?subcategoria=4" style="color: #ccc;">Decorador</a></li>
                    <li><a href="usuarios.php?subcategoria=5" style="color: #ccc;">Dedetizador</a></li>
                    <li><a href="usuarios.php?subcategoria=6" style="color: #ccc;">Desentupidor</a></li>
                    <li><a href="usuarios.php?subcategoria=7" style="color: #ccc;">Eletricista</a></li>
                    <li><a href="usuarios.php?subcategoria=8" style="color: #ccc;">Encanador</a></li>
                    <li><a href="usuarios.php?subcategoria=9" style="color: #ccc;">Engenheiro</a></li>
                    <li><a href="usuarios.php?subcategoria=10" style="color: #ccc;">Gesso e Drywall</a></li>
                    <li><a href="usuarios.php?subcategoria=11" style="color: #ccc;">Impermeabilizador</a></li>
                    <li><a href="usuarios.php?subcategoria=12" style="color: #ccc;">Jardineiro</a></li>
                    <li><a href="usuarios.php?subcategoria=13" style="color: #ccc;">Limpeza Pós-Obra</a></li>
                    <li><a href="usuarios.php?subcategoria=14" style="color: #ccc;">Marceneiro</a></li>
                    <li><a href="usuarios.php?subcategoria=15" style="color: #ccc;">Marido de aluguel</a></li>
                    <li><a href="usuarios.php?subcategoria=16" style="color: #ccc;">Montador de móveis</a></li>
                    <li><a href="usuarios.php?subcategoria=17" style="color: #ccc;">Mudanças e carretos</a></li>
                    <li><a href="usuarios.php?subcategoria=19" style="color: #ccc;">Paisagista </a></li>
                    <li><a href="usuarios.php?subcategoria=20" style="color: #ccc;">Pedreiro</a></li>
                    <li><a href="usuarios.php?subcategoria=21" style="color: #ccc;">Pintor</a></li>
                    <li><a href="usuarios.php?subcategoria=22" style="color: #ccc;">Piscina</a></li>
                    <li><a href="usuarios.php?subcategoria=23" style="color: #ccc;">Segurança eletrônica</a></li>
                    <li><a href="usuarios.php?subcategoria=24" style="color: #ccc;">Serralheiro e soldador</a></li>
                    <li><a href="usuarios.php?subcategoria=25" style="color: #ccc;">Tapeceiro</a></li>
                    <li><a href="usuarios.php?subcategoria=26" style="color: #ccc;">Tarraplanagem</a></li>
                    <li><a href="usuarios.php?subcategoria=27" style="color: #ccc;">Vidraceiro</a></li>
                </ul>
            </div>



            <div class="col-sm-3 col-md-3 col-xs-11  col-lg-3" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Assistência </li>
                    <li><a href="usuarios.php?subcategoria=28" style="color: #ccc;" >Aparelho de Som</a></li>
                    <li><a href="usuarios.php?subcategoria=29" style="color: #ccc;">Ar condicionado</a></li>
                    <li><a href="usuarios.php?subcategoria=30"  style="color: #ccc;">Cabeamento de Redes</a></li>
                    <li><a href="usuarios.php?subcategoria=31"  style="color: #ccc;">Câmara</a></li>
                    <li><a href="usuarios.php?subcategoria=32"  style="color: #ccc;">Celular</a></li>
                    <li><a href="usuarios.php?subcategoria=33"  style="color: #ccc;">Computaddor Desktop</a></li>
                    <li><a href="usuarios.php?subcategoria=34"  style="color: #ccc;">DVD/ BluRay</a></li>
                    <li><a href="usuarios.php?subcategoria=35"  style="color: #ccc;">Eletrodomésticos</a></li>
                    <li><a href="usuarios.php?subcategoria=36"  style="color: #ccc;">Fogão e Cooktop</a></li>
                    <li><a href="usuarios.php?subcategoria=37"  style="color: #ccc;">Galeria e freezer</a></li>
                    <li><a href="usuarios.php?subcategoria=38"  style="color: #ccc;">Home theater</a></li>
                    <li><a href="usuarios.php?subcategoria=39"  style="color: #ccc;">Impressora</a></li>
                    <li><a href="usuarios.php?subcategoria=40"  style="color: #ccc;">Lava louça</a></li>
                    <li><a href="usuarios.php?subcategoria=41"  style="color: #ccc;">Lavadora de roupas</a></li>
                    <li><a href="usuarios.php?subcategoria=42"  style="color: #ccc;">Microondas</a></li>
                    <li><a href="usuarios.php?subcategoria=43"  style="color: #ccc;">Notebook e laptops</a></li>
                    <li><a  href="usuarios.php?subcategoria=45"  style="color: #ccc;">Tablet</a></li>
                    <li><a href="usuarios.php?subcategoria=46"  style="color: #ccc;">Telefones (não celular)</a></li>
                    <li><a href="usuarios.php?subcategoria=47"  style="color: #ccc;">Telefonia PABX</a></li>
                    <li><a   href="usuarios.php?subcategoria=48"  style="color: #ccc;">Televisão</a></li>
                    <li><a  href="usuarios.php?subcategoria=49"  style="color: #ccc;">Video Game</a></li>
                </ul>
            </div>


            <div class="col-sm-3 col-md-3 col-xs-11 col-lg-3" class="rodape_info">
                <ul class="rodape_info">
                    <li class="titulo_rodape">Doméstico e Lar</li>
                    <li><a  href="usuarios.php?subcategoria=134" style="color: #ccc;">Adestrador de cães</a></li>
                    <li><a href="usuarios.php?subcategoria=135" style="color: #ccc;">Babá</a></li>
                    <li><a  href="usuarios.php?subcategoria=136" style="color: #ccc;">Cozinheira</a></li>
                    <li><a  href="usuarios.php?subcategoria=137" style="color: #ccc;">Diarista</a></li>
                    <li><a  href="usuarios.php?subcategoria=138" style="color: #ccc;">Limpeza de piscina</a></li>
                    <li><a  href="usuarios.php?subcategoria=139" style="color: #ccc;">Motorista</a></li>
                    <li><a  href="usuarios.php?subcategoria=141" style="color: #ccc;">Passadeira</a></li>
                    <li><a  href="usuarios.php?subcategoria=142" style="color: #ccc;">Passeador de cães</a></li>
                </ul>
            </div>










            <div class="col-sm-3 col-md-3 col-xs-11  col-lg-3" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Automotivo</li>
                    <li><a href="usuarios.php?subcategoria=61" style="color: #ccc;">Alarme automotivo</a></li>
                    <li><a href="usuarios.php?subcategoria=62" style="color: #ccc;">Ar Condicionado</a></li>
                    <li><a href="usuarios.php?subcategoria=63" style="color: #ccc;">Funilaria</a></li>
                    <li><a href="usuarios.php?subcategoria=64" style="color: #ccc;">Inspeção veicular</a></li>
                    <li><a href="usuarios.php?subcategoria=65" style="color: #ccc;">Insulfilm</a></li>
                    <li><a href="usuarios.php?subcategoria=66" style="color: #ccc;">Martelinho de Ouro</a></li>
                    <li><a href="usuarios.php?subcategoria=67" style="color: #ccc;">Revisão</a></li>
                    <li><a href="usuarios.php?subcategoria=68" style="color: #ccc;">Som</a></li>
                </ul>
            </div>





            <div class="col-sm-3 col-md-3 col-xs-11  col-lg-3" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Aulas</li>
                    <li><a  href="usuarios.php?subcategoria=50" style="color: #ccc;" >Artes e artesanatos</a></li>
                    <li><a  href="usuarios.php?subcategoria=51" style="color: #ccc;">Bem-Estar</a></li>
                    <li><a  href="usuarios.php?subcategoria=52" style="color: #ccc;">Concursos</a></li>
                    <li><a  href="usuarios.php?subcategoria=53" style="color: #ccc;">Aulas de Dança</a></li>
                    <li><a  href="usuarios.php?subcategoria=54" style="color: #ccc;">Aulas particulares</a></li>
                    <li><a  href="usuarios.php?subcategoria=55" style="color: #ccc;">Esportes</a></li>
                    <li><a  href="usuarios.php?subcategoria=56" style="color: #ccc;">Aulas de idiomas</a></li>
                    <li><a  href="usuarios.php?subcategoria=57" style="color: #ccc;">Aulas de informática</a></li>
                    <li><a  href="usuarios.php?subcategoria=58" style="color: #ccc;">Lutas</a></li>
                    <li><a  href="usuarios.php?subcategoria=59" style="color: #ccc;">Aulas de música</a></li>
                </ul>
            </div>









    </div>
    </div>
</div>
<div id="rodape_baixo">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-md-4 col-xs-11">
                <ul class="rodape_info" class="titulo_rodape">
                    <li class="titulo_rodape">2018 - UORKE </li>
                    <li class="titulo_rodape"><img src="img/cnpj.png" height="12px"> CNPJ 00.000.000/0000-00</li>
                    <li class="titulo_rodape"><img src="img/relogio.png" height="12px"> 08h as 18h em dias úteis</li>
                </ul>
            </div>
            <div class="col-sm-4 col-md-4 col-xs-11" >
                <ul class="rodape_info">
                    <li class="titulo_rodape"><img src="img/facebook.png" height="15px"> Facebook</li>
                    <li class="titulo_rodape"><img src="img/instagram.png" height="15px"> Instagram</li>
                    <li class="titulo_rodape"><img src="img/twitter.png" height="15px"> Twitter</li>

                </ul>
            </div>
            <div class="col-sm-4 col-md-4 col-xs-11" >
                <ul class="rodape_info">
                    <li class="titulo_rodape">Clique aqui e cadastre-se 15 dias grátis</li>
                    <li class="titulo_rodape">&nbsp; </li>
                    <li class="titulo_rodape"> <p><a class="btn btn-lg btn-primary" href="cadastro.php" role="button" id="cadastrar">Cadastre-se</a></p></li>

                </ul>
            </div>
        </div>
    </div>
</div>
<div id="ultimo_rodape">
    <img src="img/logo_rodape.png" id="imagem_u_r" width="100%">
    <img src="img/logo-naville.png" id="imagem_u_r_pequeno"  width="40%" style="margin-left: 30%; padding: 1% 0;">
</div>

<!--fecha rodape-->
