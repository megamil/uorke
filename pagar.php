<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="theme-color" content="#f7921c">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/cadastro_cliente.css" rel="stylesheet">



    <link href="css/bt2.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">
    <link rel="stylesheet" href="css/login.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script>


        function efeito(){
            $('#formulario_cliente').animate({'opacity':'1.00'}, 1200, 'linear');
            $('#formulario_cliente').css('display', 'table');
        }
        function enviar(){

        }
    </script>
    <style>
        .textos_input{
            width: 100%;
            margin: 0 auto;
            padding: 1% 0;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        #formulario_cliente{
            display: none;
            opacity: 0;
        }
    </style>

</head>
<!-- NAVBAR
================================================== -->
<body onload="efeito()">

<div id="fb-root"></div>

<div id="tudo" >
    <!-- topo -->
    <?php include('topo.php'); ?>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <div id="formulario_todo" class="container" >







        <form action="#" method="post" enctype="multipart/form-data"  style="opacity: 1;display: table" id="formulario_cliente">

            <input type="hidden" name="id_facebook" id="id_facebook">


            <center>
                <img src="img/logo.png" width="100" max-height="100" id="foto" style="margin-bottom: 10px;">

            </center> <p>&nbsp;</p>

            <p> Numero de cartão</p>
            <p> <input type="text" class="textos_input" id="cartao_numero" maxlength="19"></p>
            <p> Código de validação</p>
            <p> <input type="tel" class="textos_input" id="cartao_codigo" maxlength="3"></p>
            <p> Validade</p>
            <p> <input type="text" class="textos_input" id="cartao_mes" maxlength="2"></p>
            <p> Nome do titular do cartão</p>
            <p> <input type="text" class="textos_input" maxlength="100" ></p>
            <p>&nbsp;</p>


            <!--
                    <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div> -->


            <p><a  role="button" >
                    <button  class="btn btn-lg btn-primary" style="width: 100%;border:none;background: none"  type="submit"  ><img src="img/pagseguro.png"></button>
                </a></p>
        </form>

    </div>


</div>
</div>



<script type="text/javascript">
    /* Máscaras ER */
    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        return v;
    }
    function mtel3(cpf){
        cpf=cpf.replace(/\D/g,"");
        cpf=cpf.replace(/(\d{4})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{4})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{4})(\d{1,4})$/,"$1.$2");

        return cpf;
    }
    function mtel2(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        return v;
    }
    function mtel4(v){
        v=v.replace(/\D/g,"");
        v=v.replace(/^(\d{2})(\d)/g,"($1)"); //Coloca parênteses em volta dos dois primeiros dígitos
        return v;
    }

    function mtel5(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
    function mtel6(cpf){
        cpf=cpf.replace(/\D/g,"");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return cpf;
    }

    function id( el ){
        return document.getElementById( el );
    }
    window.onload = function(){
        id('cartao_codigo').onkeyup = function(){
            mascara( this, mtel );
        }
        id('cartao_numero').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('cartao_numero').onkeyup = function(){
            mascara( this, mtel3 );
        }
        id('cartao_mes').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('cartao_ano').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('cartao_ddd').onkeyup = function(){
            mascara( this, mtel4 );
        }
        id('cartao_telefone').onkeyup = function(){
            mascara( this, mtel5 );
        }
        id('cartao_cpf').onkeyup = function(){
            mascara( this, mtel6 );
        }
    }



</script>

<script src="js/jq1.js"></script>
<script src="js/jq3.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/jq2.js"></script>
</body>
</html>
