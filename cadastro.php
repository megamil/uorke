<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/bt2.css" rel="stylesheet">
    <link href="css/cadastro.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">


    <style>
        .textos{
            width: 39%;
            font-size: 23px;
            margin-left: 12%;
        }
        .textos_titulos{
            padding: 6% 0;
            width: 80%;
            margin: 0 auto;
            font-size: 30px;
        }
        .t1{
            padding: 4% 0;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script>
        function efeito(){
            $('#bloco_meio').animate({'opacity':'1.00'}, 1800, 'linear');
            $('#bloco_meio').css('display', 'table');
        }

    </script>
</head>
<!-- NAVBAR
================================================== -->
<body onload="efeito()">
<div id="tudo">
    <div id="preto"></div>
    <!---------------------- topo ------------->
    <?php include'topo.php' ; ?>
    <!---------------------- fecha topo ------------->
	<div class="col-lg-12" id="bloco_meio">
		<p id="titulo_cetnro">O modo mais prático</p>
		<p class="cor_titulo">de localizar profissionais</p>
		<p class="cor_subtitul">Cadastre-se por 15 dias grátis</p>

    <a href="cadastrar.php">	<div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><img id="botoes" src="img/profissional.png"width="170px" style="float:right;"></div></a>
    <a href="cadastrar_cliente.php"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><img id="botoes" src="img/cliente.png" width="170px"></div></a>
    </div>
    </div>




<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="js/jq1.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="js/jq3.js"></script>
<!-- IE10 viewport hack for Sur -->
</body>
</html>

