<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="theme-color" content="#f7921c">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/bt2.css" rel="stylesheet">
    <link href="css/bt3.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">
    <style>
        .textos{
            width: 70%;
            text-align: justify;
            margin: 0 auto;
            font-size: 18px;
        }
        .textos_titulos{
            font-weight: bolder;
            text-align: center;
            padding: 5% 0;
            width: 100%;
            margin: 0 auto;
            font-size: 23px;
        }
        .t2{
            background: #E2E6F5;
            padding: 4% 0;
        }
        .t1{
            padding: 4% 0;
        }
        #spc{
            height: 500px;
            padding:  2% 0 ;
            margin-top: 0;
            background: url('img/bg_institucion al.png') center center no-repeat;
            background-size: cover;
        }
        a:hover{
            text-decoration: none;
        }


         textarea:focus, input:focus, select:focus {
             box-shadow: ;
             border: 1px solid #f7921c;
             outline: 0;




         }

    </style>
</head>
<!-- NAVBAR
================================================== -->
<body>
<p id="tudo">

    <?php

        include'topo.php';
        echo '<div id="spc"></div>';

    ?>


    <div class="t1">
        <h2 class="textos_titulos"> DÚVIDAS FREQUENTES</h2>

<p class="textos" style="text-align: center;margin-bottom: 10px"> <span style="color: #f7921c; font-weight: bold;font-size: 30px;">1</span>Problemas com Cadastro? </p>
<p class="textos" style="text-align: center"><a href="cadastro.php" style="color: #f7921c">Clique aqui</a> para e faça seu cadastro.</p>

<p>&nbsp;</p>

<p class="textos" style="text-align: center;margin-bottom: 10px"><span style="color: #f7921c; font-weight: bold;font-size: 30px;">2</span> Não está conseguindo diferenciar os cadastros? </p>
<p class="textos" style="text-align: center"><a href="cadastrar.php" style="color: #f7921c">Clique aqui</a> Se deseja ser você deseja ser um profissional.</p>
<p class="textos" style="text-align: center"><a href="cadastrar_cliente.php" style="color: #f7921c">Clique aqui</a> Se deseja ser você deseja encontrar profissionais.</p>


<p>&nbsp;</p>

<p class="textos" style="text-align: center;margin-bottom: 10px"><span style="color: #f7921c; font-weight: bold;font-size: 30px;">3</span> Está com dificuldade para fazer login? </p>
<p class="textos" style="text-align: center"><a href="login.php" style="color: #f7921c">Clique aqui</a> e insira seu E-mail e senha cadastrastados.</p>

<p>&nbsp;</p>

<p class="textos" style="text-align: center;margin-bottom: 10px"><span style="color: #f7921c; font-weight: bold;font-size: 30px;">4</span> Tem dúvidas sobre os valores da Uorke? </p>
<p class="textos" style="text-align: center"><a href="institucional.php" style="color: #f7921c">Clique aqui</a> e veja mais sobre nós.</p>


<p>&nbsp;</p>

<p class="textos" style="text-align: center;margin-bottom: 10px"><span style="color: #f7921c; font-weight: bold;font-size: 30px;">5</span> Quer saber um pouco mais sobre suas privacidades? </p>
<p class="textos" style="text-align: center"><a href="quem_somos.php" style="color: #f7921c">Clique aqui</a> e descubra um pouco mais sobre as privacidades.</p>






<h2 class="textos_titulos">DIGITE UMA NOVA DÚVIDA</h2>

<p class="textos" style="text-align:">
<center><form action="#" method="POST">

<p><input type="text" name="email_d" placeholder="E-mail" style="width: 40%;padding: 10px;border-radius: 8px;background: none;border: 2px solid #f7921c"></p>
        <textarea name="texto_d" style="width: 40%;height: 150px;background: transparent;border: 2px solid #f7921c">

        </textarea>
<p><center><button class="btn btn-lg btn-primary" type="submit" name="enviar_d" role="button" style="width: 40%;display: table" id="cadastrar">Cadastre-se grátis.</centera></button></p>
    </center>

</form>
</p>
</div>

<?php

    if(isset($_POST['enviar_d'])){
        $email=$_POST['email_d'];
        $texto=$_POST['texto_d'];
        if(empty($email)){
            echo('<script>window.alert("Insira seu E-mail ");</script>');
        }
        else{
            if(empty($texto)){
                echo('<script>window.alert("Insira sua Dúvida");</script>');
            }
            else{
                echo('<script>window.alert("'.$email.' Sua dúvida foi enviada");location="index.php"</script>');
            }
        }
    }
?>







</div>


<?php

    include'rodape.php';

?>


<!--fecha rodape-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="js/jq1.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="js/jq3.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/jq2.js"></script>
</body>
</html>
