<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/cadastro_cliente.css" rel="stylesheet">



    <link href="css/bt2.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">
    <link rel="stylesheet" href="css/login.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script>


        function efeito(){
            $('#formulario_cliente').animate({'opacity':'1.00'}, 1200, 'linear');
            $('#formulario_cliente').css('display', 'table');
        }
        function enviar(){

        }
    </script>
    <style>
        .textos_input{
            width: 100%;
            margin: 0 auto;
            padding: 1% 0;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        #formulario_cliente{
            display: none;
            opacity: 0;
        }
    </style>

</head>
<!-- NAVBAR
================================================== -->
<body onload="efeito()">

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=173594486784949";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


     //Exibir imagem ao selecionar.
      function readURL(input, id) {
         if (input.files && input.files[0]) {
             var reader = new FileReader();

             reader.onload = function (e) {
                 $('#'+id)
      .attr('src', e.target.result)
      ;
             }

             reader.readAsDataURL(input.files[0]);
         }
     }

    function callbackMudancaStatus(response){
            if(response.status === 'connected'){
                $('.login').hide().fadeOut(1000);
                $('#status').append('<a onclick="LogarOff()" style="cursor:pointer"> Sair </a>');
                testAPI();
            }else if(response.status === 'not_authorized'){
                $('#status').html('<p>Por favor, faça login na pagina</p>');
            }else{
                //- $('#status').html('<p>Por favor faça login no facebook</p>');
            }
        }
        window.fbAsyncInit = function(){
              FB.init({
              appId:'{minha-id}',
              cookie:true,
              xfbml:true,
              version: 'v2.5'
          });

          FB.getLoginStatus(function(response){
              callbackMudancaStatus(response);
          });
        };
        //Codigo do facebook para inicialização do 
        //JavaScript SDK
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        function testAPI(){
          // URL de onde quer trazer os dados
          //- /me no caso seria /eu <- o usuario que esta fazendo
          // porem nas versões mais novas do SDK é preciso especificar
          // as propriedades do objeto , isso você precisa mostrar na
          // URL , ex: /me?fields=name,email,picture, etc..
          // logo depois ele traz os dados na variavel response
          FB.api('me?fields=name,email,picture' , function(response){
            $('#nome').val(response.name);
            $('#email').val(response.email);
            $('#id_facebook').val(response.id);
            $('#foto').attr('src', 'http://graph.facebook.com/'+response.id+'/picture?type=large');
            
            console.log(response);
          },{
              //é mecessario tornar o email do usuario como publico
              // então é necessario colocar este trecho de codigo
              scope: 'publish_stream,email' 
        });
       }

        function Logar(){
          FB.login(function(response){
            callbackMudancaStatus(response);
          });
        }
        function LogarOff(){
          FB.logout(function(response){
            callbackMudancaStatus(response);
            $('#status').html('<p>Fez Logoff</p>')
          });
        }

        $(document).ready(function(){

            $('form').on('submit', function (e) {
              e.preventDefault();

                var valor = document.getElementById('email').value;


                var validar = valor.split("@");
                if(validar[1]== null){

                    alert('E-mail inválido.');
                    return false;
                }
                else {
                    var validarponto = valor.split(".");
                    if(validarponto[1]== null){
                        alert("E-mail inválido.");
                        return false;
                    }

                }


              var formData = new FormData(this);

                console.log("Cadastro Cliente");

                if ($('#nome').val() != "" &&
                  $('#telefone').val() != "" &&
                  $('#email').val() != "" &&
                  $('#senha').val() != "" &&
                  $('#senha2').val() != "") {

                  if ($('#senha').val() == $('#senha2').val()) {


                    $.ajax({
                      type: "post",
                      url: "admin/Controller_webservice/novo_cliente",
                      data: formData,
                      error: function(returnval) {
                         console.log('Error',returnval,'error');
                         alert('Falha: '+returnval.error);
                      },
                      success: function (returnval) {
                          if(returnval.status) {
                              console.log("Conta criada com sucesso!");

                              $('#nome').val("");
                              $('#email').val("");
                              $('#id_facebook').val("");
                              $('#telefone').val("");
                              $('#senha').val("");
                              $('#foto').attr('src', 'img/logo.png');

                              $('#formulario_cliente').hide();
                              $('#parabens').show();


                          } else {
                              alert("Falha ao criar conta: "+returnval.erro);
                          }

                      },
                        cache: false,
                        contentType: false,
                        processData: false,
                        xhr: function() {  // Custom XMLHttpRequest
                            var myXhr = $.ajaxSettings.xhr();
                            if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                                myXhr.upload.addEventListener('progress', function () {
                                    /* faz alguma coisa durante o progresso do upload */
                                }, false);
                            }
                        return myXhr;
                        }
                    });

                  } else {
                    alert('Senhas divergentes');
                  }

                } else {
                  alert('Campo(s) em branco!');
                }

            });

        });

</script>

<div id="tudo" >
    <!-- topo -->
    <?php include('topo.php'); ?>
<p>&nbsp;</p>
<p>&nbsp;</p>
    <div id="formulario_todo" class="container" >


        <div id="parabens" style="padding-top: 50px;padding-bottom: 50px;background-color: white; margin-top: 150px; border-radius: 20px;" max-width="200px" hidden>
            <center>
                <h1>Obrigado por se registrar!</h1>
                <h5>Baixe nosso APP para Android ou iOS.</h5>
            </center>
        </div>




        <form action="#" method="post" enctype="multipart/form-data" style="opacity: 1;display: table" id="formulario_cliente">

            <input type="hidden" name="id_facebook" id="id_facebook">

            <center><h2 > Cadastre-se</h2></center>
            <center>
            </center>
            <p> Nome</p>
            <p> <input type="text" class="textos_input" name="nome" id="nome"></p>
            <p> Telefone</p>
            <p> <input type="tel" class="textos_input" name="telefone" id="telefone" maxlength="15"></p>
            <p> Email</p>
            <p> <input type="text" class="textos_input" name="email" id="email"></p>
            <p> Senha</p>
            <p> <input type="password" class="textos_input" name="senha" id="senha"></p>
            <p> Confirmar Senha</p>
            <p> <input type="password" class="textos_input" name="senha2" id="senha2"></p>
    <!-- 
            <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div> -->
            <label for='imagem'><img src="img/foto_file.png" width="50px"></label>  <img src="none"  id="foto" width="80px" style="margin-bottom: 10px;margin-left: 20px;border:none"">
            <p style="width: 10px"> <input type="file" id="imagem" name="imagem" width="100"  style="margin-bottom: 20px" onchange="readURL(this,'foto');" />
            </p>

            <!-- <a class="btn btn-block btn-social btn-facebook" onclick="Logar()" style="display:table;background: #3D5193;padding: 3%; margin:1% 0; color: #fff; ">
                <i class="fa fa-facebook" ></i> &nbsp; Cadastrar com o Facebook
            </a> -->
            <p><a  role="button" >
                <button  class="btn btn-lg btn-primary" style="width: 100%;background: #f7921c;border:1px solid #f7921c" id="enviar_cliente" name="enviar_cliente" type="submit"  >Finalizar</button>
            </a></p>
        </form>
        <?php include 'classes/cadastro_cliente.php'; ?>
        </div>


    </div>
</div>
<script src="js/jq1.js"></script>
<script src="js/jq3.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/jq2.js"></script>
</body>
</html>
<style>
    input[type='file'] {
        display: none;
    }

    /* Aparência que terá o seletor de arquivo */
    label {
        border-radius: 5px;
        color: #fff;
        cursor: pointer;
        padding: 2px 2px
    }
</style>

<script type="text/javascript">
    /* Máscaras ER */
    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
    function id( el ){
        return document.getElementById( el );
    }
    window.onload = function(){
        id('telefone').onkeyup = function(){
            mascara( this, mtel );
        }
    }



</script>