<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<div class="navbar-wrapper" id="topo">
    <div  id="topo_cor">
        <nav class="navbar navbar-inverse navbar-static-top" id="topo_bk_none">
            <div class="container" >
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="img/logo.png" width="50%" id="logotipo"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">

                    <div id="topo_cima_tudo">
                        <ul class="nav navbar-nav" id="topo_cima">

                            <!-- fecha dropdown -->

                            <li class="icones_redes"  style="opacity: 1" ><img src="img/instagram.png"  class="icone_rede" height="15px">&nbsp;&nbsp;</li>
                            <li class="icones_redes" style="opacity: 1"><img src="img/facebook.png" class="icone_rede" height="15px">&nbsp;&nbsp;</li>
                            <li class="icones_redes" style="opacity: 1"><img src="img/twitter.png" class="icone_rede" height="15px">&nbsp;&nbsp;</li>
                            <li><!--class="active"--><a href="login.php" id="icone_rede_menu"  style="color: #fff">Login</a></li>
                            <li><a href="cadastro.php" id="icone_rede_menu"  style="color: #fff">Cadastre-se</a></li>
                            <li><a href="institucional.php" id="icone_rede_menu"  style="color: #fff">Institucional</a></li>
                            <li><a href="quem_somos.php" id="icone_rede_menu"  style="color: #fff">Privacidade</a></li>
                            <li><a href="duvidas.php" id="icone_rede_menu" class="dividir_topo"  style="color: #fff">Dúvidas</a></li>



                    </div>

                    <!-- topo baixo -->
                    <div id="topo_baixo_tudo" >
                        <ul class="nav navbar-nav" id="topo_baixo_e" >
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Assistência<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp" >
                                    <li><a href="usuarios.php?subcategoria=28" style="color: #ccc;" >Aparelho de Som</a></li>
                                    <li><a href="usuarios.php?subcategoria=29" style="color: #ccc;">Ar condicionado</a></li>
                                    <li><a href="usuarios.php?subcategoria=30"  style="color: #ccc;">Cabbeamento de Redes</a></li>
                                    <li><a href="usuarios.php?subcategoria=31"  style="color: #ccc;">Câmara</a></li>
                                    <li><a href="usuarios.php?subcategoria=32"  style="color: #ccc;">Celular</a></li>
                                    <li><a href="usuarios.php?subcategoria=33"  style="color: #ccc;">Computaddor Desktop</a></li>
                                    <li><a href="usuarios.php?subcategoria=34"  style="color: #ccc;">DVD/ BluRay</a></li>
                                    <li><a href="usuarios.php?subcategoria=35"  style="color: #ccc;">Eletrodomésticos</a></li>
                                    <li><a href="usuarios.php?subcategoria=36"  style="color: #ccc;">Fogão e Cooktop</a></li>
                                    <li><a href="usuarios.php?subcategoria=37"  style="color: #ccc;">Galeria e freezer</a></li>
                                    <li><a href="usuarios.php?subcategoria=38"  style="color: #ccc;">Home theater</a></li>
                                    <li><a href="usuarios.php?subcategoria=39"  style="color: #ccc;">Impressora</a></li>
                                    <li><a href="usuarios.php?subcategoria=40"  style="color: #ccc;">Lava louça</a></li>
                                    <li><a href="usuarios.php?subcategoria=41"  style="color: #ccc;">Lavadora de roupas</a></li>
                                    <li><a href="usuarios.php?subcategoria=42"  style="color: #ccc;">Microondas</a></li>
                                    <li><a href="usuarios.php?subcategoria=43"  style="color: #ccc;">Notebook e laptops</a></li>
                                    <li><a  href="usuarios.php?subcategoria=45"  style="color: #ccc;">Tablet</a></li>
                                    <li><a href="usuarios.php?subcategoria=46"  style="color: #ccc;">Telefones (não celular)</a></li>
                                    <li><a href="usuarios.php?subcategoria=47"  style="color: #ccc;">Telefonia PABX</a></li>
                                    <li><a   href="usuarios.php?subcategoria=48"  style="color: #ccc;">Televisão</a></li>
                                    <li><a  href="usuarios.php?subcategoria=49"  style="color: #ccc;">Video Game</a></li>
                                </ul>
                            </li>
                            <!-- fecha dropdown -->








                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Aulas<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp">
                                    <li><a  href="usuarios.php?subcategoria=50" style="color: #ccc;" >Artes e artesanatos</a></li>
                                    <li><a  href="usuarios.php?subcategoria=51" style="color: #ccc;">Bem-Estar</a></li>
                                    <li><a  href="usuarios.php?subcategoria=52" style="color: #ccc;">Concursos</a></li>
                                    <li><a  href="usuarios.php?subcategoria=53" style="color: #ccc;">Aulas de Dança</a></li>
                                    <li><a  href="usuarios.php?subcategoria=54" style="color: #ccc;">Aulas particulares</a></li>
                                    <li><a  href="usuarios.php?subcategoria=55" style="color: #ccc;">Esportes</a></li>
                                    <li><a  href="usuarios.php?subcategoria=56" style="color: #ccc;">Aulas de idiomas</a></li>
                                    <li><a  href="usuarios.php?subcategoria=57" style="color: #ccc;">Aulas de informática</a></li>
                                    <li><a  href="usuarios.php?subcategoria=58" style="color: #ccc;">Lutas</a></li>
                                    <li><a  href="usuarios.php?subcategoria=59" style="color: #ccc;">Aulas de música</a></li>

                                </ul>
                            </li>
                            <!-- fecha dropdown -->




                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Tecnologias e Designs<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp">
                                    <li><a href="usuarios.php?subcategoria=83" style="color: #ccc;" >Animação</a></li>
                                    <li><a href="usuarios.php?subcategoria=84"  style="color: #ccc;">APP para smarthphone</a></li>
                                    <li><a href="usuarios.php?subcategoria=85" style="color: #ccc;" >Áudio e vídeo</a></li>
                                    <li><a href="usuarios.php?subcategoria=86" style="color: #ccc;">Modelagem 2D e 3D</a></li>
                                    <li><a href="usuarios.php?subcategoria=87" style="color: #ccc;">Convites</a></li>
                                    <li><a href="usuarios.php?subcategoria=88" style="color: #ccc;">Criação de Logos</a></li>
                                    <li><a href="usuarios.php?subcategoria=89" style="color: #ccc;" >Desenvolvimento de Sites</a></li>
                                    <li><a href="usuarios.php?subcategoria=90" style="color: #ccc;">Diagramador</a></li>
                                    <li><a href="usuarios.php?subcategoria=91" style="color: #ccc;">Edição de fotos</a></li>
                                    <li><a href="usuarios.php?subcategoria=92" style="color: #ccc;">Ilustralção</a></li>
                                    <li><a href="usuarios.php?subcategoria=93" style="color: #ccc;">Marketing online</a></li>
                                    <li><a href="usuarios.php?subcategoria=94" style="color: #ccc;">Materiais promocionais</a></li>
                                    <li><a href="usuarios.php?subcategoria=96" style="color: #ccc;">Produção gráfica</a></li>
                                    <li><a href="usuarios.php?subcategoria=97" style="color: #ccc;">Web Design</a></li>
                                </ul>
                            </li>
                            <!-- fecha dropdown -->


                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Automotivo<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp">
                                    <li><a href="usuarios.php?subcategoria=61" style="color: #ccc;">Alarme automotivo</a></li>
                                    <li><a href="usuarios.php?subcategoria=62" style="color: #ccc;">Ar Condicionado</a></li>
                                    <li><a href="usuarios.php?subcategoria=63" style="color: #ccc;">Funilaria</a></li>
                                    <li><a href="usuarios.php?subcategoria=64" style="color: #ccc;">Inspeção veicular</a></li>
                                    <li><a href="usuarios.php?subcategoria=65" style="color: #ccc;">Insufilm</a></li>
                                    <li><a href="usuarios.php?subcategoria=66" style="color: #ccc;">Martelinho de Ouro</a></li>
                                    <li><a href="usuarios.php?subcategoria=67" style="color: #ccc;">Revisão</a></li>
                                    <li><a href="usuarios.php?subcategoria=68" style="color: #ccc;">Som</a></li>
                                </ul>
                            </li>








                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Reformas e Construção Civil<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp" >
                                    <li><a href="usuarios.php?subcategoria=1" style="color: #ccc;">Arquiteto</a></li>
                                    <li><a href="usuarios.php?subcategoria=2" style="color: #ccc;">Automação Residencial</a></li>
                                    <li><a href="usuarios.php?subcategoria=3" style="color: #ccc;">Chaveiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=4" style="color: #ccc;">Decorador</a></li>
                                    <li><a href="usuarios.php?subcategoria=5" style="color: #ccc;">Dedetizador</a></li>
                                    <li><a href="usuarios.php?subcategoria=6" style="color: #ccc;">Desentupidor</a></li>
                                    <li><a href="usuarios.php?subcategoria=7" style="color: #ccc;">Eletricista</a></li>
                                    <li><a href="usuarios.php?subcategoria=8" style="color: #ccc;">Encanador</a></li>
                                    <li><a href="usuarios.php?subcategoria=9" style="color: #ccc;">Engenheiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=10" style="color: #ccc;">Gesso e Drywall</a></li>
                                    <li><a href="usuarios.php?subcategoria=11" style="color: #ccc;">Impermeabilizador</a></li>
                                    <li><a href="usuarios.php?subcategoria=12" style="color: #ccc;">Jardineiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=13" style="color: #ccc;">Limpeza Pós-Obra</a></li>
                                    <li><a href="usuarios.php?subcategoria=14" style="color: #ccc;">Marceneiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=15" style="color: #ccc;">Marido de aluguel</a></li>
                                    <li><a href="usuarios.php?subcategoria=16" style="color: #ccc;">Montador de móveis</a></li>
                                    <li><a href="usuarios.php?subcategoria=17" style="color: #ccc;">Mudanças e carretos</a></li>
                                    <li><a href="usuarios.php?subcategoria=19" style="color: #ccc;">Paisagista </a></li>
                                    <li><a href="usuarios.php?subcategoria=20" style="color: #ccc;">Pedreiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=21" style="color: #ccc;">Pintor</a></li>
                                    <li><a href="usuarios.php?subcategoria=22" style="color: #ccc;">Piscina</a></li>
                                    <li><a href="usuarios.php?subcategoria=23" style="color: #ccc;">Segurança eletrônica</a></li>
                                    <li><a href="usuarios.php?subcategoria=24" style="color: #ccc;">Serralheiro e soldador</a></li>
                                    <li><a href="usuarios.php?subcategoria=25" style="color: #ccc;">Tapeceiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=26" style="color: #ccc;">Tarraplanagem</a></li>
                                    <li><a href="usuarios.php?subcategoria=27" style="color: #ccc;">Vidraceiro</a></li>
                                </ul>
                            </li>


                            <!-- fecha dropdown -->




                            <!-- fecha dropdown -->



                        </ul>


                    </div>
                        <div id="topo_baixo_tudo" >
                        <ul class="nav navbar-nav" style="margin-top: -10px" id="topo_baixo_e" >


                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Moda e Estética<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp">
                                    <li><a  href="usuarios.php?subcategoria=114" style="color: #ccc;" >Artesanato</a></li>
                                    <li><a  href="usuarios.php?subcategoria=115" style="color: #ccc;" >Cabeleleiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=116" style="color: #ccc;">Corte e costura</a></li>
                                    <li><a href="usuarios.php?subcategoria=117" style="color: #ccc;">Depilação</a></li>
                                    <li><a href="usuarios.php?subcategoria=118" style="color: #ccc;">Design de sombrancelhas</a></li>
                                    <li><a href="usuarios.php?subcategoria=119" style="color: #ccc;">Estérico</a></li>
                                    <li><a href="usuarios.php?subcategoria=120" style="color: #ccc;">Esteticista</a></li>
                                    <li><a href="usuarios.php?subcategoria=121" style="color: #ccc;">Manicure e pedicure</a></li>
                                    <li><a href="usuarios.php?subcategoria=122" style="color: #ccc;">Maquiadores</a></li>
                                    <li><a href="usuarios.php?subcategoria=125" style="color: #ccc;">Sapateiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=124" style="color: #ccc;">Personal stylist</a></li>
                                    <li><a href="usuarios.php?subcategoria=126" style="color: #ccc;">Artesanato</a></li>
                                </ul>
                            </li>
                            <!-- fecha dropdown -->







                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Consultoria<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp">
                                    <li><a href="usuarios.php?subcategoria=72" style="color: #ccc;">Consultor pessoal</a></li>
                                    <li><a href="usuarios.php?subcategoria=73" style="color: #ccc;">Consultoria especializada</a></li>
                                    <li><a href="usuarios.php?subcategoria=74" style="color: #ccc;">Contador</a></li>
                                    <li><a href="usuarios.php?subcategoria=75" style="color: #ccc;">Detetive Particular</a></li>
                                    <li><a href="usuarios.php?subcategoria=76" style="color: #ccc;">Digitalizar documentos</a></li>
                                    <li><a href="usuarios.php?subcategoria=77" style="color: #ccc;">Economia e finanças</a></li>
                                    <li><a href="usuarios.php?subcategoria=79" style="color: #ccc;">Pesquisas em geral</a></li>
                                    <li><a href="usuarios.php?subcategoria=80" style="color: #ccc;">Produção de conteúdo</a></li>
                                    <li><a href="usuarios.php?subcategoria=81" style="color: #ccc;">Segurança do trabalho</a></li>
                                    <li><a href="usuarios.php?subcategoria=82" style="color: #ccc;">Tradutores</a></li>

                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Doméstico e Lar<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp">
                                    <li><a  href="usuarios.php?subcategoria=134" style="color: #ccc;">Adestrador de cães</a></li>
                                    <li><a href="usuarios.php?subcategoria=135" style="color: #ccc;">Babá</a></li>
                                    <li><a  href="usuarios.php?subcategoria=136" style="color: #ccc;">Cozinheira</a></li>
                                    <li><a  href="usuarios.php?subcategoria=137" style="color: #ccc;">Diarista</a></li>
                                    <li><a  href="usuarios.php?subcategoria=138" style="color: #ccc;">Limpeza de piscina</a></li>
                                    <li><a  href="usuarios.php?subcategoria=139" style="color: #ccc;">Motorista</a></li>
                                    <li><a  href="usuarios.php?subcategoria=141" style="color: #ccc;">Passadeira</a></li>
                                    <li><a  href="usuarios.php?subcategoria=142" style="color: #ccc;">Passeador de cães</a></li>
                                </ul>
                            </li>
                            <!-- fecha dropdown -->


                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff">Saúde/ Bem Estar<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp">
                                    <li><a href="usuarios.php?subcategoria=127" style="color: #ccc;" >Acompanhante de idosos</a></li>
                                    <li><a href="usuarios.php?subcategoria=128" style="color: #ccc;">Enfermeira</a></li>
                                    <li><a href="usuarios.php?subcategoria=129" style="color: #ccc;">Fisioterapeuta</a></li>
                                    <li><a href="usuarios.php?subcategoria=130" style="color: #ccc;">Fonoaudiólogo</a></li>
                                    <li><a href="usuarios.php?subcategoria=131" style="color: #ccc;">Nutricionista</a></li>
                                    <li><a href="usuarios.php?subcategoria=132" style="color: #ccc;">Psicólogo</a></li>
                                    <li><a href="usuarios.php?subcategoria=133" style="color: #ccc;">Quiroprático</a></li>
                                </ul>
                            </li>
                            <!-- fecha dropdown -->


                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"  style="color: #fff" >Eventos<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="opcoes_dp">
                                    <li><a href="usuarios.php?subcategoria=98" style="color: #ccc;" >Animação de festas</a></li>
                                    <li><a href="usuarios.php?subcategoria=99" style="color: #ccc;" >Assessor de eventos</a></li>
                                    <li><a href="usuarios.php?subcategoria=100" style="color: #ccc;" >Bandas e cantores</a></li>
                                    <li><a href="usuarios.php?subcategoria=101" style="color: #ccc;" >Bartenders</a></li>
                                    <li><a href="usuarios.php?subcategoria=102" style="color: #ccc;" >Brindes e Lembrancinhas</a></li>
                                    <li><a href="usuarios.php?subcategoria=103" style="color: #ccc;" >Buffet completo</a></li>
                                    <li><a href="usuarios.php?subcategoria=104" style="color: #ccc;" >Churrasqueiro</a></li>
                                    <li><a href="usuarios.php?subcategoria=105" style="color: #ccc;" >Decoração</a></li>
                                    <li><a href="usuarios.php?subcategoria=106" style="color: #ccc;" >Djs</a></li>
                                    <li><a href="usuarios.php?subcategoria=107" style="color: #ccc;" >Equipamentos para festas</a></li>
                                    <li><a href="usuarios.php?subcategoria=108" style="color: #ccc;" >Fotografia</a></li>
                                    <li><a href="usuarios.php?subcategoria=109" style="color: #ccc;" >Garçons e copeiras</a></li>
                                    <li><a href="usuarios.php?subcategoria=110" style="color: #ccc;" >Gravação de vídeos</a></li>
                                    <li><a href="usuarios.php?subcategoria=112" style="color: #ccc;" >Recepcionistas</a></li>
                                    <li><a href="usuarios.php?subcategoria=113" style="color: #ccc;" >Segurança</a></li>
                                </ul>
                            </li>
                            <!-- fecha dropdown -->

                        </ul>



                        </ul>

                    </div>

                </div>
            </div>
        </nav>

    </div>
</div>
