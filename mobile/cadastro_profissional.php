<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="theme-color" content="#f7921c">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/bt2.css" rel="stylesheet">



    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="../css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <!-- <link rel="stylesheet" href="../css/home.css" type="text/css"> -->
    <!-- <link rel="stylesheet" href="../css/login.css" type="text/css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- <script type="text/javascript" src="../js/jquery.maskedinput.js"></script> -->

    <script src="../js/js1.js"></script>
    <!-- <script src="../js/mask.js"></script> -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLIqubFVjemLL6IvV9TH5K6G7NAEWbhjg&libraries=places">
    </script>


    <script>
        function efeito(){
            $('#formulario_todo').animate({'opacity':'1.00'}, 1200, 'linear');
            $('#formulario_todo').css('display', 'table');
        }
    </script>
    <style>
        body {
            margin: 0px;
            padding: 0px;
        }

        #tudo{
            width: 100%;
            min-height: 100%;
            background:url("../img/slide1_2.png") center center no-repeat;
            background-size: cover;
            position: absolute;
            display: table;
        }
        #formulario_todo{

            width: 100%;
            opacity: 0;
            margin-top: 1%;
            display: none;
        }
        #formulario_profissional{
            margin-top: 5%;
        }
        .textos_input{
            width: 100%;
            margin: 0 auto;
            padding: 1% 0;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
    @media screen and (min-width: 0px) and (max-width: 500px)
    {
        #formulario_profissional{
            width: 100%;
        }
    }
    @media screen and (min-width: 501px){
            #formulario_profissional{
                width: 100%;
            }
        }
        @media screen and (max-width: 768px) {
            #spac{
                display: none;
            }
        }
        #spac{
            height: 10px;
        }



        @media screen and (min-width: 1200px) and (max-width: 3000px) {
            #formulario_todo{
                margin-top: 5%;
            }
        }
    </style>
    <style>
        #formulario_todo{
            padding: 0px;
        }
        @media screen and (min-width: 391px) and (max-width: 400px) {
            #formulario_todo{
                padding: 0px;
            }
        }

        .select2-container{
            width:100%;
        }
        @media screen and (min-width: 370px) and (max-width: 400px) {
            .select2-container--default .select2-selection--multiple{
                width: 100%;

            }
        }
        @media screen and (min-width: 350px) and (max-width: 370px) {
            .select2-container--default .select2-selection--multiple{
                width: 90%;

            }
        }
        @media screen and (min-width: 330px) and (max-width: 300px) {
            .select2-container--default .select2-selection--multiple{
                width: 75%;

            }
        }
    </style>

</head>
<!-- NAVBAR
================================================== -->
<body onload="efeito()">
    <!-- topo -->
    <div class="col-xs-12">

        <div id="parabens" style="background-color: white;" hidden>
            <center>
                <h1>Obrigado por se registrar!</h1>
                <h3>Baixe nosso APP para Android ou iOS.</h3>
            </center>
        </div>

        <form action="#" method="post" class="center-block" enctype="multipart/form-data" id="formulario_profissional" style="display:table;background: #fff; margin:0 auto; margin-top:0%;padding: 3%;border-radius: 5px;" >
            <center><h2 style="margin-top: 20px"> Cadastre-se</h2></center>

            <center style="margin-bottom: 20px;margin-top: 20px;">
            </center>

            <input type="hidden" name="lat_usuario" id="lat_usuario">
            <input type="hidden" name="long_usuario" id="long_usuario">

            <input type="hidden" name="bairro_profissional" id="bairro_profissional">
            <input type="hidden" name="cidade_profissional" id="cidade_profissional">
            <input type="hidden" name="estado_profissional" id="estado_profissional">
            <input type="hidden" name="pais_profissional"   id="pais_profissional">

            <div class="col-lg-6 col-md-6 col-sm-6">
            <p> Nome</p>
            <p> <input type="text" class="textos_input" name="nome_profissional" id="nome_profissional"></p>
            <p> Email</p>
            <p> <input type="email" class="textos_input" name="email_profissional" id="email_profissional"></p>

            <p> CPF</p>
            <p> <input type="tel" class="textos_input" name="cpf_profissional" id="cpf_profissional" maxlength="14"></p>

            <p>  Senha</p>
            <p> <input type="password" class="textos_input" name="senha_profissional" id="senha_profissional"></p>
            <p> Confirmar senha</p>
            <p> <input type="password" class="textos_input" name="senha_profissional2" id="senha_profissional2"></p>
            <p> Telefone</p>
            <p> <input type="tel" class="textos_input" name="telefone_profissional" maxlength="15" id="telefone_profissional"></p>
            <p> CEP</p>
            <p> <input type="tel" class="textos_input cep" name="cep_profissional" id="cep_profissional" maxlength="8" placeholder="Digite o CEP" id="cep_profissional"></p>
            <p> Localização</p>
            <p> <input type="text" class="textos_input" name="localizacao_profissional" id="localizacao_profissional"></p>
            <p> Número</p>
            <p> <input type="text" class="textos_input" name="numero" id="numero" maxlength="15" id=""></p>


            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">


            <p> Qual(is) sua(s) profissão(ões) ?</p>
            <p>
            <select class="select2" style="width: 100%" multiple name="fk_sub_categoria[]" id="fk_sub_categoria">
                <option value="1">REFORMAS -> Arquiteto</option>
                <option value="2">REFORMAS -> Automação Residencial</option>
                <option value="3">REFORMAS -> Chaveiro</option>
                <option value="4">REFORMAS -> Decorador</option>
                <option value="5">REFORMAS -> Dedetizador   </option>
                <option value="6">REFORMAS -> Desentupidor</option>
                <option value="7">REFORMAS -> Eletricista</option>
                <option value="8">REFORMAS -> Encanador</option>
                <option value="9">REFORMAS -> Engenheiro</option>
                <option value="10">REFORMAS -> Gesso e drywall</option>
                <option value="11">REFORMAS -> Impermeabilizador</option>
                <option value="12">REFORMAS -> Jardinagem</option>
                <option value="13">REFORMAS -> Limpeza Pós-Obra</option>
                <option value="14">REFORMAS -> Marceneiro</option>
                <option value="15">REFORMAS -> Marido de aluguel</option>
                <option value="16">REFORMAS -> Montador de móveis</option>
                <option value="17">REFORMAS -> Mudanças e carretos</option>
                <option value="19">REFORMAS -> Paisagista       </option>
                <option value="20">REFORMAS -> Pedreiro</option>
                <option value="21">REFORMAS -> Pintor</option>
                <option value="22">REFORMAS -> Piscina</option>
                <option value="23">REFORMAS -> Segurança eletrônica</option>
                <option value="24">REFORMAS -> Serralheria e solda</option>
                <option value="25">REFORMAS -> Tapeceiro</option>
                <option value="26">REFORMAS -> Terraplanagem</option>
                <option value="27">REFORMAS -> Vidraceiro</option>
                <option value="28">ASSISTÊNCIA TÉCNICA -> Aparelhos de Som</option>
                <option value="29">ASSISTÊNCIA TÉCNICA -> Ar condicionado</option>
                <option value="30">ASSISTÊNCIA TÉCNICA -> Cabeamento e Redes</option>
                <option value="31">ASSISTÊNCIA TÉCNICA -> Câmera</option>
                <option value="32">ASSISTÊNCIA TÉCNICA -> Celular</option>
                <option value="33">ASSISTÊNCIA TÉCNICA -> Computador desktop</option>
                <option value="34">ASSISTÊNCIA TÉCNICA -> DVD / Blu-Ray</option>
                <option value="35">ASSISTÊNCIA TÉCNICA -> Eletrodomésticos</option>
                <option value="36">ASSISTÊNCIA TÉCNICA -> Fogão e Cooktop</option>
                <option value="37">ASSISTÊNCIA TÉCNICA -> Geladeira e freezer</option>
                <option value="38">ASSISTÊNCIA TÉCNICA -> Home theater</option>
                <option value="39">ASSISTÊNCIA TÉCNICA -> Impressora</option>
                <option value="40">ASSISTÊNCIA TÉCNICA -> Lava Louça</option>
                <option value="41">ASSISTÊNCIA TÉCNICA -> Lava Roupas</option>
                <option value="42">ASSISTÊNCIA TÉCNICA -> Microondas</option>
                <option value="43">ASSISTÊNCIA TÉCNICA -> Notebooks e laptops</option>
                <option value="45">ASSISTÊNCIA TÉCNICA -> Tablet</option>
                <option value="46">ASSISTÊNCIA TÉCNICA -> Telefones (não celular)</option>
                <option value="47">ASSISTÊNCIA TÉCNICA -> Telefonia PABX</option>
                <option value="48">ASSISTÊNCIA TÉCNICA -> Televisão</option>
                <option value="49">ASSISTÊNCIA TÉCNICA -> Video game</option>
                <option value="50">AULAS -> Artes e artesanatos</option>
                <option value="51">AULAS -> Bem-Estar</option>
                <option value="52">AULAS -> Concursos</option>
                <option value="53">AULAS -> Aulas de Dança</option>
                <option value="54">AULAS -> Aulas particulares</option>
                <option value="55">AULAS -> Esportes</option>
                <option value="56">AULAS -> Aulas de idiomas</option>
                <option value="57">AULAS -> Aulas de informática</option>
                <option value="58">AULAS -> Lutas</option>
                <option value="59">AULAS -> Aulas de música</option>
                <option value="61">AUTOS -> Alarme automotivo</option>
                <option value="62">AUTOS -> Ar Condicionado</option>
                <option value="63">AUTOS -> Funilaria</option>
                <option value="64">AUTOS -> Inspeção veicular</option>
                <option value="65">AUTOS -> Insufilm</option>
                <option value="66">AUTOS -> Martelinho de Ouro</option>
                <option value="67">AUTOS -> Revisão</option>
                <option value="68">AUTOS -> Som Automotivo</option>
                <option value="69">CONSULTORIA -> Advogados</option>
                <option value="70">CONSULTORIA -> Assessoria de imprensa</option>
                <option value="71">CONSULTORIA -> Auxilio administrativo</option>
                <option value="72">CONSULTORIA -> Consultor pessoal</option>
                <option value="73">CONSULTORIA -> Consultoria especializada</option>
                <option value="74">CONSULTORIA -> Contador</option>
                <option value="75">CONSULTORIA -> Detetive Particular</option>
                <option value="76">CONSULTORIA -> Digitalizar documentos</option>
                <option value="77">CONSULTORIA -> Economia e finanças</option>
                <option value="79">CONSULTORIA -> Pesquisas em geral</option>
                <option value="80">CONSULTORIA -> Produção de conteúdo</option>
                <option value="81">CONSULTORIA -> Segurança do trabalho</option>
                <option value="82">CONSULTORIA -> Tradutores</option>
                <option value="83">DESIGN E TECNOLOGIA -> Animação</option>
                <option value="84">DESIGN E TECNOLOGIA -> Apps para smartphone</option>
                <option value="85">DESIGN E TECNOLOGIA -> Áudio e vídeo</option>
                <option value="86">DESIGN E TECNOLOGIA -> Modelagem 2D e 3D</option>
                <option value="87">DESIGN E TECNOLOGIA -> Convites</option>
                <option value="88">DESIGN E TECNOLOGIA -> Criação de logos</option>
                <option value="89">DESIGN E TECNOLOGIA -> Desenvolvimento de sites</option>
                <option value="90">DESIGN E TECNOLOGIA -> Diagramador</option>
                <option value="91">DESIGN E TECNOLOGIA -> Edição de fotos</option>
                <option value="92">DESIGN E TECNOLOGIA -> Ilustração</option>
                <option value="93">DESIGN E TECNOLOGIA -> Marketing online</option>
                <option value="94">DESIGN E TECNOLOGIA -> Materiais promocionais</option>
                <option value="96">DESIGN E TECNOLOGIA -> Produção gráfica</option>
                <option value="97">DESIGN E TECNOLOGIA -> Web Design</option>
                <option value="98">EVENTOS -> Animação de festas</option>
                <option value="99">EVENTOS -> Assessor de eventos</option>
                <option value="100">EVENTOS -> Bandas e cantores</option>
                <option value="101">EVENTOS -> Bartenders</option>
                <option value="102">EVENTOS -> Brindes e Lembrancinhas</option>
                <option value="103">EVENTOS -> Buffet completo</option>
                <option value="104">EVENTOS -> Churrasqueiro</option>
                <option value="105">EVENTOS -> Decoração</option>
                <option value="106">EVENTOS -> Djs</option>
                <option value="107">EVENTOS -> Equipamentos para festas</option>
                <option value="108">EVENTOS -> Fotografia</option>
                <option value="109">EVENTOS -> Garçons e copeiras</option>
                <option value="110">EVENTOS -> Gravação de vídeos</option>
                <option value="112">EVENTOS -> Recepcionistas</option>
                <option value="113">EVENTOS -> Segurança</option>
                <option value="114">MODA E BELEZA -> Artesanato</option>
                <option value="115">MODA E BELEZA -> Cabeleireiros  </option>
                <option value="116">MODA E BELEZA -> Corte e costura</option>
                <option value="117">MODA E BELEZA -> Depilação</option>
                <option value="118">MODA E BELEZA -> Design de sobrancelhas</option>
                <option value="119">MODA E BELEZA -> Esotérico</option>
                <option value="120">MODA E BELEZA -> Esteticista</option>
                <option value="121">MODA E BELEZA -> Manicure e pedicure</option>
                <option value="122">MODA E BELEZA -> Maquiadores</option>
                <option value="124">MODA E BELEZA -> Personal stylist</option>
                <option value="125">MODA E BELEZA -> Sapateiro</option>
                <option value="126">MODA E BELEZA -> Artesanato</option>
                <option value="127">SAÚDE -> Acompanhante de idosos</option>
                <option value="128">SAÚDE -> Enfermeira</option>
                <option value="129">SAÚDE -> Fisioterapeuta</option>
                <option value="130">SAÚDE -> Fonoaudiólogo</option>
                <option value="131">SAÚDE -> Nutricionista</option>
                <option value="132">SAÚDE -> Psicólogo</option>
                <option value="133">SAÚDE -> Quiroprático</option>
                <option value="134">SERVIÇOS DOMÉSTICOS -> Adestrador de cães</option>
                <option value="135">SERVIÇOS DOMÉSTICOS -> Babá</option>
                <option value="136">SERVIÇOS DOMÉSTICOS -> Cozinheira</option>
                <option value="137">SERVIÇOS DOMÉSTICOS -> Diarista</option>
                <option value="138">SERVIÇOS DOMÉSTICOS -> Limpeza de piscina</option>
                <option value="139">SERVIÇOS DOMÉSTICOS -> Motorista</option>
                <option value="141">SERVIÇOS DOMÉSTICOS -> Passadeira</option>
                <option value="142">SERVIÇOS DOMÉSTICOS -> Passeador de cães</option>

            </select>
            <p> Selecione uma foto</p>
                <label for='imagem'><img src="../img/foto_file.png" width="50px"></label>  <img src="" id="mini_foto_new" width="80px" style="margin-bottom: 10px;margin-left: 20px;border:none">

                <p><input type="file" id="imagem" name="imagem" width="100" onchange="readURL(this,'mini_foto_new');" /></p>

            <p> Descrição</p>
             <textarea style="width: 100%;height:100px;margin-bottom: 5%" name="breve_descricao_profissional" id="breve_descricao_profissional"></textarea>
            <p><button class="btn btn-lg btn-primary" style="width: 100%;" id="enviar_profissional" type="submit">Finalizar</button></p>

            </div>

        </form>
    </div>

    <center>
      <a href="../quem_somos.php?mobile=true">Termos de uso / Política de Privacidade</a>
    </center>

</div>
</div>

    

<style>
    input[type='file'] {
        display: none;
    }

    /* Aparência que terá o seletor de arquivo */
    label {
        border-radius: 5px;
        color: #fff;
        cursor: pointer;
        padding: 2px 2px
    }
</style>

<script type="text/javascript">

    //Exibir imagem ao selecionar.
        function readURL(input, id) {
           if (input.files && input.files[0]) {
               var reader = new FileReader();

               reader.onload = function (e) {
                   $('#'+id)
        .attr('src', e.target.result)
        ;
               }

               reader.readAsDataURL(input.files[0]);
           }
       }

    $(document).ready(function () {

        $('.select2').select2({
            width: 'resolve',
            multiple: true,
            placeholder: 'Selecione...'
        });

        $('form').on('submit', function (e) {

            e.preventDefault();

            var valor = document.getElementById('email_profissional').value;


            var validar = valor.split("@");
            if(validar[1]== null){

                alert('E-mail inválido.');
                return false;
            }
            else {
                var validarponto = valor.split(".");
                if(validarponto[1]== null){
                    alert("E-mail inválido.");
                    return false;
                }

            }

            console.log("Cadastro Profissional");
            var formData = new FormData(this);

            if ($('#nome_profissional').val() != "" &&
              $('#email_profissional').val() != "" &&
              $('#senha_profissional').val() != "" &&
              $('#senha_profissional2').val() != "" &&
              
              $('#cep_profissional').val() != "" &&
              $('#bairro_profissional').val() != "" &&
              $('#cidade_profissional').val() != "" &&
              $('#estado_profissional').val() != "" &&
              $('#pais_profissional').val() != "" &&

              $('#telefone_profissional').val() != "" &&
              $('#localizacao_profissional').val() != "" &&
              $('#breve_descricao_profissional').val() != "") {

              if ($('#senha_profissional').val() == $('#senha_profissional2').val()) {

                $('#enviar_profissional').attr('disabled',true);
                $('#enviar_profissional').text('Enviando...');

                $.ajax({
                    url: "../admin/Controller_webservice/novo_profissional",
                    type: 'POST',
                    data: formData,
                      error: function(returnval) {
                         console.log('Error',returnval,'error');
                         alert('Falha: '+returnval.error);
                         $('#enviar_profissional').attr('disabled',false);
                         $('#enviar_profissional').text('Finalizar');
                      },
                      success: function (returnval) {
                          if(returnval.status) {
                              alert("Conta criada com sucesso!");
                                $('#nome_profissional').val("");
                                $('#email_profissional').val("");
                                $('#senha_profissional').val("");
                                $('#senha_profissional2').val("");
                                $('#telefone_profissional').val("");
                                $('#localizacao_profissional').val("");
                                $('#cep_profissional').val("");
                                $('#fk_sub_categoria').select2("val", "");
                                $('#breve_descricao_profissional').val("");
                                window.location = "../pagar_cadastro.php";
                                $('#imagem').attr('src', 'img/logo.png');

                          } else {
                              alert("Falha ao criar conta: "+returnval.erro);
                          }

                      },
                    cache: false,
                    contentType: false,
                    processData: false,
                    xhr: function() {  // Custom XMLHttpRequest
                        var myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                            myXhr.upload.addEventListener('progress', function () {
                                /* faz alguma coisa durante o progresso do upload */
                            }, false);
                        }
                    return myXhr;
                    }
                });

              } else {
                alert('Senhas divergentes');
              }

            } else {
              alert('Campo(s) em branco!');
            }

        });

        //Pegar localização, conforme digita e guardar Nome, Latitude e Longitude ao selecionar, também busca o CEP
        geocoder = new google.maps.Geocoder();
        
        input = document.getElementById("cep_profissional");
        //autocomplete = new google.maps.places.Autocomplete(input);

        $(document).on('keyup','#cep_profissional',function(){
            //Após digitar 5 caracteres.
            if ($('#cep_profissional').val().length > 7) {

                console.log("change");

                // google.maps.event.addListener(autocomplete, 'place_changed', function () {
                //     var place = autocomplete.getPlace();

                //     document.getElementById('lat_usuario').value = place.geometry.location.lat();
                //     document.getElementById('long_usuario').value = place.geometry.location.lng();

                //     console.log("google.maps.event");

                //   //Caso a busca seja por endereço, como pegar o CEP avulso.
                //   //   var cep = "";

                //   //   $.each(place.address_components, function(){
                //   //       if(this.types[0]=="postal_code"){
                //   //        cep = this.short_name;
                //   //       }
                //   //   });

                //   // $('#cep_profissional').val(cep);
                   

                // });

                geocoder.geocode( { 'address': $(this).val() + ', Brasil', 'region': 'BR'}, function(r, s) {

                    console.log("geocoder "+$('#cep_profissional').val());

                    if (s === google.maps.GeocoderStatus.OK) {
                      endereco = r[0].formatted_address;

                        $.each(r[0].address_components, function(){


                            for (var i = r[0].address_components.length - 1; i >= 0; i--) {
                                
                                switch(this.types[i]) {
                                    case 'postal_code':
                                        console.log('CEP: '+this.short_name)
                                        break;
                                    case 'sublocality':
                                        console.log('Bairro: '+this.short_name)
                                        $('#bairro_profissional').val(this.short_name);
                                        break;
                                    case 'administrative_area_level_2':
                                        console.log('Cidade: '+this.short_name)
                                        $('#cidade_profissional').val(this.short_name);
                                        break;
                                    case 'administrative_area_level_1':
                                        console.log('Estado: '+this.short_name)
                                        $('#estado_profissional').val(this.short_name);
                                        break;
                                    case 'country':
                                        console.log('País: '+this.short_name)
                                        $('#pais_profissional').val(this.short_name);
                                        break;
                                    default:
                                        //console.log("Nenhum: "+r[0].address_components.short_name);
                                        break;
                                        
                                }

                            }
                        });


                      $('#localizacao_profissional').val(endereco);
                      $('#lat_usuario').val(r[0].geometry.location.lat());
                      $('#long_usuario').val(r[0].geometry.location.lng());

                    } else {

                      endereco = "Status: " + s;

                    }

                });

            } else {
                console.log('menor '+$(this).val().length);
            }
        });

    });
</script>

<script type="text/javascript">
    /* Máscaras ER */
    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
    function mtel3(cpf){
        cpf=cpf.replace(/\D/g,"");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
        cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

        return cpf;
    }
    function mtel2(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        return v;
    }

    function id( el ){
        return document.getElementById( el );
    }
    window.onload = function(){
        id('telefone_profissional').onkeyup = function(){
            mascara( this, mtel );
        }
        id('cep_profissional').onkeyup = function(){
            mascara( this, mtel2 );
        }
        id('cpf_profissional').onkeyup = function(){
            mascara( this, mtel3 );
        }
    }



</script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../js/jq2.js"></script>
</body>
</html>
