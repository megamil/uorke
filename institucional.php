<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="theme-color" content="#f7921c">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/bt2.css" rel="stylesheet">
    <link href="css/bt3.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">

    <style>
        .textos{
            width: 30%;
            text-align: center;
            margin: 0 auto;
            font-size: 18px;
        }
        .textos_titulos{
            font-weight: bolder;
            text-align: center;
            padding: 1% 0;
            width: 100%;
            margin: 0 auto;
            font-size: 23px;
        }
        .t2{
            background: #E2E6F5;
            padding: 4% 0;
        }
        .t1{
            padding: 4% 0;
        }
        #spc{
            height: 500px;
            padding:  2% 0 ;
            margin-top: 0;
            background: url('img/bg_institucion al.png') center center no-repeat;
            background-size: cover;
        }
    </style>
</head>
<!-- NAVBAR
================================================== -->
<body>
<p id="tudo">
    <!---------------------- topo ------------->
    <?php include'topo.php' ; ?>

    <!---------------------- fecha topo ------------->
    <!---------------------- Textos ------------->
<div id="spc"></div>

        <div class="t1" >
           <h2 class="textos_titulos">OBJETIVO</h2>
            <p class="textos">Inspirar oportunidades de otimismo conectando clientes e profissionais, utilizando ferramentas modernas e transformando relacionamentos em resultados.</p>
        </div>
        <div class="t2">
            <h2 class="textos_titulos">VALORES</h2>
            <p class="textos">Além de contribuir para que nossos profissionais aumentem os lucros, a equipe
            Uorke também investe para que inovação, confiança e transparência façam parte do nosso dia.</p>
        </div>
        <div class="t1" >
            <h2 class="textos_titulos">MISSÃO</h2>
            <p class="textos">Conectar profissionais e clientes de forma rápida e segura, utilizando de
                ferramentas modernas e de fácil uso. Criar valor e satisfazer nossos profissionais agregando oportunidades de ganhos.</p>
        </div>
        <div class="t2">
            <h2 class="textos_titulos">VISÃO</h2>
            <p class="textos">Continuar conectado, investindo e proporcionando oportunidades de crescimento para nossos clientes.</p>
        </div>

</div>











<?php include 'rodape.php'; ?>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="js/jq1.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="js/jq3.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/jq2.js"></script>
</body>
</html>
