<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="theme-color" content="#f7921c">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Uorke</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/bt2.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/js1.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
    <!-- Meus CSS-->
    <link rel="stylesheet" href="css/home.css" type="text/css">
    <link rel="stylesheet" href="css/login.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script>
        function efeito(){
            $('#formulario').animate({'opacity':'1.00'}, 1800, 'linear');
            $('#formulario').css('display', 'table');
        }

    </script>
    <style>
        .payment-methods {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .payment-methods:after {
            content: "";
            clear: both;
        }

        .payment-method {
            box-sizing: border-box;
            float: left;
            height: 50px;
            position: relative;
            width: 50%;
        }

        .payment-method + .payment-method {

        }
        .payment-method label {
            background: #fff no-repeat center center;
            bottom: 1px;
            cursor: pointer;
            display: block;
            font-size: 0;
            left: 1px;
            position: absolute;
            right: 1px;
            text-indent: 100%;
            top: 1px;
            white-space: nowrap;
        }

        .pagseguro label {
            background: url("img/cliente2.png") center center no-repeat;
            background-color: #00AEF2;
            background-size: 60%;
            border-radius:5px;
            opacity:.90;
        }

        .paypal label {
            background: url("img/profissional2.png") center center no-repeat;
            background-size: 60%;
            background-color: #f7921c;

            border-radius:5px;
            opacity:.9;
        }


        .payment-methods input:checked + label {
            opacity: 1;
            box-shadow: 2px 2px 2px #ccc;
        }

        .payment-methods input:checked + label:after {

        }

        @-moz-document url-prefix() {
            .payment-methods input:checked + label:after {
                bottom: 0;
                right: 0;
                background-color: #f7921c;
            }
        }
    </style>

</head>
<!-- NAVBAR
================================================== -->
<body onload="efeito()">
<div id="tudo" >
    <!-- topo -->
    <?php include('topo.php'); ?>
    <div id="preto"></div>

    <div id="formulario_todo" class="container">

        <div id="formulario" class="">
            <form method="POST" action="admin/main/login" id="formulario_todo">
                   <img src="img/logo.png" id="logo">

                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input id="email" type="text" class="form-control" name="usuario" class="login_senha" placeholder="Email">
                </div>
                <div class="input-group" id="senha_esp">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input  type="password" class="form-control" name="senha" id="senha" class="login_senha" placeholder="Senha">
                </div>

                <button type="submit" id="logar" name="enviar" class="btn btn-default" style="background:#f7921c ;border:1px solid #f7921c ; color: #fff;margin-top: 2%">Login</button>
                <a href="cadastro.php"> <div id="logar" class="btn btn-default" style="background:#f7921c ;border:1px solid #f7921c ;color: #fff;">Cadastre-se</div></a>
                <div>
                    <p>&nbsp;</p>

                   <!--  <p class="alinhar_ao_centro2">Esqueceu sua Senha ?</p>
                    <a href="#" class="btn btn-lg btn-block onl_btn-twitter" data-toggle="tooltip" id="redes_sociais" data-placement="top">
                        <i class="fa fa-facebook fa-2x" id="redes" ></i>
                        <i class="fa fa-pinterest fa-2x"  id="redes2" ></i>
                        <i class="fa fa-twitter fa-2x"  id="redes" ></i>
                        <i class="fa fa-linkedin fa-2x" id="redes2" ></i>
                        <span class="hidden-xs"></span>
                    </a> -->
                </div>

            </form>
        </div>


    </div>
</div>
<script src="js/jq1.js"></script>
<script src="js/jq3.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/jq2.js"></script>
</body>
</html>
